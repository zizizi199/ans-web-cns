import React from 'react';
import { Route, Routes } from 'react-router-dom';
import { AppRoutes } from '../helpers/app.routes';
import MainLayout from '../layouts/Main';

import { Page403 } from '../pages/ErrorPage/403';
import { Page404 } from '../pages/ErrorPage/404';
import { routers } from './constants';
const HomePage = React.lazy(async () => await import('src/pages/home'));
const LoginPage = React.lazy(async () => await import('src/pages/login'));
const ForgetPasswordPage = React.lazy(async () => await import('src/pages/quen-mat-khau'));

export const RootRouter = React.memo(() => {
  RootRouter.displayName = 'RootRouter';
  return (
    <Routes>
      <Route path={AppRoutes.login} element={<LoginPage isLogOut={false} />} />
      <Route path={AppRoutes.forgot_password} element={<ForgetPasswordPage />} />
      {/* <Route path={AppRoutes.forgot_password} element={<ForgetPasswordPage />} />
      <Route path={AppRoutes.get_otp} element={<GetOTPPage />} />
      <Route path={AppRoutes.get_new_password} element={<ResetPasswordPage />} /> */}
      <Route path={AppRoutes.home} element={<MainLayout currentUser={localStorage.getItem('role')} />}>
        <Route index element={<HomePage />} />
        {routers.map((route: any, index: any) => {
          // const isExistFromRole = route.permission?.includes(allowModules);
          return (
            <Route
              key={`route${index}`}
              path={route.path}
              // element={true ? route.element : <Page403 />}
              element={route.element}
            />
          );
        })}
      </Route>
      <Route path={AppRoutes.page404} element={<Page404 />} />
      <Route path={AppRoutes.page403} element={<Page403 />} />
      <Route path={AppRoutes.logout} element={<LoginPage isLogOut={true} />} />
    </Routes>
  );
});
