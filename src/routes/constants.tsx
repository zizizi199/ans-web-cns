import React from 'react';
import {
  AccountRoutes,
  EvaluateRoutes,
  HostelRoutes,
  OwnerRoutes,
  TenantRoutes,
  TransactionRoutes,
} from '../helpers/app.routes';

// Danh sách chủ nhà
const OwnerListPage = React.lazy(async () => await import('src/pages/quan-li-chu-nha/danh-sach'));
// Danh sách người đi thuê
const TenantListPage = React.lazy(async () => await import('src/pages/quan-li-nguoi-di-thue/danh-sach'));
// Lịch sử giao dịch
const TransactionListPage = React.lazy(async () => await import('src/pages/lich-su-giao-dich/danh-sach'));
// Quản lí đánh giá
const EvaluateListPage = React.lazy(async () => await import('src/pages/quan-li-danh-gia/danh-sach'));
// Danh sách nhà trọ
const HostelListPage = React.lazy(async () => await import('src/pages/danh-sach-nha-tro/danh-sach'));
const HostelCreatePage = React.lazy(async () => await import('src/pages/danh-sach-nha-tro/tao-moi'));
const HostelDetailPage = React.lazy(async () => await import('src/pages/danh-sach-nha-tro/chi-tiet'));
// Quản lí tiện ích
const UtilityListPage = React.lazy(async () => await import('src/pages/danh-sach-nha-tro/danh-sach'));
// Quản lí tài khoản
const AccountListPage = React.lazy(async () => await import('src/pages/danh-sach-tai-khoan/danh-sach'));
// Phân uyền
const RoleListPage = React.lazy(async () => await import('src/pages/danh-sach-tai-khoan/danh-sach'));

export const routers = [
  // Danh sách chủ nhà
  {
    path: OwnerRoutes.list,
    element: <OwnerListPage />,
    permission: [],
  },
  // Danh sách người đi thuê
  {
    path: TenantRoutes.list,
    element: <TenantListPage />,
    permission: [],
  },
  // Lịch sử giao dịch
  {
    path: TransactionRoutes.list,
    element: <TransactionListPage />,
    permission: [],
  },
  // Quản lí đánh giá
  {
    path: EvaluateRoutes.list,
    element: <EvaluateListPage />,
    permission: [],
  },
  // Danh sách nhà trọ
  {
    path: HostelRoutes.list,
    element: <HostelListPage />,
    permission: [],
  },
  // Tạo mới nhà trọ
  {
    path: HostelRoutes.create,
    element: <HostelCreatePage />,
    permission: [],
  },
  // Chi tiết nhà trọ
  {
    path: HostelRoutes.detail,
    element: <HostelDetailPage />,
    permission: [],
  },
  //Quản lí tiện ích
  {
    path: HostelRoutes.list,
    element: <UtilityListPage />,
    permission: [],
  },
  //Danh sách tài khoản
  {
    path: AccountRoutes.list,
    element: <AccountListPage />,
    permission: [],
  },
  //Phân quyền
  {
    path: AccountRoutes.list,
    element: <RoleListPage />,
    permission: [],
  },
];
