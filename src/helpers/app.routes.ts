export const AppRoutes = {
  home: '/',
  login: '/login',
  page404: '*',
  page403: '/403',
  forgot_password: '/quen-mat-khau',
  get_otp: '/lay-ma-otp',
  get_new_password: '/lay-mat-khau-moi',
  logout: '/logout',
};

// danh sách chủ nhà
export const OwnerRoutes = {
  list: '/quan-li-chu-nha/danh-sach',
};

// danh sách người đi thuê
export const TenantRoutes = {
  list: '/quan-li-nguoi-di-thue/danh-sach',
};

// lịch sử giao dịch
export const TransactionRoutes = {
  list: '/lich-su-giao-dich/danh-sach',
};

// quản lí đánh giá
export const EvaluateRoutes = {
  list: '/quan-li-danh-gia/danh-sach',
};

// quản lí nhà trọ
export const HostelRoutes = {
  list: '/danh-sach-nha-tro/danh-sach',
  create: '/danh-sach-nha-tro/tao-moi',
  detail: '/danh-sach-nha-tro/chi-tiet/:id/:isEdit',
  details: (id: any, isEdit: boolean) => `/danh-sach-nha-tro/chi-tiet/${id}/${isEdit}`,
};

// quản lí tài khoản
export const AccountRoutes = {
  list: '/danh-sach-tai-khoan/danh-sach',
};
