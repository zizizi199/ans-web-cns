export const AUTH = {
  LOGIN: '/api/login',
  LOGOUT: '/api/logout',
  REGISTER: '/api/auth/register',
  GET_TOKEN: '/api/auth/get-tokens',
  FORGOT_PASSWORD: '/api/auth/reset-password/send-otp',
  VALIDATE_OTP: '/api/auth/reset-password/validate-otp',
  RESET_PASSWORD: '/api/auth/reset-password',
  REFRESH_TOKEN: '/auth/refresh',
  CURRENT_USER: '/user/currentUserInfo',
  CHANGE_PASSWORD: '/api/user/change-pass',
};

export const UPLOAD = {
  SINGLE: '/api/common/upload',
  GET: 'api/common/get',
};

export const OWNER_API = {
  LIST: '/api/user/get-all',
  CREATE: '/api/user/create-by-cms',
  DETAIL: '/api/user',
  UPDATE: '/api/user/update-by-cms',
  DELETE: '/api/user',
  BLOCKPOST: '/api/user/locked-post',
  BLOCKUSER: '/api/user/locked-acc',
};

export const TRANSACTION_API = {
  LIST: '/api/transaction/get-all',
};

export const TENANT_API = {
  LIST: '/api/user/get-all',
  CREATE: '/api/user/create-by-cms',
  DETAIL: '/api/user',
  UPDATE: '/api/user/update-by-cms',
  DELETE: '/api/user',
};

export const EVALUATE_API = {
  LIST: '/api/review/get-all-cms',
  STATUS: '/api/review/change-status',
};

export const DISTRICT_API = {
  LIST: '/api/province/district',
};

export const HOME = {
  LIST: '/api/product/get-all-by-cms',
  DETAIL: '/api/product/detail',
  CREATE: '/api/product/create',
  UPDATE: '/api/product/update',
  DELETE: '/api/product',
  CHANGE_STATUS: '/api/product/change-status',
  APPROVE: '/api/product/is-approved',
};

export const PROVINCE_API = {
  LIST: '/api/province',
};

export const WARD_API = {
  LIST: '/api/province/district/wards',
};

export const SERVICE_API = {
  LIST: '/api/service',
};

export const USER_API = {
  LIST: '/api/user/cms',
  CREATE: '/api/user/cms',
  UPDATE: '/api/user/cms',
  DETAIL: '/api/user',
};
