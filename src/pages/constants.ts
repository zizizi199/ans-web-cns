import moment from 'moment';

export const formLayout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};

export const formValidateMessages = {
  required: 'Thông tin bắt buộc',
  types: {
    email: 'Email không đúng định dạng!',
    number: 'Vui lòng nhập đúng định dạng số!',
  },
  number: {
    range: '${label} phải nằm trong khoảng ${min} và ${max}',
  },
  string: {
    min: '${label} phải ít nhất ${min} kí tự',
    max: 'Không được phép nhập nhiều hơn ${max} kí tự',
  },
  whitespace: 'Thông tin bắt buộc',
};

export const filterOption = (input: string, option: any) => {
  return option.props.children.toString().toLowerCase().indexOf(input.toLowerCase().trim()) >= 0;
};

export const disableDateAfter = (current: any) => {
  // Can not select days before today and today
  return current > moment().startOf('day');
};

export const disableDateEnd = (current: any) => {
  // || current < moment().startOf("day");
  return current < moment(current?.toString()).startOf('day');
};
