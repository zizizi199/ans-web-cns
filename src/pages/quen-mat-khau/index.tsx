import React from 'react';
import { LoginContainer } from './containers/LoginContainer';
import LogoBig from 'src/components/Icon/logo-big.png';
import styles from './styles.module.scss';

export default function LoginPage() {
  return (
    <div className={styles.container}>
      <div className={styles.backGround}>
        <img src={LogoBig} alt={'Ans Logo'} className={styles.logoBig} />
      </div>
      <div className={styles.loginForm}>
        <LoginContainer />;
      </div>
    </div>
  );
}
