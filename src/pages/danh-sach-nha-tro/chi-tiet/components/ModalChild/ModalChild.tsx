import { Button, Form, InputNumber, Modal, Select } from 'antd';
import React, { useEffect } from 'react';
import styles from './styles.module.scss';
import { roomStatus } from 'src/common';
import { formValidateMessages } from 'src/pages/constants';

interface Props {
  openModalRoom: boolean;
  modalRoomStatus: any; // thông tin chi tiết của phòng theo từng tháng
  handleVisibleChangeModalRoom: (value: boolean) => void;
  onHandleStatus: (year: number, month: number, status: number, price: number) => void;
}

const ModalRoomInfor: React.FC<Props> = ({
  openModalRoom,
  modalRoomStatus,
  handleVisibleChangeModalRoom,
  onHandleStatus,
}) => {
  const [form] = Form.useForm();

  useEffect(() => {
    if (modalRoomStatus) {
      form.setFieldsValue({
        price: modalRoomStatus?.price,
        status: modalRoomStatus?.status,
      });
    }
  }, [modalRoomStatus]);

  const handleCancel = () => {
    handleVisibleChangeModalRoom(false);
    form.resetFields();
  };

  const onSubmit = () => {
    onHandleStatus(
      modalRoomStatus?.year,
      modalRoomStatus?.month,
      form.getFieldValue('status'),
      form.getFieldValue('price')
    );
  };

  return (
    <Modal
      onCancel={handleCancel}
      className={styles.container}
      open={openModalRoom}
      title={`THÔNG TIN THÁNG ${modalRoomStatus?.month}`}
      footer={[
        <div
          key={Math.random()}
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            paddingBottom: 20,
          }}
        >
          <Button
            style={{
              backgroundColor: '#F89420',
              color: 'white',
              width: '120px',
              borderRadius: '48px',
              // height: "48px",
            }}
            onClick={onSubmit}
            type="primary"
          >
            Lưu
          </Button>
        </div>,
      ]}
    >
      <div className="body-custom">
        <Form form={form} validateMessages={formValidateMessages}>
          <Form.Item label="Giá phòng" name="price">
            <InputNumber
              min={0}
              step={1000}
              formatter={(value) => ` ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
              parser={(value: any) => value.replace(/\$\s?|(,*)/g, '')}
              placeholder="Nhập giá phòng"
            />
          </Form.Item>
          <Form.Item label="trang thái" name="status">
            <Select placeholder="Chọn trạng thái">
              {roomStatus?.map((item: any) => {
                return (
                  <Select.Option value={item?.key} key={item?.key}>
                    {item?.label}
                  </Select.Option>
                );
              })}
            </Select>
          </Form.Item>
        </Form>
      </div>
    </Modal>
  );
};

export default ModalRoomInfor;
