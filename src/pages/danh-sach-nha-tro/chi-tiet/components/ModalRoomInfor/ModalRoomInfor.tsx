import { Button, Checkbox, Col, Form, Input, InputNumber, Modal, Row, Select, Upload, UploadFile, message } from 'antd';
import React, { useLayoutEffect, useState } from 'react';
import { PlusOutlined, MinusCircleOutlined } from '@ant-design/icons';
import styles from './styles.module.scss';
import { AttributeType, RoomDirectionArray, formatPrice, getVideoId, roomStatus, today } from 'src/common';
import { UPLOAD } from 'src/helpers/api';
import axiosIns, { HOST } from 'src/helpers/request';
import { formValidateMessages } from 'src/pages/constants';
import ModalChild from '../ModalChild/ModalChild';
import { EditRoomIcon, MoneyRoomIcon, NextIcon, PreviousIcon, StatusRoomIcon } from 'src/components/Icon';

interface Props {
  // form: FormInstance<any>;
  visibleModal: boolean;
  towerItem: any;
  roomItem: any;
  dsTienIch: any;
  onHandleVisibleModal: (value: boolean) => void;
  isEdit: any;
  onHandleEditRoom: (
    value: any,
    apartmentSales: any,
    towerKey: number,
    roomKey: number,
    attributesValue: any,
    infrastructureItem: any,
    fileListimgLivingRooms: any,
    listimgLivingRooms: any,
    fileListimgBedRooms: any,
    listimgBedRooms: any,
    fileListimgBathRooms: any,
    listimgBathRooms: any,
    fileListimgBalconys: any,
    listimgBalconys: any,
    fileListimgOverall: any,
    imgOverallModal: any,
    videoLinks: any
  ) => void;
}

const ModalRoomInfor: React.FC<Props> = ({
  // form,
  visibleModal,
  towerItem,
  roomItem,
  dsTienIch,
  onHandleVisibleModal,
  onHandleEditRoom,
  isEdit,
}) => {
  const [form] = Form.useForm();
  const [openModalRoom, setOpenModalRoom] = useState<boolean>(false);
  const [apartmentSales, setApartmentSales] = useState<any>([]);
  const [infrastructureItem, setInfrastructureItem] = useState<any>([]);
  const [state, _setState] = useState<any>({});
  const [selectedYear, setSelectedYear] = useState<number>(0);

  const [fileListimgLivingRooms, setFileListimgLivingRooms] = useState<UploadFile[]>([]);
  const [listimgLivingRooms, setListimgLivingRooms] = useState<any>([]);
  const [fileListimgBedRooms, setFileListimgBedRooms] = useState<UploadFile[]>([]);
  const [listimgBedRooms, setListimgBedRooms] = useState<any>([]);
  const [fileListimgBathRooms, setFileListimgBathRooms] = useState<UploadFile[]>([]);
  const [listimgBathRooms, setListimgBathRooms] = useState<any>([]);
  const [fileListimgBalconys, setFileListimgBalconys] = useState<UploadFile[]>([]);
  const [listimgBalconys, setListimgBalconys] = useState<any>([]);
  const [fileListimgOverall, setFileListimgOverall] = useState<UploadFile[]>([]);
  const [youtubeLinks, setYoutubeLinks] = useState<any>([]);
  const [youtubeLink, setYoutubeLink] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const setState = (_state: any) => {
    _setState((state: any) => ({
      ...state,
      ...(_state || {}),
    }));
  };

  useLayoutEffect(() => {
    if (visibleModal === true) {
      //xử lý  ngày tháng năm
      let apartmentSalesArr: any = [];
      const result: any = {};
      const apartmentSales = roomItem?.apartmentSales;
      apartmentSales?.map((item: any) => {
        const { year, month, status, row } = item;
        if (!result[year]) {
          result[year] = { year, allMonths: [] };
        }
        const itemArr = {
          month,
          status,
          row,
        };
        result[year].allMonths.push(itemArr);
        apartmentSalesArr = Object.values(result);
      });
      form.setFieldsValue({
        roomPrice: roomItem?.roomPrice,
        attributes: roomItem?.attributes,
        direction: roomItem?.direction,
        temporaryRegistration: roomItem?.temporaryRegistration,
        fireProtection: roomItem?.fireProtection,
        lightingSystem: roomItem?.lightingSystem,
        washingMachine: roomItem?.washingMachine,
        apartmentSales: roomItem?.apartmentSales[0]?.allMonths ? roomItem?.apartmentSales : apartmentSalesArr,
        area: roomItem?.area,
        maxPeople: roomItem?.maxPeople,
        // videoLinks: roomItem?.videoLinks?.map((item: any) => {
        //   return {
        //     videoLink: item,
        //   };
        // }),
      });
      setYoutubeLinks(roomItem?.videoLinks || []);
      if (roomItem?.attributes) {
        roomItem?.attributes.map((item: any) => {
          form.setFieldsValue({
            [item?.id]: item?.value || 0,
          });
        });
      }
      const infrastructures = dsTienIch.map((item: any) => {
        const found = roomItem?.infrastructures.some((otherItem: any) => (otherItem?.id || otherItem) === item?.id);
        return { ...item, checked: found };
      });

      infrastructures
        ?.filter((item: any) => item?.typeAttribute === AttributeType.INFRASTRUCTURE)
        ?.map((item: any) => {
          if (item?.checked) {
            form.setFieldsValue({
              [item?.id]: true,
            });
          } else {
            form.setFieldsValue({
              [item?.id]: false,
            });
          }
        });

      const infrastructuresArr = roomItem?.infrastructures?.map((item: any) => (item?.id ? item?.id : item));
      setInfrastructureItem(infrastructuresArr);

      setApartmentSales(roomItem?.apartmentSales[0]?.allMonths ? roomItem?.apartmentSales : apartmentSalesArr);
      setState({
        attributes: roomItem?.attributes,
        infrastructures,
      });

      //upload
      const listImgBedRooms: any = [];
      roomItem?.listImgBedRooms
        ?.filter((it: any) => it !== '')
        ?.map((item: any) => {
          const idx = item.lastIndexOf('/') + 1;
          const filename = item.substr(idx);

          listImgBedRooms.push({
            uid: item?.id,
            name: filename,
            status: 'done',
            url: item && `${HOST}${UPLOAD.GET}/${new URL(item).pathname.split('/').pop()}`,
            type: 'image/*',
            webkitRelativePath: '',
          });
        });
      setFileListimgBedRooms(listImgBedRooms);
      setListimgBedRooms(roomItem?.listImgBedRooms ? roomItem?.listImgBedRooms : []);

      const listImgBathRooms: any = [];
      roomItem?.listImgBathRooms
        ?.filter((it: any) => it !== '')
        ?.map((item: any) => {
          const idx = item.lastIndexOf('/') + 1;
          const filename = item.substr(idx);

          listImgBathRooms.push({
            uid: item,
            name: filename,
            status: 'done',
            url: item && `${HOST}${UPLOAD.GET}/${new URL(item).pathname.split('/').pop()}`,
            type: 'image/*',
            webkitRelativePath: '',
          });
        });
      setFileListimgBathRooms(listImgBathRooms);
      setListimgBathRooms(roomItem?.listImgBathRooms ? roomItem?.listImgBathRooms : []);

      const listImgBalconys: any = [];
      roomItem?.listImgBalconys
        ?.filter((it: any) => it !== '')
        ?.map((item: any) => {
          const idx = item.lastIndexOf('/') + 1;
          const filename = item.substr(idx);

          listImgBalconys.push({
            uid: item,
            name: filename,
            status: 'done',
            url: item && `${HOST}${UPLOAD.GET}/${new URL(item).pathname.split('/').pop()}`,
            type: 'image/*',
            webkitRelativePath: '',
          });
        });
      setFileListimgBalconys(listImgBalconys);
      setListimgBalconys(roomItem?.listImgBalconys ? roomItem?.listImgBalconys : []);

      const listImgLivingRooms: any = [];
      roomItem?.listImgLivingRooms
        ?.filter((it: any) => it !== '')
        ?.map((item: any) => {
          const idx = item.lastIndexOf('/') + 1;
          const filename = item.substr(idx);

          listImgLivingRooms.push({
            uid: item,
            name: filename,
            status: 'done',
            url: item && `${HOST}${UPLOAD.GET}/${new URL(item).pathname.split('/').pop()}`,
            type: 'image/*',
            webkitRelativePath: '',
          });
        });
      setFileListimgLivingRooms(listImgLivingRooms);
      setListimgLivingRooms(roomItem?.listImgLivingRooms ? roomItem?.listImgLivingRooms : []);

      const imgOverall: any = [];
      const idx = roomItem?.imgOverall?.lastIndexOf('/') + 1;
      const filename = roomItem?.imgOverall?.substr(idx);

      imgOverall.push({
        uid: roomItem?.imgOverall,
        name: filename,
        status: 'done',
        url: roomItem?.imgOverall && `${HOST}${UPLOAD.GET}/${new URL(roomItem?.imgOverall).pathname.split('/').pop()}`,
        type: 'image/*',
        webkitRelativePath: '',
      });
      setFileListimgOverall(roomItem?.imgOverall ? imgOverall : []);
      // setListimgLivingRooms(roomItem?.imgOverall);
      form.setFieldsValue({ imgOverallModal: roomItem?.imgOverall });
    }
  }, [towerItem, roomItem, visibleModal]);

  const handleVisibleChangeModalRoom = (flag: any) => {
    setOpenModalRoom(flag);
  };

  const onHandleAttribute = (id: any, value: any) => {
    let data = state?.attributes || dsTienIch?.filter((item: any) => item?.typeAttribute === AttributeType.SERVICE);
    data = data?.map((item: any) => {
      if (item?.id === id) {
        return {
          id: item?.id,
          name: item?.name,
          value: value,
          typeAttribute: item?.typeAttribute,
          unit: item?.unit,
        };
      }
      return {
        id: item?.id,
        name: item?.name,
        value: item?.value || 0,
        typeAttribute: item?.typeAttribute,
        unit: item?.unit,
      };
    });
    setState({
      attributes: data,
    });
    // console.log("data", data);

    form.setFieldsValue({
      attributes: data,
    });
  };

  const onHandleStatus = (year: number, month: number, status: number, price: number) => {
    setOpenModalRoom(false);
    const arrApartmentSales = apartmentSales?.map((item: any) => {
      if (item?.year === year) {
        return {
          ...item,
          allMonths: item?.allMonths?.map((itemMonth: any) => {
            if (itemMonth?.month === month) {
              return {
                ...itemMonth,
                status: status,
                price: price,
              };
            }
            return itemMonth;
          }),
        };
      }
      return item;
    });
    setApartmentSales(arrApartmentSales);
    form.setFieldsValue({
      apartmentSales: arrApartmentSales,
    });
  };

  const onHandleModalRoom = (year: number, month: number, status: number, price: number) => () => {
    setState({
      modalRoomStatus: {
        year: year,
        month: month,
        status: status,
        price: price,
      },
    });
    setOpenModalRoom(true);
  };

  const onHandleInfrastructure = (id: any, value: any) => {
    let infrastructure: any = infrastructureItem;
    if (value === true) {
      infrastructure.push(id);
      setInfrastructureItem(infrastructure);
    } else {
      infrastructure = infrastructureItem?.filter((item: any) => item !== id);
      setInfrastructureItem(infrastructure);
    }
  };

  const handleCancel = () => {
    onHandleVisibleModal(false);
  };

  //upload
  const handleChangeUploadImage = (type: string, data: any) => {
    setIsLoading(true);
    const isLt2M = data?.file?.size / 1024 / 1024 < 10;
    if (data?.file?.status !== 'removed') {
      if (!isLt2M) {
        message.error('Kích thước ảnh không được vượt quá 10Mb.');
        setIsLoading(false);
        return;
      }
    }

    // if (data?.file?.status === "error") {
    if (type === 'imgLivingRooms') {
      setFileListimgLivingRooms(data?.fileList);
    } else if (type === 'imgBedRooms') {
      setFileListimgBedRooms(data?.fileList);
    } else if (type === 'imgBathRooms') {
      setFileListimgBathRooms(data?.fileList);
    } else if (type === 'imgBalconys') {
      setFileListimgBalconys(data?.fileList);
    } else if (type === 'imgOverall') {
      setFileListimgOverall(data?.fileList);
    }

    const formData: any = new FormData();
    const file = data?.fileList?.map((item: any) => item?.originFileObj);
    file.map((item: any) => {
      formData.append('file', item);
    });
    if (data?.file?.status === 'removed') {
      setIsLoading(false);
    }
    if (data?.file?.status === 'error' || data?.file?.status === 'done') {
      return new Promise((resolve, reject) => {
        axiosIns
          .post(`${UPLOAD.SINGLE}`, formData, {
            headers: {
              'Content-Type': 'multipart/form-data',
            },
          })
          .then((s) => {
            setIsLoading(false);
            if (type === 'imgLivingRooms') {
              const arr: any = [];
              arr.push(...listimgLivingRooms, s?.data?.data?.fileUrl);
              setListimgLivingRooms(arr);
            } else if (type === 'imgBedRooms') {
              const arr: any = [];
              arr.push(...listimgBedRooms, s?.data?.data?.fileUrl);
              setListimgBedRooms(arr);
            } else if (type === 'imgBathRooms') {
              const arr: any = [];
              arr.push(...listimgBathRooms, s?.data?.data?.fileUrl);
              setListimgBathRooms(arr);
            } else if (type === 'imgBalconys') {
              const arr: any = [];
              arr.push(...listimgBalconys, s?.data?.data?.fileUrl);
              setListimgBalconys(arr);
            } else if (type === 'imgOverall') {
              form.setFieldsValue({
                imgOverallModal: s?.data?.data?.fileUrl,
              });
            }
          })
          .catch((e) => {
            setIsLoading(false);
            reject(e);
          });
      });
    }
  };

  const handleRemoveFile = (type: string) => (data: any) => {
    if (type === 'imgLivingRooms') {
      const index = fileListimgLivingRooms?.findIndex((item: any) => item?.uid === data?.uid);
      const arr = listimgLivingRooms;
      arr.splice(index, 1);
      const _arr = [...arr];

      setListimgLivingRooms(_arr);
    } else if (type === 'imgBedRooms') {
      const index = fileListimgBedRooms?.findIndex((item: any) => item?.uid === data?.uid);
      const arr = listimgBedRooms;
      arr.splice(index, 1);
      const _arr = [...arr];
      setListimgBedRooms(_arr);
    } else if (type === 'imgBathRooms') {
      const index = fileListimgBathRooms?.findIndex((item: any) => item?.uid === data?.uid);
      const arr = listimgBathRooms;
      arr.splice(index, 1);
      const _arr = [...arr];
      setListimgBathRooms(_arr);
    } else if (type === 'imgBalconys') {
      const index = fileListimgBalconys?.findIndex((item: any) => item?.uid === data?.uid);
      const arr = listimgBalconys;
      arr.splice(index, 1);
      const _arr = [...arr];
      setListimgBalconys(_arr);
    } else if (type === 'imgOverall') {
      form.setFieldsValue({
        imgOverallModal: undefined,
      });
    }
  };

  const onSubmit = () => {
    if (
      fileListimgLivingRooms?.length === 0 ||
      fileListimgBedRooms?.length === 0 ||
      fileListimgBathRooms?.length === 0 ||
      fileListimgBalconys?.length === 0 ||
      fileListimgOverall?.length === 0
    ) {
      if (fileListimgLivingRooms?.length === 0) {
        message.error('Còn thiếu ảnh phòng khách');
      }
      if (fileListimgBedRooms?.length === 0) {
        message.error('Còn thiếu ảnh phòng ngủ');
      }
      if (fileListimgBathRooms?.length === 0) {
        message.error('Còn thiếu ảnh phòng vệ sinh');
      }
      if (fileListimgBalconys?.length === 0) {
        message.error('Còn thiếu ảnh ban công');
      }
      if (fileListimgOverall?.length === 0) {
        message.error('Còn thiếu bao quát');
      }
    } else {
      onHandleVisibleModal(false);
      onHandleEditRoom(
        form.getFieldsValue(),
        apartmentSales,
        towerItem.key,
        roomItem.key,
        form.getFieldValue('attributes'),
        infrastructureItem,
        fileListimgLivingRooms,
        listimgLivingRooms,
        fileListimgBedRooms,
        listimgBedRooms,
        fileListimgBathRooms,
        listimgBathRooms,
        fileListimgBalconys,
        listimgBalconys,
        fileListimgOverall,
        form.getFieldValue('imgOverallModal'),
        youtubeLinks
      );
    }
  };

  const handleInputChangeLinkVideo = (event: any) => {
    const link = event.target.value;
    setYoutubeLink(link);
  };

  const embedVideo = () => {
    // Kiểm tra xem đầu vào có phải là liên kết YouTube hợp lệ hay không
    const regExp =
      /^(?:https?:\/\/)?(?:www\.)?(?:youtube\.com\/(?:watch\?v=|embed\/|v\/|channels\/(?:\w+\/)?|user\/\w+|user\/(?:\w+\/)?\/user\/\w+)|youtu\.be\/)([a-zA-Z0-9_-]{11})/;
    const match = youtubeLink.match(regExp);
    if (match) {
      setYoutubeLinks((prevLinks: any) => [...prevLinks, youtubeLink]);
    } else {
      message.error('Link video không đúng định dạng');
    }
  };

  const handleDeleteVideo = (index: number) => {
    setYoutubeLinks(youtubeLinks.filter((item: any, i: number) => i !== index));
  };

  return (
    <div>
      <Modal
        open={visibleModal}
        onCancel={handleCancel}
        width={'50%'}
        className={styles.container}
        title={'Phòng ' + roomItem?.heading}
        onOk={onSubmit}
        closable={false}
        okText="Lưu"
        cancelText="Hủy"
        footer={[
          <div
            key={Math.random()}
            style={{
              display: 'flex',
              // flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Button
              style={{
                background: '#C8CBD0',
                color: 'white',
                width: '120px',
                borderRadius: '48px',
                // height: "48px",
                marginRight: '10px',
              }}
              onClick={handleCancel}
              loading={isLoading}
            >
              Đóng
            </Button>
            {isEdit && (
              <Button
                style={{
                  backgroundColor: '#F89420',
                  color: 'white',
                  width: '120px',
                  borderRadius: '48px',
                  // height: "48px",
                }}
                onClick={onSubmit}
                loading={isLoading}
                type="primary"
              >
                Lưu
              </Button>
            )}
          </div>,
        ]}
        afterClose={() => {
          setState({
            attributes: undefined,
          });
          setListimgLivingRooms([]);
          setFileListimgLivingRooms([]);
          setListimgBedRooms([]);
          setFileListimgBedRooms([]);
          setListimgBathRooms([]);
          setFileListimgBathRooms([]);
          setListimgBalconys([]);
          setFileListimgBalconys([]);
          setFileListimgOverall([]);

          const fields = form.getFieldsValue();
          const nullValues = Object.keys(fields).reduce((acc: any, fieldName: any) => {
            if (!isNaN(fieldName)) {
              acc[fieldName] = null;
              return acc;
            } else {
              return acc;
            }
          }, {});
          form.setFieldsValue(nullValues);
        }}
      >
        <Form validateMessages={formValidateMessages} form={form} layout="vertical">
          <Row gutter={[16, 16]}>
            <Col xs={23} sm={23} md={23} lg={12} xl={12}>
              <Form.Item name="area" label="Diện tích phòng (m&#x00B2;)">
                <InputNumber
                  formatter={(value) => ` ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                  parser={(value: any) => value.replace(/\$\s?|(,*)/g, '')}
                  min={0}
                  placeholder="Nhập diện tích"
                  disabled={!isEdit}
                />
              </Form.Item>
            </Col>
            <Col xs={23} sm={23} md={23} lg={12} xl={12}>
              <Form.Item name="maxPeople" label="Số người tối đa">
                <InputNumber disabled={!isEdit} min={0} placeholder="Nhập số người tối đa" />
              </Form.Item>
            </Col>
          </Row>
          <p className="section">Giá dịch vụ</p>
          <Row gutter={[16, 16]}>
            {(state.attributes || dsTienIch)
              ?.filter((item: any) => item?.typeAttribute === AttributeType.SERVICE)
              ?.map((itemMap: any, index: number) => {
                return (
                  <Col xs={23} sm={23} md={23} lg={12} xl={12} key={index}>
                    <Form.Item name={itemMap?.id} label={`${itemMap?.name}(${itemMap?.unit})`}>
                      <InputNumber
                        disabled={!isEdit}
                        formatter={(value) => ` ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                        parser={(value: any) => value.replace(/\$\s?|(,*)/g, '')}
                        step={1000}
                        min={0}
                        placeholder={`Nhập giá ${itemMap?.name}`}
                        value={itemMap?.value}
                        onChange={(e) => onHandleAttribute(itemMap?.id, e)}
                      />
                    </Form.Item>
                  </Col>
                );
              })}

            {/* <Col xs={23} sm={23} md={23} lg={12} xl={12}>
            <Form.Item name="cleaningPrice" label="Vệ sinh (đồng/tháng)">
              <InputNumber step={1000} min={0} placeholder="Nhập giá vệ sinh" />
            </Form.Item>
          </Col>
          <Col xs={23} sm={23} md={23} lg={12} xl={12}>
            <Form.Item name="waterPrice" label="Nước (đồng/tháng)">
              <InputNumber placeholder="Nhập giá nước" />
            </Form.Item>
          </Col>
          <Col xs={23} sm={23} md={23} lg={12} xl={12}>
            <Form.Item name="parkingPrice" label="Gửi xe (đồng/tháng)">
              <InputNumber placeholder="Nhập giá gửi xe" />
            </Form.Item>
          </Col>
          <Col xs={23} sm={23} md={23} lg={12} xl={12}>
            <Form.Item name="internetPrice" label="Mạng (đồng/tháng)">
              <InputNumber placeholder="Nhập giá mạng" />
            </Form.Item>
          </Col> */}
          </Row>

          <p className="section">Thông tin phòng</p>
          <div className="tableWrapperRoom">
            <div
              style={{
                display: 'flex',
                justifyContent: 'flex-end',
                gap: '8px',
                padding: '0 10px 10px 10px',
                fontSize: '12px',
              }}
            >
              <div className="circle" style={{ backgroundColor: '#B2B5BC' }}></div>
              <p>Đã cho thuê</p>
              <div className="circle" style={{ backgroundColor: '#C7F09E' }}></div>
              <p>Đã mở bán</p>
              <div className="circle" style={{ backgroundColor: '#C8D8FF' }}></div>
              <p>Chưa mở bán</p>
              <div className="circle" style={{ backgroundColor: '#FFCCC9' }}></div>
              <p>Không được mở bán</p>
            </div>
            <table style={{ width: '100%' }}>
              <tr style={{ backgroundColor: '#F89420' }}>
                <th colSpan={4}>
                  <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <div>
                      {/* Lùi năm */}
                      {/* <Button
                        className="yearButton"
                        onClick={() => {
                          setSelectedYear(selectedYear - 1);
                        }}
                        disabled={selectedYear > 0 ? false : true}
                      >
                        &#8826;
                      </Button> */}
                      <PreviousIcon
                        onClick={() => {
                          setSelectedYear(selectedYear - 1);
                        }}
                        style={{ display: selectedYear > 0 ? '' : 'none', cursor: 'pointer' }}
                        // disabled={selectedYear > 0 ? false : true}
                      />
                    </div>
                    <div>{apartmentSales?.length > 0 ? apartmentSales[selectedYear]?.year : today.getFullYear()}</div>
                    <div>
                      {/* Tiến năm */}
                      {/* <Button
                        className="yearButton"
                        onClick={() => {
                          setSelectedYear(selectedYear + 1);
                        }}
                        disabled={selectedYear + 1 === apartmentSales?.length ? true : false}
                      >
                        &#8827;
                      </Button> */}
                      <NextIcon
                        onClick={() => {
                          setSelectedYear(selectedYear + 1);
                        }}
                        style={{
                          display: selectedYear + 1 === apartmentSales?.length ? 'none' : '',
                          cursor: 'pointer',
                        }}
                        // disabled={selectedYear > 0 ? false : true}
                      />
                    </div>
                  </div>
                </th>
              </tr>
              <tr>
                {apartmentSales?.length > 0 &&
                  apartmentSales[selectedYear]?.allMonths
                    ?.filter((item: any) => item?.row === 1)
                    ?.map((itemMap: any, index: any) => {
                      return (
                        <td
                          key={index}
                          style={{
                            width: '153px',
                            height: '64px',
                            // backgroundColor:
                            //   itemMap?.status === 1
                            //     ? "#B2B5BC"
                            //     : itemMap?.status === 2
                            //     ? "#C7F09E"
                            //     : itemMap?.status === 3
                            //     ? "#C8D8FF"
                            //     : "#FFCCC9",
                          }}
                        >
                          <div>
                            <div
                              style={{
                                background: '#EFF0F1',
                                display: 'flex',
                                justifyContent: 'space-between',
                              }}
                            >
                              <p className="monthLabel">THÁNG {itemMap?.month}</p>
                              {/* <img
                                onClick={onHandleModalRoom(
                                  apartmentSales[selectedYear]?.year,
                                  itemMap?.month,
                                  itemMap?.status,
                                  itemMap?.price || 0
                                )}
                                style={{ cursor: 'pointer' }}
                                src={'/img/icon/EditIcon.png'}
                              /> */}
                              {isEdit && (
                                <EditRoomIcon
                                  onClick={onHandleModalRoom(
                                    apartmentSales[selectedYear]?.year,
                                    itemMap?.month,
                                    itemMap?.status,
                                    itemMap?.price || 0
                                  )}
                                  style={{ cursor: 'pointer' }}
                                />
                              )}

                              {/* <DownOutlined style={{ marginTop: "7px" }} /> */}
                            </div>
                            <div>
                              <div style={{ padding: '5px 0', display: 'flex' }}>
                                {/* <img
                                  style={{ width: 17, height: 16 }}
                                  src={'/img/icon/statusRoom.png'}
                                  alt={'statusRoom Icon'}
                                  width={0}
                                  height={0}
                                /> */}
                                <StatusRoomIcon />
                                <span style={{ marginTop: '-3px' }} className="pl-3">
                                  <p
                                    style={{
                                      color:
                                        itemMap?.status === 1
                                          ? '#B2B5BC'
                                          : itemMap?.status === 2
                                          ? '#C7F09E'
                                          : itemMap?.status === 3
                                          ? '#C8D8FF'
                                          : '#FFCCC9',
                                    }}
                                  >
                                    {roomStatus?.find((item: any) => item?.key === itemMap?.status)?.label}
                                  </p>
                                </span>
                              </div>
                              <div style={{ padding: '6px 0', display: 'flex' }} className="pt-6 pb-6">
                                {/* <img
                                  style={{ width: 17, height: 16 }}
                                  src={'/img/icon/moneyRoom.png'}
                                  alt={'moneyRoom Icon'}
                                  width={0}
                                  height={0}
                                /> */}
                                <MoneyRoomIcon />
                                <span style={{ marginTop: '-3px' }} className="pl-3">
                                  {formatPrice(itemMap.price)}
                                </span>
                              </div>
                            </div>
                          </div>
                          {/* <Dropdown
                        visible={openModalRoom === itemMap?.month}
                        onVisibleChange={(visible) =>
                          handleVisibleChange(visible ? itemMap?.month : null)
                        }
                        menu={
                          <div
                            style={{
                              backgroundColor: "white",
                              padding: "4px",
                              listStyleType: "none",
                              backgroundClip: "padding-box",
                              borderRadius: "8px",
                              outline: "none",
                              boxShadow:
                                "0 6px 16px 0 rgba(0, 0, 0, 0.08), 0 3px 6px -4px rgba(0, 0, 0, 0.12), 0 9px 28px 8px rgba(0, 0, 0, 0.05)",
                            }}
                          >
                            {items?.map((itemStatus: any) => (
                              <div>
                                <Button
                                  onClick={onHandleStatus(
                                    apartmentSales[selectedYear]?.year,
                                    itemMap?.month,
                                    itemStatus?.key
                                  )}
                                  type="text"
                                >
                                  {itemStatus?.label}
                                </Button>
                              </div>
                            ))}
                          </div>
                        }
                        trigger={["click"]}
                      ></Dropdown> */}
                        </td>
                      );
                    })}
              </tr>
              <tr>
                {apartmentSales?.length > 0 &&
                  apartmentSales[selectedYear]?.allMonths
                    ?.filter((item: any) => item?.row === 2)
                    ?.map((itemMap: any, index: any) => (
                      <td
                        key={index}
                        style={{
                          width: '153px',
                          height: '64px',
                          // backgroundColor:
                          //   itemMap?.status === 1
                          //     ? "#B2B5BC"
                          //     : itemMap?.status === 2
                          //     ? "#C7F09E"
                          //     : itemMap?.status === 3
                          //     ? "#C8D8FF"
                          //     : "#FFCCC9",
                        }}
                      >
                        <div>
                          <div
                            style={{
                              background: '#EFF0F1',
                              display: 'flex',
                              justifyContent: 'space-between',
                            }}
                          >
                            <p className="monthLabel">THÁNG {itemMap?.month}</p>
                            {/* <img
                              onClick={onHandleModalRoom(
                                apartmentSales[selectedYear]?.year,
                                itemMap?.month,
                                itemMap?.status,
                                itemMap?.price || 0
                              )}
                              style={{ cursor: 'pointer' }}
                              src={'/img/icon/EditIcon.png'}
                            /> */}
                            {isEdit && (
                              <EditRoomIcon
                                onClick={onHandleModalRoom(
                                  apartmentSales[selectedYear]?.year,
                                  itemMap?.month,
                                  itemMap?.status,
                                  itemMap?.price || 0
                                )}
                                style={{ cursor: 'pointer' }}
                              />
                            )}
                            {/* <DownOutlined style={{ marginTop: "7px" }} /> */}
                          </div>
                          <div>
                            <div style={{ padding: '5px 0', display: 'flex' }}>
                              {/* <img
                                style={{ width: 17, height: 16 }}
                                src={'/img/icon/statusRoom.png'}
                                alt={'statusRoom Icon'}
                                width={0}
                                height={0}
                              /> */}
                              <StatusRoomIcon />
                              <span style={{ marginTop: '-3px' }} className="pl-3">
                                <p
                                  style={{
                                    color:
                                      itemMap?.status === 1
                                        ? '#B2B5BC'
                                        : itemMap?.status === 2
                                        ? '#C7F09E'
                                        : itemMap?.status === 3
                                        ? '#C8D8FF'
                                        : '#FFCCC9',
                                  }}
                                >
                                  {roomStatus?.find((item: any) => item?.key === itemMap?.status)?.label}
                                </p>
                              </span>
                            </div>
                            <div style={{ padding: '6px 0', display: 'flex' }} className=" pt-6 pb-6">
                              {/* <img
                                style={{ width: 17, height: 16 }}
                                src={'/img/icon/moneyRoom.png'}
                                alt={'moneyRoom Icon'}
                                width={0}
                                height={0}
                              /> */}
                              <MoneyRoomIcon />
                              <span style={{ marginTop: '-3px' }} className="pl-3">
                                {formatPrice(itemMap.price)}
                              </span>
                            </div>
                          </div>
                        </div>
                        {/* <Dropdown
                        visible={openModalRoom === itemMap?.month}
                        onVisibleChange={(visible) =>
                          handleVisibleChange(visible ? itemMap?.month : null)
                        }
                        menu={
                          <div
                            style={{
                              backgroundColor: "white",
                              padding: "4px",
                              listStyleType: "none",
                              backgroundClip: "padding-box",
                              borderRadius: "8px",
                              outline: "none",
                              boxShadow:
                                "0 6px 16px 0 rgba(0, 0, 0, 0.08), 0 3px 6px -4px rgba(0, 0, 0, 0.12), 0 9px 28px 8px rgba(0, 0, 0, 0.05)",
                            }}
                          >
                            {items?.map((itemStatus: any) => (
                              <div>
                                <Button
                                  onClick={onHandleStatus(
                                    apartmentSales[selectedYear]?.year,
                                    itemMap?.month,
                                    itemStatus?.key
                                  )}
                                  type="text"
                                >
                                  {itemStatus?.label}
                                </Button>
                              </div>
                            ))}
                          </div>
                        }
                        trigger={["click"]}
                      ></Dropdown> */}
                      </td>
                    ))}
              </tr>
              <tr>
                {apartmentSales?.length > 0 &&
                  apartmentSales[selectedYear]?.allMonths
                    ?.filter((item: any) => item?.row === 3)
                    ?.map((itemMap: any, index: any) => (
                      <td
                        key={index}
                        style={{
                          width: '153px',
                          height: '64px',
                          // backgroundColor:
                          //   itemMap?.status === 1
                          //     ? "#B2B5BC"
                          //     : itemMap?.status === 2
                          //     ? "#C7F09E"
                          //     : itemMap?.status === 3
                          //     ? "#C8D8FF"
                          //     : "#FFCCC9",
                        }}
                      >
                        <div>
                          <div
                            style={{
                              background: '#EFF0F1',
                              display: 'flex',
                              justifyContent: 'space-between',
                            }}
                          >
                            <p className="monthLabel">THÁNG {itemMap?.month}</p>
                            {/* <img
                              onClick={onHandleModalRoom(
                                apartmentSales[selectedYear]?.year,
                                itemMap?.month,
                                itemMap?.status,
                                itemMap?.price || 0
                              )}
                              style={{ cursor: 'pointer' }}
                              src={'/img/icon/EditIcon.png'}
                            /> */}
                            {isEdit && (
                              <EditRoomIcon
                                onClick={onHandleModalRoom(
                                  apartmentSales[selectedYear]?.year,
                                  itemMap?.month,
                                  itemMap?.status,
                                  itemMap?.price || 0
                                )}
                                style={{ cursor: 'pointer' }}
                              />
                            )}
                            {/* <DownOutlined style={{ marginTop: "7px" }} /> */}
                          </div>
                          <div>
                            <div style={{ padding: '5px 0', display: 'flex' }}>
                              {/* <img
                                style={{ width: 17, height: 16 }}
                                src={'/img/icon/statusRoom.png'}
                                alt={'statusRoom Icon'}
                                width={0}
                                height={0}
                              /> */}
                              <StatusRoomIcon />
                              <span style={{ marginTop: '-3px' }} className="pl-3">
                                <p
                                  style={{
                                    color:
                                      itemMap?.status === 1
                                        ? '#B2B5BC'
                                        : itemMap?.status === 2
                                        ? '#C7F09E'
                                        : itemMap?.status === 3
                                        ? '#C8D8FF'
                                        : '#FFCCC9',
                                  }}
                                >
                                  {roomStatus?.find((item: any) => item?.key === itemMap?.status)?.label}
                                </p>
                              </span>
                            </div>
                            <div style={{ padding: '6px 0', display: 'flex' }} className="pt-6 pb-6">
                              {/* <img
                                style={{ width: 17, height: 16 }}
                                src={'/img/icon/moneyRoom.png'}
                                alt={'moneyRoom Icon'}
                                width={0}
                                height={0}
                              /> */}
                              <MoneyRoomIcon />
                              <span style={{ marginTop: '-3px' }} className="pl-3">
                                {formatPrice(itemMap.price)}
                              </span>
                            </div>
                          </div>
                        </div>
                        {/* <Dropdown
                        visible={openModalRoom === itemMap?.month}
                        onVisibleChange={(visible) =>
                          handleVisibleChange(visible ? itemMap?.month : null)
                        }
                        menu={
                          <div
                            style={{
                              backgroundColor: "white",
                              padding: "4px",
                              listStyleType: "none",
                              backgroundClip: "padding-box",
                              borderRadius: "8px",
                              outline: "none",
                              boxShadow:
                                "0 6px 16px 0 rgba(0, 0, 0, 0.08), 0 3px 6px -4px rgba(0, 0, 0, 0.12), 0 9px 28px 8px rgba(0, 0, 0, 0.05)",
                            }}
                          >
                            {items?.map((itemStatus: any) => (
                              <div>
                                <Button
                                  onClick={onHandleStatus(
                                    apartmentSales[selectedYear]?.year,
                                    itemMap?.month,
                                    itemStatus?.key
                                  )}
                                  type="text"
                                >
                                  {itemStatus?.label}
                                </Button>
                              </div>
                            ))}
                          </div>
                        }
                        trigger={["click"]}
                      ></Dropdown> */}
                      </td>
                    ))}
              </tr>
            </table>
          </div>
          <p className="section">Tiện ích</p>
          <Col xs={23} sm={23} md={23} lg={12} xl={12}>
            <Form.Item name="direction" label="Hướng phòng">
              <Select disabled={!isEdit} placeholder="Chọn hướng phòng">
                {RoomDirectionArray?.map((item: any) => (
                  <Select.Option value={item?.value} key={item?.value}>
                    {item?.label}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <p className="subSection">Cơ sở vật chất</p>
          <Row gutter={[16, 16]}>
            {isEdit ? (
              (state?.infrastructures || dsTienIch)
                ?.filter((item: any) => item?.typeAttribute === AttributeType.INFRASTRUCTURE)
                ?.map((itemMap: any, index: number) => {
                  if (index % 2 === 1) {
                    return (
                      <Col xs={23} sm={23} md={23} lg={12} xl={12} key={index}>
                        <Form.Item name={itemMap?.id} valuePropName="checked">
                          <Checkbox
                            onChange={(e) => {
                              onHandleInfrastructure(itemMap?.id, e.target.checked);
                            }}
                            value={itemMap?.checked}
                            className="checkboxValue"
                          >
                            {itemMap?.name}
                          </Checkbox>
                        </Form.Item>
                      </Col>
                    );
                  } else if (index % 2 === 0) {
                    return (
                      <Col xs={23} sm={23} md={23} lg={12} xl={12} key={index}>
                        <Form.Item name={itemMap?.id} valuePropName="checked">
                          <Checkbox
                            onChange={(e) => {
                              onHandleInfrastructure(itemMap?.id, e.target.checked);
                            }}
                            className="checkboxValue"
                            value={itemMap?.checked}
                          >
                            {itemMap?.name}
                          </Checkbox>
                        </Form.Item>
                      </Col>
                    );
                  }
                })
            ) : (
              <ul>
                {(state?.infrastructures || dsTienIch)
                  ?.filter(
                    (item: any) => item?.typeAttribute === AttributeType.INFRASTRUCTURE && item?.checked === true
                  )
                  ?.map((itemMap: any, index: number) => {
                    return (
                      <li style={{ marginLeft: '25px' }} key={index}>
                        {itemMap?.name}
                      </li>
                    );
                  })}
              </ul>
            )}
          </Row>

          {/* <Row>
          <Col xs={23} sm={23} md={23} lg={12} xl={12}>
            <Form.Item name="temporaryRegistration" valuePropName="checked">
              <Checkbox className="checkboxValue">Đăng kí tạm trú</Checkbox>
            </Form.Item>
          </Col>

          <Col xs={23} sm={23} md={23} lg={12} xl={12}>
            <Form.Item name="fireProtection" valuePropName="checked">
              <Checkbox className="checkboxValue">Phòng cháy</Checkbox>
            </Form.Item>
          </Col>

          <Col xs={23} sm={23} md={23} lg={12} xl={12}>
            <Form.Item name="lightingSystem" valuePropName="checked">
              <Checkbox className="checkboxValue">Hệ thống chiếu sáng</Checkbox>
            </Form.Item>
          </Col>

          <Col xs={23} sm={23} md={23} lg={12} xl={12}>
            <Form.Item name="washingMachine" valuePropName="checked">
              <Checkbox className="checkboxValue">Máy giặt</Checkbox>
            </Form.Item>
          </Col>
        </Row> */}

          <p className="section">Ảnh phòng</p>
          <p className="subSection">Bao quát</p>
          {isEdit ? (
            <div>
              {youtubeLinks?.map((link: any, index: any) => (
                <div style={{ display: 'flex' }} key={index}>
                  <iframe
                    style={{ padding: '20px 0' }}
                    key={index}
                    src={`https://www.youtube.com/embed/${getVideoId(link)}`}
                    width="560"
                    height="315"
                    frameBorder="0"
                    allowFullScreen
                  />
                  <Button
                    onClick={() => handleDeleteVideo(index)}
                    type="text"
                    className="flex items-center justify-center m-5"
                    icon={<MinusCircleOutlined />}
                  />
                </div>
              ))}
              <div className="flex gap-2 pt-4 pb-4">
                <Input
                  onChange={handleInputChangeLinkVideo}
                  placeholder="Nhập liên kết YouTube"
                  style={{ width: '30%' }}
                />
                <Button onClick={embedVideo}>Hiển thị video</Button>
              </div>
            </div>
          ) : (
            <ul>
              {youtubeLinks.map((link: any, index: any) => (
                <li key={index}>{`https://www.youtube.com/embed/${getVideoId(link)}`}</li>
              ))}
            </ul>
          )}
          {isEdit ? (
            <Upload
              style={{ width: '100%' }}
              className="imgOverall"
              accept="image/*"
              listType="picture-card"
              fileList={fileListimgOverall}
              // onPreview={handlePreview}
              showUploadList={{
                showPreviewIcon: false,
                // showRemoveIcon: false,
              }}
              onChange={(e) => handleChangeUploadImage('imgOverall', e)}
              onRemove={handleRemoveFile('imgOverall')}
            >
              {fileListimgOverall?.length === 0 ? <PlusOutlined /> : null}
            </Upload>
          ) : (
            <div>
              {fileListimgOverall.map((item: any, index: any) => (
                <img key={index} src={item?.url} alt={item?.name} width={'10%'} height={'auto'}></img>
              ))}
            </div>
          )}
          <p className="subSection">Ảnh thuộc Album phòng trọ</p>
          <p>Ảnh phòng khách</p>
          {isEdit ? (
            <Upload
              accept="image/*"
              listType="picture-card"
              fileList={fileListimgLivingRooms}
              // onPreview={handlePreview}
              showUploadList={{
                showPreviewIcon: false,
                // showRemoveIcon: false,
              }}
              onChange={(e) => handleChangeUploadImage('imgLivingRooms', e)}
              onRemove={handleRemoveFile('imgLivingRooms')}
            >
              {fileListimgLivingRooms?.length <= 2 ? <PlusOutlined /> : null}
            </Upload>
          ) : (
            <div>
              {fileListimgLivingRooms.map((item: any, index: any) => (
                <img
                  style={{ marginRight: '10px' }}
                  key={index}
                  src={item?.url}
                  alt={item?.name}
                  width={'10%'}
                  height={'auto'}
                ></img>
              ))}
            </div>
          )}
          <p>Ảnh phòng ngủ</p>
          {isEdit ? (
            <Upload
              accept="image/*"
              listType="picture-card"
              fileList={fileListimgBedRooms}
              // onPreview={handlePreview}
              showUploadList={{
                showPreviewIcon: false,
                // showRemoveIcon: false,
              }}
              onChange={(e) => handleChangeUploadImage('imgBedRooms', e)}
              onRemove={handleRemoveFile('imgBedRooms')}
            >
              {fileListimgBedRooms?.length <= 2 ? <PlusOutlined /> : null}
            </Upload>
          ) : (
            <div>
              {fileListimgBedRooms.map((item: any, index: any) => (
                <img
                  style={{ marginRight: '10px' }}
                  key={index}
                  src={item?.url}
                  alt={item?.name}
                  width={'10%'}
                  height={'auto'}
                ></img>
              ))}
            </div>
          )}
          <p>Ảnh phòng vệ sinh</p>
          {isEdit ? (
            <Upload
              accept="image/*"
              listType="picture-card"
              fileList={fileListimgBathRooms}
              // onPreview={handlePreview}
              showUploadList={{
                showPreviewIcon: false,
                // showRemoveIcon: false,
              }}
              onChange={(e) => handleChangeUploadImage('imgBathRooms', e)}
              onRemove={handleRemoveFile('imgBathRooms')}
            >
              {fileListimgBathRooms?.length <= 2 ? <PlusOutlined /> : null}
            </Upload>
          ) : (
            <div>
              {fileListimgBathRooms.map((item: any, index: any) => (
                <img
                  style={{ marginRight: '10px' }}
                  key={index}
                  src={item?.url}
                  alt={item?.name}
                  width={'10%'}
                  height={'auto'}
                ></img>
              ))}
            </div>
          )}
          <p>Ảnh ban công</p>
          {isEdit ? (
            <Upload
              accept="image/*"
              listType="picture-card"
              fileList={fileListimgBalconys}
              // onPreview={handlePreview}
              showUploadList={{
                showPreviewIcon: false,
                // showRemoveIcon: false,
              }}
              onChange={(e) => handleChangeUploadImage('imgBalconys', e)}
              onRemove={handleRemoveFile('imgBalconys')}
            >
              {fileListimgBalconys?.length <= 2 ? <PlusOutlined /> : null}
            </Upload>
          ) : (
            <div>
              {fileListimgBalconys.map((item: any, index: any) => (
                <img
                  style={{ marginRight: '10px' }}
                  key={index}
                  src={item?.url}
                  alt={item?.name}
                  width={'10%'}
                  height={'auto'}
                ></img>
              ))}
            </div>
          )}
        </Form>
        {/* <p
              style={{ fontSize: "24px", lineHeight: "40px" }}
              className="font-bold"
            >
              Thông tin phòng cho thuê
            </p>
            <Divider /> */}
      </Modal>
      <ModalChild
        openModalRoom={openModalRoom}
        modalRoomStatus={state.modalRoomStatus}
        handleVisibleChangeModalRoom={handleVisibleChangeModalRoom}
        onHandleStatus={onHandleStatus}
      />
    </div>
  );
};

export default ModalRoomInfor;
