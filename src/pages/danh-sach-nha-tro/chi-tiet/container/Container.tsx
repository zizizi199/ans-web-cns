import {
  Button,
  Checkbox,
  Col,
  Divider,
  Form,
  Input,
  InputNumber,
  Row,
  Select,
  Spin,
  Upload,
  UploadFile,
  message,
} from 'antd';
import React, { FC, useEffect, useState } from 'react';
import { PlusOutlined, MinusCircleOutlined } from '@ant-design/icons';
import styles from './styles.module.scss';
import {
  AttributeType,
  ProductTypeArray,
  combineUrlParams,
  formatPrice,
  getVideoId,
  roomStatusType,
  today,
} from 'src/common';
import axiosIns, { HOST } from 'src/helpers/request';
import { HOME, SERVICE_API, UPLOAD } from 'src/helpers/api';
import { formValidateMessages } from 'src/pages/constants';
import ModalRoomInfor from '../components/ModalRoomInfor/ModalRoomInfor';
import { useNavigate, useParams } from 'react-router-dom';
import { HouseIcon, MoneyIcon, PreviousIcon } from 'src/components/Icon';
import { HostelRoutes } from 'src/helpers/app.routes';

interface Props {}

const MotelInforDetail: FC<Props> = () => {
  const [form] = Form.useForm();
  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingUpdate, setIsLoadingUpdate] = useState(false);
  const [visibleModal, setVisibleModal] = useState<any>(false);
  const [numFloor, setNumFloor] = useState<any>();
  const [numRoom, setNumRoom] = useState<any>();
  const [towerArr, setTowerArr] = useState<any>([]);
  const [tower, setTowerItem] = useState<any>();
  const [room, setRoomItem] = useState<any>();
  const [dsTienIch, setDsTienIch] = useState<any>([]);
  const [utilitiesItem, setUtilitiesItem] = useState<any>([]);
  const [chiTiet, setChiTiet] = useState<any>({});
  const [isFirstRender, setisFirstRender] = useState<any>(false);
  const [generalUtilitiesArr, setGeneralUtilitiesArr] = useState<any>([]);

  const [fileListimgOthers, setFileListimgOthers] = useState<UploadFile[]>([]);
  const [listimgOthers, setListimgOthers] = useState<any>([]);
  const [fileListimgOwners, setFileListimgOwners] = useState<UploadFile[]>([]);
  const [listimgOwners, setListimgOwners] = useState<any>([]);
  const [fileListimgOverall, setFileListimgOverall] = useState<UploadFile[]>([]);
  const [isLoadingUpload, setIsLoadingUpload] = useState(false);
  const [youtubeLinks, setYoutubeLinks] = useState<any>([]);
  const [youtubeLink, setYoutubeLink] = useState('');
  const matchParam = useParams();
  const [isEdit, setIsEdit] = useState<any>(false);
  const navigate = useNavigate();

  //gọi API chi tiết
  function fetchDetail() {
    return new Promise((resolve, reject) => {
      axiosIns
        .get(`${HOME.DETAIL}/${matchParam?.id}`)
        .then((s) => {
          setIsLoading(false);
          setChiTiet(s?.data);
          resolve(s?.data);
        })
        .catch((e) => {
          reject(e);
        });
    });
    // .then
  }

  //gọi API tiện ích
  function fetchService(params = {}) {
    return new Promise((resolve, reject) => {
      axiosIns
        .get(
          combineUrlParams(`${SERVICE_API.LIST}`, {
            limit: 10,
            pages: 1,
            ...params,
          })
        )
        .then((s) => {
          setDsTienIch(s?.data?.data);
          resolve(s?.data);
        })
        .catch((e) => {
          reject(e);
        });
    });
    // .then
  }

  //api cập nhật
  function fetchUpdate(input: any) {
    setIsLoadingUpdate(true);
    return new Promise((resolve, reject) => {
      axiosIns
        .put(`${HOME.UPDATE}/${matchParam?.id}`, { ...input })
        .then((s) => {
          if (s?.data?.code === 200) {
            setIsLoadingUpdate(false);
            resolve(s?.data);
            message.success('Cập nhật thành công');
            setTimeout(() => {
              navigate(HostelRoutes.list);
            }, 500);
          } else if (s?.data?.code >= 400) {
            message.error(s?.data?.message);
            setIsLoadingUpdate(false);
          }
        })
        .catch((e) => {
          if (e?.response?.data?.statusCode >= 400 || e?.response?.data?.code >= 400) {
            message.error(e?.response?.data?.message);
            setIsLoadingUpdate(false);
            reject(e);
          } else {
            reject(e);
          }
        });
    });
    // .then
  }

  useEffect(() => {
    if (matchParam !== undefined) {
      const isEdit: string = matchParam?.isEdit || '';
      setIsEdit(JSON.parse(isEdit));
    }
    fetchService();
    fetchDetail();
  }, []);

  // const debounceSearch = useDebounce(
  //   {
  //     numFloor: numFloor,
  //     numRoom: numRoom,
  //   },
  //   500
  // );totalFloor

  useEffect(() => {
    if (numFloor && numRoom && isFirstRender) {
      const data: any = towerArr;
      const currentRoom = data[0]?.rooms?.length;
      const allMonths = [
        {
          month: 1,
          status: 1,
          row: 1,
          price: 0,
        },
        {
          month: 2,
          status: 1,
          row: 1,
          price: 0,
        },
        {
          month: 3,
          status: 1,
          row: 1,
          price: 0,
        },
        {
          month: 4,
          status: 1,
          row: 1,
          price: 0,
        },
        {
          month: 5,
          status: 1,
          row: 2,
          price: 0,
        },
        {
          month: 6,
          status: 2,
          row: 2,
          price: 0,
        },
        {
          month: 7,
          status: 2,
          row: 2,
          price: 0,
        },
        {
          month: 8,
          status: 2,
          row: 2,
          price: 0,
        },
        {
          month: 9,
          status: 2,
          row: 3,
          price: 0,
        },
        {
          month: 10,
          status: 2,
          row: 3,
          price: 0,
        },
        {
          month: 11,
          status: 2,
          row: 3,
          price: 0,
        },
        {
          month: 12,
          status: 2,
          row: 3,
          price: 0,
        },
      ];

      const numYear = 3;
      const currentYear = today.getFullYear();

      const apartmentSale: any = Array.from({ length: numYear }, (_, k) => {
        const apartmentSale = {
          year: currentYear + k,
          allMonths: allMonths,
        };
        return apartmentSale;
      });

      //tầng lớn hơn tầng hiện tại
      if (numFloor > towerArr?.length) {
        const quantityToAdd = numFloor - towerArr?.length; // Số lượng giá trị cần thêm
        const startingFloor = data.length + 1; // Giá trị floor ban đầu
        const newEntries = Array.from({ length: quantityToAdd }, (_, index) => ({
          key: startingFloor + index,
          heading: `Tầng ${startingFloor + index}`,
          rooms: Array.from({ length: numRoom }, (_, id) => ({
            area: null,
            apartmentSales: apartmentSale?.map((sale: any) => {
              if (sale.year === today.getFullYear()) {
                const updatedMonths = sale.allMonths.map((month: any) => {
                  if (month.month >= today.getMonth() + 1) {
                    return { ...month, status: 2 };
                  } else {
                    return { ...month, status: 4 };
                  }
                });
                return { ...sale, allMonths: updatedMonths };
              } else if (sale.year > today.getFullYear()) {
                const updatedMonths = sale.allMonths.map((month: any) => {
                  return { ...month, status: 2 };
                });
                return { ...sale, allMonths: updatedMonths };
              }
              return sale;
            }),
            attributes: undefined,
            heading: id + 1 <= 9 ? `${startingFloor + index}0${id + 1}` : `${startingFloor + index}${id + 1}`,
            infrastructures: [],
            key: id + 1,
            isVerify: false,
            maxPeople: null,
            roomPrice: null,
            direction: null,
            imgOverall: null,
            listImgBalconys: null,
            listImgBathRooms: null,
            listImgBedRooms: null,
            listImgLivingRooms: null,
            videoLinks: [],
          })),
        }));
        const shadowTowerArr = [...data, ...newEntries];
        setTowerArr(shadowTowerArr);
      }
      //tầng nhỏ hơn tầng hiện tại
      else if (numFloor < towerArr?.length) {
        const shadowTowerArr = data.filter((item: any) => item?.key <= numFloor);
        setTowerArr(shadowTowerArr);
      }
      //phòng lớn hơn phòng hiện tại
      if (numRoom > currentRoom) {
        const quantityToAdd = numRoom - currentRoom; // Số lượng giá trị cần thêm
        const startingRoom = currentRoom + 1;

        const shadowTowerArr = data?.map((item: any) => {
          const addRooms = Array.from({ length: quantityToAdd }, (_, id) => ({
            area: null,
            apartmentSales: apartmentSale?.map((sale: any) => {
              if (sale.year === today.getFullYear()) {
                const updatedMonths = sale.allMonths.map((month: any) => {
                  if (month.month >= today.getMonth() + 1) {
                    return { ...month, status: 2 };
                  } else {
                    return { ...month, status: 4 };
                  }
                });
                return { ...sale, allMonths: updatedMonths };
              } else if (sale.year > today.getFullYear()) {
                const updatedMonths = sale.allMonths.map((month: any) => {
                  return { ...month, status: 2 };
                });
                return { ...sale, allMonths: updatedMonths };
              }
              return sale;
            }),
            attributes: undefined,
            heading: startingRoom + id <= 9 ? `${item?.key}0${startingRoom + id}` : `${item?.key}${startingRoom + id}`,
            infrastructures: [],
            key: startingRoom + id,
            isVerify: false,
            maxPeople: null,
            roomPrice: null,
            direction: null,
            imgOverall: null,
            listImgBalconys: null,
            listImgBathRooms: null,
            listImgBedRooms: null,
            listImgLivingRooms: null,
            videoLinks: [],
          }));
          const afterRoomArr = [...item.rooms, ...addRooms];
          return {
            ...item,
            rooms: afterRoomArr,
          };
        });
        setTowerArr(shadowTowerArr);
      }
      //phòng nhỏ hơn phòng hiện tại
      else if (numRoom < currentRoom) {
        const shadowTowerArr = data?.map((item: any) => {
          return {
            ...item,
            rooms: item?.rooms.filter((it: any) => it?.key <= numRoom),
          };
        });
        setTowerArr(shadowTowerArr);
      }
    }
  }, [numFloor, numRoom]);

  console.log(towerArr);

  useEffect(() => {
    if (Object?.keys(chiTiet).length !== 0) {
      // console.log(chiTiet);
      form.setFieldsValue({
        addressFull: `${chiTiet?.addressBuilding?.district?.name}, ${chiTiet?.addressBuilding?.province?.name}`,
        provinceId: chiTiet?.addressBuilding?.province?.id,
        districtId: chiTiet?.addressBuilding?.district?.id,
        wardId: chiTiet?.addressBuilding?.wards?.id,
        street: chiTiet?.addressBuilding?.street,
        address: chiTiet?.addressBuilding?.address,
        priceFromTo: `${formatPrice(chiTiet?.lowPrice)} - ${formatPrice(chiTiet?.highPrice)}`,
        roomOfFloor: chiTiet?.roomOfFloor,
        totalFloor: chiTiet?.totalFloor,
        productType: chiTiet?.productType,
        // videoLinks: chiTiet?.videoLinks?.map((item: any) => {
        //   return {
        //     videoLink: item,
        //   };
        // }),
      });
      setYoutubeLinks(chiTiet?.videoLinks || []);
      setNumFloor(chiTiet?.totalFloor);
      setNumRoom(chiTiet?.roomOfFloor);

      const generalUtilities = dsTienIch.map((item: any) => {
        const found = chiTiet?.generalUtilities.some((otherItem: any) => otherItem?.id === item?.id);
        return { ...item, checked: found };
      });

      generalUtilities
        ?.filter((item: any) => item?.typeAttribute === AttributeType.GENERAL_UTILITIES)
        ?.map((item: any) => {
          if (item?.checked) {
            form.setFieldsValue({
              [item?.id]: true,
            });
          } else {
            form.setFieldsValue({
              [item?.id]: false,
            });
          }
        });

      setGeneralUtilitiesArr(generalUtilities);

      const generalUtilitiesArr = chiTiet?.generalUtilities?.map((item: any) => item?.id);
      setUtilitiesItem(generalUtilitiesArr);

      const imgOwners: any = [];
      chiTiet?.imgOwners
        ?.filter((it: any) => it !== '')
        ?.map((item: any) => {
          const idx = item.lastIndexOf('/') + 1;
          const filename = item.substr(idx);

          imgOwners.push({
            uid: item,
            name: filename,
            status: 'done',
            url: item && `${HOST}${UPLOAD.GET}/${new URL(item).pathname.split('/').pop()}`,
            type: 'image/*',
            webkitRelativePath: '',
          });
        });
      setFileListimgOwners(imgOwners);
      setListimgOwners(chiTiet?.imgOwners || []);

      const imgOverall: any = [];
      const idx = chiTiet?.imgOverall?.lastIndexOf('/') + 1;
      const filename = chiTiet?.imgOverall?.substr(idx);

      if (chiTiet?.imgOverall) {
        imgOverall.push({
          uid: chiTiet?.imgOverall,
          name: filename,
          status: 'done',
          url: chiTiet?.imgOverall && `${HOST}${UPLOAD.GET}/${new URL(chiTiet?.imgOverall).pathname.split('/').pop()}`,
          type: 'image/*',
          webkitRelativePath: '',
        });
      }
      setFileListimgOverall(imgOverall);
      form.setFieldsValue({ imgOverall: chiTiet?.imgOverall });

      const imgOther: any = [];
      chiTiet?.imgOther
        ?.filter((it: any) => it !== '')
        ?.map((item: any) => {
          const idx = item.lastIndexOf('/') + 1;
          const filename = item.substr(idx);

          imgOther.push({
            uid: item,
            name: filename,
            status: 'done',
            url: item && `${HOST}${UPLOAD.GET}/${new URL(item).pathname.split('/').pop()}`,
            type: 'image/*',
            webkitRelativePath: '',
          });
        });
      setFileListimgOthers(imgOther);
      setListimgOthers(chiTiet?.imgOther || []);

      let data: any = [];
      const result: any = {};
      chiTiet?.apartments
        ?.filter((it: any) => it !== '')
        ?.map((item: any) => {
          const { floor } = item;
          if (!result[floor]) {
            result[floor] = { heading: `Tầng ${floor}`, key: floor, rooms: [] };
          }
          let apartmentSalesArr: any = [];
          const resultApartment: any = {};
          item?.apartmentSales?.map((item: any) => {
            const { year, month, status, row, price } = item;
            if (!resultApartment[year]) {
              resultApartment[year] = { year, allMonths: [] };
            }
            const itemArr = {
              month,
              status,
              row,
              price,
            };
            resultApartment[year].allMonths.push(itemArr);
            apartmentSalesArr = Object.values(resultApartment);
          });
          const itemArr = {
            area: item?.area,
            attributes: item?.attributes,
            apartmentSales: apartmentSalesArr,
            heading: item?.code,
            isVerify: item?.isVerify,
            infrastructures: item?.infrastructures,
            key: result[floor]?.rooms?.length + 1,
            maxPeople: item?.maxPeople,
            roomPrice: item?.price,
            direction: item?.direction,
            imgOverall: item?.imgOverall,
            listImgBalconys: item?.listImgBalconys,
            listImgBathRooms: item?.listImgBathRooms,
            listImgBedRooms: item?.listImgBedRooms,
            listImgLivingRooms: item?.listImgLivingRooms,
            videoLinks: item?.videoLinks,
          };
          result[floor].rooms.push(itemArr);
          data = Object.values(result);
          console.log(data);
        });

      setTowerArr(data);
    }
  }, [chiTiet]);

  const onHandleEditRoom = (
    value: any,
    apartmentSales: any,
    towerKey: number,
    roomKey: number,
    attributesValue: any,
    infrastructureItem: any,
    fileListimgLivingRooms: any,
    listImgLivingRooms: any,
    fileListimgBedRooms: any,
    listImgBedRooms: any,
    fileListimgBathRooms: any,
    listImgBathRooms: any,
    fileListimgBalconys: any,
    listImgBalconys: any,
    fileListimgOverall: any,
    imgOverallModal: any,
    videoLinks: any
  ) => {
    const data = towerArr;

    data[towerKey - 1].rooms[roomKey - 1] = {
      ...data[towerKey - 1].rooms[roomKey - 1],
      roomPrice: value.roomPrice || 0,
      area: value.area || 1,
      isVerify: true,
      maxPeople: value.maxPeople || 1,
      attributes: attributesValue,
      direction: value.direction,
      apartmentSales: apartmentSales,
      infrastructures: infrastructureItem,
      fileListimgLivingRooms: [...fileListimgLivingRooms],
      listImgLivingRooms: [...listImgLivingRooms],
      fileListimgBedRooms: [...fileListimgBedRooms],
      listImgBedRooms: [...listImgBedRooms],
      fileListimgBathRooms: [...fileListimgBathRooms],
      listImgBathRooms: [...listImgBathRooms],
      fileListimgBalconys: [...fileListimgBalconys],
      listImgBalconys: [...listImgBalconys],
      fileListimgOverall: [...fileListimgOverall],
      imgOverall: imgOverallModal,
      videoLinks: videoLinks,
    };
    setTowerArr(data);
  };

  // useEffect(() => {
  //   let value = {
  //     numFloor: numFloor,
  //     numRoom: numRoom,
  //   };
  // }, [numFloor, numRoom]);

  const onHandleUtilities = (id: any, value: any) => {
    let utilities: any = utilitiesItem;
    if (value === true) {
      utilities.push(id);
      setUtilitiesItem(utilities);
    } else {
      utilities = utilitiesItem?.filter((item: any) => item !== id);
      setUtilitiesItem(utilities);
    }
  };

  const onHandleTower = (type: string) => (e: any) => {
    setisFirstRender(true);
    if (type === 'floor') {
      setNumFloor(e);
    } else {
      setNumRoom(e);
    }
  };

  const onHandleVisibleModal = (value: boolean) => {
    setVisibleModal(value);
  };

  const handleClickCell = (towerItem: any, roomItem: any) => () => {
    console.log(roomItem);

    setVisibleModal(true);
    setTowerItem(towerItem);
    setRoomItem(roomItem);
  };

  //upload
  const handleChangeUploadImage = (type: string, data: any) => {
    debugger;
    setIsLoadingUpload(true);
    if (data?.file?.status !== 'removed') {
      const isLt2M = data?.file?.size / 1024 / 1024 < 10;
      if (!isLt2M) {
        message.error('Kích thước ảnh không được vượt quá 10Mb.');
        setIsLoadingUpload(false);
        return;
      }
    }

    if (type === 'imgOthers') {
      setFileListimgOthers(data?.fileList);
    } else if (type === 'imgOwners') {
      setFileListimgOwners(data?.fileList);
    } else if (type === 'imgOverall') {
      setFileListimgOverall(data?.fileList);
    }
    if (data?.file?.status === 'removed') {
      setIsLoadingUpload(false);
    }

    console.log(data?.file?.status);
    console.log(data);

    const formData: any = new FormData();
    const file = data?.file?.originFileObj;
    formData.append('file', file);
    if (data?.file?.status === 'error' || data?.file?.status === 'done') {
      return new Promise((resolve, reject) => {
        axiosIns
          .post(`${UPLOAD.SINGLE}`, formData, {
            headers: {
              'Content-Type': 'multipart/form-data',
            },
          })
          .then((s) => {
            setIsLoadingUpload(false);
            console.log('s', s);
            if (type === 'imgOthers') {
              const arr: any = [];
              arr.push(...listimgOthers, s?.data?.data?.fileUrl);
              setListimgOthers(arr);
            } else if (type === 'imgOwners') {
              const arr: any = [];
              arr.push(...listimgOwners, s?.data?.data?.fileUrl);
              setListimgOwners(arr);
            } else {
              form.setFieldsValue({
                [type]: s?.data?.data?.fileUrl,
              });
            }
            // let image = s?.data?.data?.transactionImageInfors.map(
            //   (item: any) => item.fileUrl
            // );
            // form.setFieldsValue({ imageUrls: image });
          })
          .catch((e) => {
            setIsLoadingUpload(false);
            reject(e);
          });
      });
    } else {
      return;
    }
  };

  const handleRemoveFile = (type: string) => (data: any) => {
    if (type === 'imgOthers') {
      const index = fileListimgOthers?.findIndex((item: any) => item?.uid === data?.uid);
      const arr = listimgOthers;
      arr.splice(index, 1);
      const _arr = [...arr];

      setListimgOthers(_arr);
    } else if (type === 'imgOwners') {
      const index = fileListimgOwners?.findIndex((item: any) => item?.uid === data?.uid);
      const arr = listimgOwners;
      arr.splice(index, 1);
      const _arr = [...arr];

      setListimgOwners(_arr);
    } else if (type === 'imgOverall') {
      form.setFieldsValue({
        imgOverall: undefined,
      });
    }
  };

  const onSubmit = () => {
    const towerArrSubmit = towerArr?.map((item: any) => {
      return {
        ...item,
        rooms: item.rooms?.map((it: any) => {
          return {
            ...it,
            infrastructures: it?.infrastructures?.map((infrastructuresItem: any) => {
              if (infrastructuresItem?.id) {
                return infrastructuresItem?.id;
              } else {
                return infrastructuresItem;
              }
            }),
            attributes: it?.attributes?.map((attributesItem: any) => {
              return {
                id: attributesItem?.id,
                typeAttribute: attributesItem?.typeAttribute,
                unit: attributesItem?.unit,
                status: attributesItem?.status,
                name: attributesItem?.name,
                price: attributesItem?.value,
              };
            }),
          };
        }),
      };
    });
    const input = {
      totalFloor: form.getFieldValue('totalFloor') || 0,
      roomOfFloor: form.getFieldValue('roomOfFloor') || 0,
      productType: form.getFieldValue('productType'),
      provinceId: form.getFieldValue('provinceId'),
      districtId: form.getFieldValue('districtId'),
      wardId: form.getFieldValue('wardId'),
      street: form.getFieldValue('street')?.trim(),
      address: form.getFieldValue('address')?.trim(),
      // videoLinks: form.getFieldValue("videoLinks")?.map((item: any) => {
      //   return item["videoLink"]?.trim();
      // }),
      videoLinks: youtubeLinks,
      generalUtilities: utilitiesItem,
      floors: towerArrSubmit?.map((item: any) => {
        return {
          ...item,
          rooms: item?.rooms?.map((itemRoom: any) => {
            return {
              apartmentSales: itemRoom?.apartmentSales,
              area: itemRoom?.area,
              attributes: itemRoom?.attributes,
              direction: itemRoom?.direction,
              heading: itemRoom?.heading,
              imgOverall: itemRoom?.imgOverall,
              infrastructures: itemRoom?.infrastructures,
              isVerify: itemRoom?.isVerify,
              key: itemRoom?.key,
              listImgBathRooms: itemRoom?.listImgBathRooms,
              listImgBalconys: itemRoom?.listImgBalconys,
              listImgBedRooms: itemRoom?.listImgBedRooms,
              listImgLivingRooms: itemRoom?.listImgLivingRooms,
              maxPeople: itemRoom?.maxPeople,
              videoLinks: itemRoom?.videoLinks,
            };
          }),
        };
      }),
      imgOwners: listimgOwners,
      imgOthers: listimgOthers,
      imgOverall: form.getFieldValue('imgOverall'),
    };
    if (
      !form.getFieldValue('provinceId') ||
      !form.getFieldValue('districtId') ||
      !form.getFieldValue('wardId') ||
      !form.getFieldValue('street') ||
      !form.getFieldValue('address') ||
      form.getFieldValue('street')?.length === 0 ||
      form.getFieldValue('address')?.length === 0 ||
      !form.getFieldValue('totalFloor') ||
      !form.getFieldValue('roomOfFloor') ||
      !form.getFieldValue('productType')
    ) {
      message?.error('Vui lòng nhập đủ các trường thông tin còn thiếu');
      return;
    }
    if (fileListimgOthers?.length === 0 || fileListimgOverall?.length === 0) {
      if (fileListimgOthers?.length === 0) {
        message.error('Còn thiếu ảnh thuộc Album tòa nhà');
      }
      if (fileListimgOverall?.length === 0) {
        message.error('Còn thiếu ảnh bao quát');
      }
    } else if (fileListimgOthers?.length < 4) {
      message.error('Cần tải lên ít nhất 4 ảnh thuộc Album tòa nhà');
    } else {
      fetchUpdate(input);
    }
  };

  const handleInputChangeLinkVideo = (event: any) => {
    const link = event.target.value;
    setYoutubeLink(link);
  };

  const embedVideo = () => {
    // Kiểm tra xem đầu vào có phải là liên kết YouTube hợp lệ hay không
    const regExp =
      /^(?:https?:\/\/)?(?:www\.)?(?:youtube\.com\/(?:watch\?v=|embed\/|v\/|channels\/(?:\w+\/)?|user\/\w+|user\/(?:\w+\/)?\/user\/\w+)|youtu\.be\/)([a-zA-Z0-9_-]{11})/;
    const match = youtubeLink.match(regExp);
    if (match) {
      setYoutubeLinks((prevLinks: any) => [...prevLinks, youtubeLink]);
    } else {
      message.error('Link video không đúng định dạng');
    }
  };

  const handleDeleteVideo = (index: number) => {
    setYoutubeLinks(youtubeLinks.filter((item: any, i: number) => i !== index));
  };
  console.log(fileListimgOverall);

  return (
    <>
      <Spin spinning={isLoading || isLoadingUpdate}>
        <div className="container p-10">
          <div className={styles.container}>
            <Form
              validateMessages={formValidateMessages}
              form={form}
              layout="vertical"
              // onFinish={onSubmit}
            >
              <p style={{ fontSize: '24px', lineHeight: '40px' }} className="font-bold">
                <PreviousIcon
                  style={{ marginTop: '5px', cursor: 'pointer' }}
                  onClick={() => navigate(HostelRoutes.list)}
                />{' '}
                Chi tiết phòng cho thuê
              </p>
              <Divider />
              <p
                style={{
                  fontWeight: 700,
                  fontSize: '20px',
                  lineHeight: '40px',
                  color: '#f89420',
                }}
              >
                THÔNG TIN CHUNG
              </p>
              <Row gutter={[16, 0]}>
                <Col xs={23} sm={23} md={23} lg={8} xl={6}>
                  <Form.Item name="addressFull" label="Địa chỉ">
                    <Input disabled />
                  </Form.Item>
                </Col>
                <Col xs={23} sm={23} md={23} lg={8} xl={6}>
                  <Form.Item name="totalFloor" label="Số tầng">
                    <InputNumber disabled onChange={onHandleTower('floor')} placeholder="Nhập số tầng" min={1} />
                  </Form.Item>
                </Col>
                <Col xs={23} sm={23} md={23} lg={8} xl={6}>
                  <Form.Item name="priceFromTo" label="Diện tích">
                    <Input disabled />
                  </Form.Item>
                </Col>
                <Col xs={23} sm={23} md={23} lg={8} xl={6}>
                  <Form.Item name="priceFromTo" label="Giá phòng">
                    <Input disabled />
                  </Form.Item>
                </Col>
                <Col xs={23} sm={23} md={23} lg={8} xl={6}>
                  <Form.Item name="productType" label="Loại phòng">
                    <Select disabled placeholder="Chọn loại phòng">
                      {ProductTypeArray?.map((item: any) => (
                        <Select.Option value={item?.key} key={item?.key}>
                          {item?.label}
                        </Select.Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={23} sm={23} md={23} lg={8} xl={6}>
                  <Form.Item name="priceFromTo" label="Trạng thái">
                    <Input />
                  </Form.Item>
                </Col>
                <Col xs={23} sm={23} md={23} lg={8} xl={6}>
                  <Form.Item name="timeOpen" label="Thời gian mở bán (tháng)">
                    <InputNumber min={0} step={1} />
                  </Form.Item>
                </Col>
                <Col xs={23} sm={23} md={23} lg={8} xl={6}>
                  <Form.Item name="timeOpen" label="Hành động">
                    <InputNumber min={0} step={1} />
                  </Form.Item>
                </Col>
              </Row>
              <p className="section" style={{ color: '#f89420' }}>
                THÔNG TIN NHÀ TRỌ
              </p>
              <div className="tableTower">
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'flex-end',
                    gap: '8px',
                    padding: '0 10px 10px 10px',
                    fontSize: '12px',
                  }}
                >
                  <div className="circle" style={{ backgroundColor: '#B2B5BC' }}></div>
                  <p>Đã cho thuê</p>
                  <div className="circle" style={{ backgroundColor: '#C7F09E' }}></div>
                  <p>Đã mở bán</p>
                  <div className="circle" style={{ backgroundColor: '#C8D8FF' }}></div>
                  <p>Chưa mở bán</p>
                  <div className="circle" style={{ backgroundColor: '#FFCCC9' }}></div>
                  <p>Không được mở bán</p>
                </div>
                <div>
                  <table
                    style={{
                      maxWidth: '1275px',
                      width: '100%',
                      minWidth: '100%',
                      maxHeight: '300px',
                      display: 'block',
                    }}
                    // className="table-auto"
                  >
                    {towerArr.map((item: any, index: number) => (
                      <tr
                        // className="border bg-green-500 text-center font-bold text-white py-3 px-4"
                        key={index}
                      >
                        <th>{item.heading}</th>
                        {item?.rooms?.map((roomItem: any, roomIndex: number) => {
                          // console.log(
                          //   roomItem?.apartmentSales?.find(
                          //     (item: any) =>
                          //       item?.month === today.getMonth() + 1 &&
                          //       item?.year === today.getFullYear()
                          //   )?.status
                          // );

                          return (
                            <td
                              // className="border py-2 px-4"
                              style={{
                                backgroundColor:
                                  roomItem?.apartmentSales
                                    ?.find((item: any) => item?.year === today.getFullYear())
                                    ?.allMonths?.find((itemMonth: any) => itemMonth?.month === today.getMonth() + 1)
                                    ?.status === roomStatusType.RENTED
                                    ? '#C8CBD0'
                                    : roomItem?.apartmentSales
                                        ?.find((item: any) => item?.year === today.getFullYear())
                                        ?.allMonths?.find((itemMonth: any) => itemMonth?.month === today.getMonth() + 1)
                                        ?.status === roomStatusType.NOT_SOLD
                                    ? '#FFCCC9'
                                    : roomItem?.apartmentSales
                                        ?.find((item: any) => item?.year === today.getFullYear())
                                        ?.allMonths?.find((itemMonth: any) => itemMonth?.month === today.getMonth() + 1)
                                        ?.status === roomStatusType.NOT_SOLD_YET
                                    ? '#C8D8FF'
                                    : '#C7F09E',
                              }}
                              onClick={handleClickCell(item, roomItem)}
                              key={roomIndex}
                            >
                              <div className="underlineDiv">
                                <p className="underlineText">{roomItem.heading}</p>
                                {/* {roomItem.isVerify === true ? (
                                  <img src={'/img/icon/isVerify.png'} alt={'isVerify Icon'} width={21} height={21} />
                                ) : (
                                  <img
                                    src={'/img/icon/isUnVerify.png'}
                                    alt={'isUnVerify Icon'}
                                    width={21}
                                    height={21}
                                  />
                                )} */}
                              </div>
                              <div>
                                <div style={{ display: 'flex' }}>
                                  <MoneyIcon />
                                  <span className="pl-3">
                                    {formatPrice(
                                      roomItem?.apartmentSales
                                        ?.find((item: any) => item?.year === today.getFullYear())
                                        ?.allMonths?.find((itemMonth: any) => itemMonth?.month === today.getMonth() + 1)
                                        ?.price
                                    )}
                                  </span>
                                </div>
                                <div style={{ display: 'flex' }}>
                                  <HouseIcon />
                                  <span className="pl-3">{roomItem.area}</span>
                                </div>
                              </div>
                            </td>
                          );
                        })}
                      </tr>
                    ))}
                  </table>
                </div>
              </div>

              <p className="section">Tiện ích chung</p>
              <div className="flex">
                <Row gutter={[16, 16]}>
                  {isEdit ? (
                    (generalUtilitiesArr || dsTienIch)
                      ?.filter((item: any) => item?.typeAttribute === AttributeType.GENERAL_UTILITIES)
                      ?.map((itemMap: any, indexMap: number) => {
                        if (indexMap % 2 === 1) {
                          return (
                            <Col xs={23} sm={23} md={23} lg={12} xl={12} key={indexMap}>
                              <Form.Item name={itemMap?.id} valuePropName="checked">
                                <Checkbox
                                  onChange={(e) => {
                                    onHandleUtilities(itemMap?.id, e.target.checked);
                                  }}
                                  className="checkboxValue"
                                  value={itemMap?.checked}
                                >
                                  {itemMap?.name}
                                </Checkbox>
                              </Form.Item>
                            </Col>
                          );
                        } else if (indexMap % 2 === 0) {
                          return (
                            <Col xs={23} sm={23} md={23} lg={12} xl={12} key={indexMap}>
                              <Form.Item name={itemMap?.id} valuePropName="checked">
                                <Checkbox
                                  onChange={(e) => {
                                    onHandleUtilities(itemMap?.id, e.target.checked);
                                  }}
                                  className="checkboxValue"
                                  value={itemMap?.checked}
                                >
                                  {itemMap?.name}
                                </Checkbox>
                              </Form.Item>
                            </Col>
                          );
                        }
                      })
                  ) : (
                    <ul>
                      {(generalUtilitiesArr || dsTienIch)
                        ?.filter(
                          (item: any) =>
                            item?.typeAttribute === AttributeType.GENERAL_UTILITIES && item?.checked === true
                        )
                        ?.map((itemMap: any, indexMap: number) => {
                          return (
                            <li style={{ marginLeft: '25px' }} key={indexMap}>
                              {itemMap?.name}
                            </li>
                          );
                        })}
                    </ul>
                  )}
                </Row>
              </div>
              <div>
                <p
                  style={{
                    fontWeight: 700,
                    fontSize: '20px',
                    lineHeight: '40px',
                    color: '#f89420',
                  }}
                >
                  GIẤY CHỨNG NHẬN SỞ HỮU
                </p>
                {isEdit ? (
                  <Upload
                    accept="image/*"
                    listType="picture-card"
                    fileList={fileListimgOwners}
                    // onPreview={handlePreview}
                    showUploadList={{
                      showPreviewIcon: false,
                      // showRemoveIcon: false,
                    }}
                    onChange={(e) => handleChangeUploadImage('imgOwners', e)}
                    onRemove={handleRemoveFile('imgOwners')}
                  >
                    {fileListimgOwners?.length <= 2 ? <PlusOutlined /> : null}
                  </Upload>
                ) : (
                  <div>
                    {fileListimgOwners.map((item: any, index: any) => (
                      <img key={index} src={item?.url} alt={item?.name} width={'10%'} height={'auto'}></img>
                    ))}
                  </div>
                )}
              </div>

              <p
                style={{
                  fontWeight: 700,
                  fontSize: '20px',
                  lineHeight: '40px',
                  color: '#f89420',
                }}
              >
                ẢNH PHÒNG
              </p>
              <p className="section">Bao quát</p>
              {isEdit ? (
                <div>
                  {youtubeLinks.map((link: any, index: any) => (
                    <div className="flex" key={Math.random()}>
                      <iframe
                        style={{ padding: '20px 0' }}
                        key={index}
                        src={`https://www.youtube.com/embed/${getVideoId(link)}`}
                        width="560"
                        height="315"
                        frameBorder="0"
                        allowFullScreen
                      />
                      <Button
                        onClick={() => handleDeleteVideo(index)}
                        type="text"
                        className="flex items-center justify-center m-5"
                        icon={<MinusCircleOutlined />}
                      />
                    </div>
                  ))}
                  <div className="flex gap-2 pt-4 pb-4">
                    <Input
                      onChange={handleInputChangeLinkVideo}
                      placeholder="Nhập liên kết YouTube"
                      style={{ width: '30%' }}
                    />
                    <Button onClick={embedVideo}>Hiển thị video</Button>
                  </div>
                </div>
              ) : (
                <ul>
                  {youtubeLinks.map((link: any, index: any) => (
                    <li key={index}>{`https://www.youtube.com/embed/${getVideoId(link)}`}</li>
                  ))}
                </ul>
              )}

              {isEdit ? (
                <Upload
                  style={{ width: '100%' }}
                  className="imgOverall"
                  accept="image/*"
                  listType="picture-card"
                  fileList={fileListimgOverall}
                  // onPreview={handlePreview}
                  showUploadList={{
                    showPreviewIcon: false,
                    // showRemoveIcon: false,
                  }}
                  onChange={(e) => handleChangeUploadImage('imgOverall', e)}
                  onRemove={handleRemoveFile('imgOverall')}
                >
                  {fileListimgOverall?.length === 0 ? <PlusOutlined /> : null}
                </Upload>
              ) : (
                <div>
                  {fileListimgOverall.map((item: any, index: any) => (
                    <img key={index} src={item?.url} alt={item?.name} width={'10%'} height={'auto'}></img>
                  ))}
                </div>
              )}
              <p className="section">Ảnh thuộc Album tòa nhà</p>
              {isEdit ? (
                <Upload
                  accept="image/*"
                  listType="picture-card"
                  fileList={fileListimgOthers}
                  // onPreview={handlePreview}
                  showUploadList={{
                    showPreviewIcon: false,
                    // showRemoveIcon: false,
                  }}
                  // removeIcon={null}
                  onChange={(e) => handleChangeUploadImage('imgOthers', e)}
                  onRemove={handleRemoveFile('imgOthers')}
                >
                  <PlusOutlined />
                </Upload>
              ) : (
                <div>
                  {fileListimgOthers.map((item: any, index: any) => (
                    <img
                      style={{ marginRight: '10px' }}
                      key={index}
                      src={item?.url}
                      alt={item?.name}
                      width={'10%'}
                      height={'auto'}
                    ></img>
                  ))}
                </div>
              )}
              <ModalRoomInfor
                // form={form}
                visibleModal={visibleModal}
                onHandleVisibleModal={onHandleVisibleModal}
                towerItem={tower}
                roomItem={room}
                onHandleEditRoom={onHandleEditRoom}
                dsTienIch={dsTienIch}
                isEdit={isEdit}
              />

              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <Form.Item>
                  <Button
                    loading={isLoadingUpload}
                    style={{
                      alignItems: 'center',
                      padding: '12px 24px',
                      gap: '10px',
                      width: '150px ',
                      height: '48px',
                      background: '#C8CBD0',
                      borderRadius: '48px',
                      color: 'white',
                      marginRight: '10px',
                    }}
                    onClick={() => navigate(HostelRoutes.list)}
                  >
                    Đóng
                  </Button>
                  {isEdit && (
                    <Button
                      htmlType="submit"
                      loading={isLoadingUpload}
                      style={{
                        alignItems: 'center',
                        padding: '12px 24px',
                        gap: '10px',
                        width: '150px',
                        height: '48px',
                        background: '#F89420',
                        borderRadius: '48px',
                        color: 'white',
                      }}
                      onClick={onSubmit}
                    >
                      Cập nhật
                    </Button>
                  )}
                </Form.Item>
              </div>
            </Form>
          </div>
        </div>
      </Spin>
    </>
  );
};

export default MotelInforDetail;
