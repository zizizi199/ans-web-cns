'use client';

import { Button, Checkbox, Col, Form, Input, InputNumber, Modal, Row, Select, Upload, UploadFile, message } from 'antd';
import React, { useLayoutEffect, useState } from 'react';
import { PlusOutlined, MinusCircleOutlined } from '@ant-design/icons';
import styles from './styles.module.scss';
import { AttributeType, RoomDirectionArray, formatPrice, getVideoId, roomStatus, today } from 'src/common';
import axiosIns from 'src/helpers/request';
import { UPLOAD } from 'src/helpers/api';
import { formValidateMessages } from 'src/pages/constants';
import ModalChild from '../ModalChild/ModalChild';
import { EditRoomIcon, MoneyRoomIcon, StatusRoomIcon } from 'src/components/Icon';

interface Props {
  visibleModal: boolean;
  towerItem: any;
  roomItem: any;
  dsTienIch: any;
  onHandleVisibleModal: (value: boolean) => void;
  onHandleEditRoom: (
    value: any,
    apartmentSales: any,
    towerKey: number,
    roomKey: number,
    attributesValue: any,
    infrastructureItem: any,
    fileListimgLivingRooms: any,
    listimgLivingRooms: any,
    fileListimgBedRooms: any,
    listimgBedRooms: any,
    fileListimgBathRooms: any,
    listimgBathRooms: any,
    fileListimgBalconys: any,
    listimgBalconys: any,
    fileListimgOverall: any,
    imgOverallModal: any,
    videoLinks: any
  ) => void;
}

const ModalRoomInfor: React.FC<Props> = ({
  visibleModal,
  towerItem,
  roomItem,
  dsTienIch,
  onHandleVisibleModal,
  onHandleEditRoom,
}) => {
  const [form] = Form.useForm();
  const [apartmentSales, setApartmentSales] = useState<any>([]);
  const [openModalRoom, setOpenModalRoom] = useState<boolean>(false);
  const [infrastructureItem, setInfrastructureItem] = useState<any>([]);
  const [selectedYear, setSelectedYear] = useState<number>(0);
  const [state, _setState] = useState<any>({});
  const [fileListimgLivingRooms, setFileListimgLivingRooms] = useState<UploadFile[]>([]);
  const [listimgLivingRooms, setListimgLivingRooms] = useState<any>([]);

  const [fileListimgBedRooms, setFileListimgBedRooms] = useState<UploadFile[]>([]);
  const [listimgBedRooms, setListimgBedRooms] = useState<any>([]);

  const [fileListimgBathRooms, setFileListimgBathRooms] = useState<UploadFile[]>([]);
  const [listimgBathRooms, setListimgBathRooms] = useState<any>([]);

  const [fileListimgBalconys, setFileListimgBalconys] = useState<UploadFile[]>([]);
  const [listimgBalconys, setListimgBalconys] = useState<any>([]);

  const [fileListimgOverall, setFileListimgOverall] = useState<UploadFile[]>([]);
  const [youtubeLinks, setYoutubeLinks] = useState<any>([]);
  const [youtubeLink, setYoutubeLink] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const setState = (_state: any) => {
    _setState((state: any) => ({
      ...state,
      ...(_state || {}),
    }));
  };

  useLayoutEffect(() => {
    if (visibleModal === true) {
      form.setFieldsValue({
        roomPrice: roomItem?.roomPrice,
        area: roomItem?.area,
        maxPeople: roomItem?.maxPeople,
        attributes: roomItem?.attributes,
        direction: roomItem?.direction,
        apartmentSales: roomItem?.apartmentSales,
        imgOverallModal: roomItem?.imgOverall,
        // videoLinks: roomItem?.videoLinks?.map((item: any) => {
        //   return {
        //     videoLink: item,
        //   };
        // }),
      });
      setYoutubeLinks(roomItem?.videoLinks);
      roomItem?.attributes?.map((item: any) => {
        form.setFieldsValue({
          [item?.id]: item?.price || 0,
        });
      });
      roomItem?.infrastructures?.map((item: any) => {
        form.setFieldsValue({
          [item]: true,
        });
      });
      setApartmentSales(roomItem?.apartmentSales);
      setInfrastructureItem(roomItem?.infrastructures);

      setState({
        attributes: roomItem?.attributes,
      });
      // Ảnh
      setFileListimgLivingRooms(roomItem?.fileListimgLivingRooms);
      setListimgLivingRooms(roomItem?.listImgLivingRooms);
      setFileListimgBedRooms(roomItem?.fileListimgBedRooms);
      setListimgBedRooms(roomItem?.listImgBedRooms);
      setFileListimgBathRooms(roomItem?.fileListimgBathRooms);
      setListimgBathRooms(roomItem?.listImgBathRooms);
      setFileListimgBalconys(roomItem?.fileListimgBalconys);
      setListimgBalconys(roomItem?.listImgBalconys);
      setFileListimgOverall(roomItem?.fileListimgOverall);
    }
  }, [towerItem, roomItem, visibleModal]);

  const handleVisibleChangeModalRoom = (flag: any) => {
    setOpenModalRoom(flag);
  };

  const onHandleAttribute = (id: any, value: any) => {
    let data = (state.attributes?.length > 0 ? state.attributes : dsTienIch)?.filter(
      (item: any) => item?.typeAttribute === AttributeType.SERVICE
    );
    data = data?.map((item: any) => {
      if (item?.id === id) {
        return {
          id: item?.id,
          name: item?.name,
          price: value,
          typeAttribute: item?.typeAttribute,
          unit: item?.unit,
        };
      }
      return {
        id: item?.id,
        name: item?.name,
        price: item?.price || 0,
        typeAttribute: item?.typeAttribute,
        unit: item?.unit,
      };
    });
    setState({
      attributes: data,
    });

    form.setFieldsValue({
      attributes: data,
    });
  };

  const onHandleStatus = (year: number, month: number, status: number, price: number) => {
    setOpenModalRoom(false);
    const arrApartmentSales = apartmentSales?.map((item: any) => {
      if (item?.year === year) {
        return {
          ...item,
          allMonths: item?.allMonths?.map((itemMonth: any) => {
            if (itemMonth?.month === month) {
              return {
                ...itemMonth,
                status: status,
                price: price,
              };
            }
            return itemMonth;
          }),
        };
      }
      return item;
    });
    setApartmentSales(arrApartmentSales);
    form.setFieldsValue({
      apartmentSales: arrApartmentSales,
    });
  };

  const onHandleModalRoom = (year: number, month: number, status: number, price: number) => () => {
    setState({
      modalRoomStatus: {
        year: year,
        month: month,
        status: status,
        price: price,
      },
    });
    setOpenModalRoom(true);
  };

  const onHandleInfrastructure = (id: any, value: any) => {
    let infrastructure: any = [];
    if (value === true) {
      infrastructure.push(...infrastructureItem, id);
      setInfrastructureItem(infrastructure);
    } else {
      infrastructure = infrastructureItem?.filter((item: any) => item !== id);
      setInfrastructureItem(infrastructure);
    }
  };

  const handleChangeUploadImage = (type: string, data: any) => {
    setIsLoading(true);
    const isLt2M = data?.file?.size / 1024 / 1024 < 10;
    if (!isLt2M) {
      message.error('Kích thước ảnh không được vượt quá 10Mb.');
      setIsLoading(false);
      return;
    }
    // if (data?.file?.status === "error") {
    if (type === 'imgLivingRooms') {
      setFileListimgLivingRooms(data?.fileList);
    } else if (type === 'imgBedRooms') {
      setFileListimgBedRooms(data?.fileList);
    } else if (type === 'imgBathRooms') {
      setFileListimgBathRooms(data?.fileList);
    } else if (type === 'imgBalconys') {
      setFileListimgBalconys(data?.fileList);
    } else if (type === 'imgOverall') {
      setFileListimgOverall(data?.fileList);
    }
    if (data?.file?.status === 'removed') {
      setIsLoading(false);
    }
    const formData: any = new FormData();
    const file = data?.file?.originFileObj;
    formData.append('file', file);
    if (data?.file?.status === 'error' || data?.file?.status === 'done') {
      return new Promise((resolve, reject) => {
        axiosIns
          .post(`${UPLOAD.SINGLE}`, formData, {
            headers: {
              'Content-Type': 'multipart/form-data',
            },
          })
          .then((s) => {
            setIsLoading(false);
            if (type === 'imgLivingRooms') {
              setListimgLivingRooms((prevList: any) => [...prevList, s?.data?.data?.fileUrl]);
            } else if (type === 'imgBedRooms') {
              setListimgBedRooms((prevList: any) => [...prevList, s?.data?.data?.fileUrl]);
            } else if (type === 'imgBathRooms') {
              setListimgBathRooms((prevList: any) => [...prevList, s?.data?.data?.fileUrl]);
            } else if (type === 'imgBalconys') {
              setListimgBalconys((prevList: any) => [...prevList, s?.data?.data?.fileUrl]);
            } else if (type === 'imgOverall') {
              form.setFieldsValue({
                imgOverallModal: s?.data?.data?.fileUrl,
              });
            }
          })
          .catch((e) => {
            setIsLoading(false);
            reject(e);
          });
      });
    }
  };

  const handleRemoveFile = (type: string) => (data: any) => {
    console.log('listimgLivingRooms2', listimgLivingRooms);
    if (type === 'imgLivingRooms') {
      const index = fileListimgLivingRooms?.findIndex((item: any) => item?.uid === data?.uid);
      const arr = listimgLivingRooms;
      arr.splice(index, 1);
      const _arr = [...arr];

      setListimgLivingRooms(_arr);
    } else if (type === 'imgBedRooms') {
      const index = fileListimgBedRooms?.findIndex((item: any) => item?.uid === data?.uid);
      const arr = listimgBedRooms;
      arr.splice(index, 1);
      const _arr = [...arr];
      setListimgBedRooms(_arr);
    } else if (type === 'imgBathRooms') {
      const index = fileListimgBathRooms?.findIndex((item: any) => item?.uid === data?.uid);
      const arr = listimgBathRooms;
      arr.splice(index, 1);
      const _arr = [...arr];
      setListimgBathRooms(_arr);
    } else if (type === 'imgBalconys') {
      const index = fileListimgBalconys?.findIndex((item: any) => item?.uid === data?.uid);
      const arr = listimgBalconys;
      arr.splice(index, 1);
      const _arr = [...arr];
      setListimgBalconys(_arr);
    } else if (type === 'imgOverall') {
      form.setFieldsValue({
        imgOverallModal: undefined,
      });
    }
  };

  const handleCancel = () => {
    onHandleVisibleModal(false);
    // form.resetFields();
  };

  const onSubmit = () => {
    if (
      fileListimgLivingRooms?.length === 0 ||
      fileListimgBedRooms?.length === 0 ||
      fileListimgBathRooms?.length === 0 ||
      fileListimgBalconys?.length === 0 ||
      fileListimgOverall?.length === 0
    ) {
      if (fileListimgLivingRooms?.length === 0) {
        message.error('Còn thiếu ảnh phòng khách');
      }
      if (fileListimgBedRooms?.length === 0) {
        message.error('Còn thiếu ảnh phòng ngủ');
      }
      if (fileListimgBathRooms?.length === 0) {
        message.error('Còn thiếu ảnh phòng vệ sinh');
      }
      if (fileListimgBalconys?.length === 0) {
        message.error('Còn thiếu ảnh ban công');
      }
      if (fileListimgOverall?.length === 0) {
        message.error('Còn thiếu bao quát');
      }
    } else {
      onHandleVisibleModal(false);
      onHandleEditRoom(
        form.getFieldsValue(),
        apartmentSales,
        towerItem.key,
        roomItem.key,
        form.getFieldValue('attributes'),
        infrastructureItem,
        fileListimgLivingRooms,
        listimgLivingRooms,
        fileListimgBedRooms,
        listimgBedRooms,
        fileListimgBathRooms,
        listimgBathRooms,
        fileListimgBalconys,
        listimgBalconys,
        fileListimgOverall,
        form.getFieldValue('imgOverallModal'),
        youtubeLinks
        // form.getFieldValue("videoLinks")?.map((item: any) => {
        //   return item["videoLink"]?.trim();
        // })
      );
    }
  };

  const handleInputChangeLinkVideo = (event: any) => {
    const link = event.target.value;
    setYoutubeLink(link);
  };

  const embedVideo = () => {
    // Kiểm tra xem đầu vào có phải là liên kết YouTube hợp lệ hay không
    const regExp =
      /^(?:https?:\/\/)?(?:www\.)?(?:youtube\.com\/(?:watch\?v=|embed\/|v\/|channels\/(?:\w+\/)?|user\/\w+|user\/(?:\w+\/)?\/user\/\w+)|youtu\.be\/)([a-zA-Z0-9_-]{11})/;
    const match = youtubeLink.match(regExp);
    if (match) {
      setYoutubeLinks((prevLinks: any) => [...prevLinks, youtubeLink]);
    } else {
      message.error('Link video không đúng định dạng');
    }
  };

  const handleDeconsteVideo = (index: number) => {
    setYoutubeLinks(youtubeLinks.filter((item: any, i: number) => i !== index));
  };

  return (
    <div>
      <Modal
        open={visibleModal}
        onCancel={handleCancel}
        width={'50%'}
        className={styles.container}
        title={'Phòng' + roomItem?.heading}
        okText="Lưu"
        cancelText="Hủy"
        afterClose={() => {
          setState({
            attributes: undefined,
          });
          setFileListimgLivingRooms([]);
          setListimgLivingRooms([]);
          setFileListimgBedRooms([]);
          setListimgBedRooms([]);
          setFileListimgBathRooms([]);
          setListimgBathRooms([]);
          setFileListimgBalconys([]);
          setListimgBalconys([]);
          const fields = form.getFieldsValue();
          const nullValues = Object.keys(fields).reduce((acc: any, fieldName: any) => {
            if (!isNaN(fieldName)) {
              acc[fieldName] = null;
              return acc;
            } else {
              return acc;
            }
          }, {});
          form.setFieldsValue(nullValues);
        }}
        footer={[
          <div
            key={Math.random()}
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Button
              style={{
                backgroundColor: '#F89420',
                color: 'white',
                width: '120px',
                borderRadius: '48px',
                // height: "48px",
              }}
              onClick={onSubmit}
              loading={isLoading}
              type="primary"
            >
              Lưu
            </Button>
          </div>,
        ]}
      >
        <Form validateMessages={formValidateMessages} form={form} layout="vertical">
          <Row gutter={[16, 16]}>
            <Col xs={23} sm={23} md={23} lg={12} xl={12}>
              <Form.Item name="area" label="Diện tích phòng (m&#x00B2;)">
                <InputNumber
                  formatter={(value) => ` ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                  parser={(value: any) => value.replace(/\$\s?|(,*)/g, '')}
                  min={0}
                  placeholder="Nhập diện tích"
                />
              </Form.Item>
            </Col>
            <Col xs={23} sm={23} md={23} lg={12} xl={12}>
              <Form.Item name="maxPeople" label="Số người tối đa">
                <InputNumber min={0} placeholder="Nhập số người tối đa" />
              </Form.Item>
            </Col>
          </Row>
          <p className="section">Giá dịch vụ</p>
          <Row gutter={[16, 16]}>
            {(state.attributes?.length > 0 ? state.attributes : dsTienIch)
              ?.filter((item: any) => item?.typeAttribute === AttributeType.SERVICE)
              ?.map((itemMap: any, index: number) => {
                return (
                  <Col xs={23} sm={23} md={23} lg={12} xl={12} key={index}>
                    <Form.Item name={itemMap?.id} label={`${itemMap?.name}(${itemMap?.unit})`}>
                      <InputNumber
                        key={itemMap?.id}
                        formatter={(value) => ` ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                        parser={(value: any) => value.replace(/\$\s?|(,*)/g, '')}
                        step={1000}
                        min={0}
                        placeholder={`Nhập giá ${itemMap?.name}`}
                        onChange={(e) => onHandleAttribute(itemMap?.id, e)}
                      />
                    </Form.Item>
                  </Col>
                );
              })}
          </Row>
          <Col xs={23} sm={23} md={23} lg={12} xl={12}>
            <Form.Item name="direction" label="Hướng phòng">
              <Select placeholder="Chọn hướng phòng">
                {RoomDirectionArray?.map((item: any) => (
                  <Select.Option value={item?.value} key={item?.value}>
                    {item?.label}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <p className="section">Thông tin phòng</p>
          <div className="tableWrapperRoom">
            <div
              style={{
                display: 'flex',
                justifyContent: 'flex-end',
                gap: '8px',
                padding: '0 10px 10px 10px',
                fontSize: '12px',
              }}
            >
              <div className="circle" style={{ backgroundColor: '#B2B5BC' }}></div>
              <p>Đã cho thuê</p>
              <div className="circle" style={{ backgroundColor: '#C7F09E' }}></div>
              <p>Đã mở bán</p>
              <div className="circle" style={{ backgroundColor: '#C8D8FF' }}></div>
              <p>Chưa mở bán</p>
              <div className="circle" style={{ backgroundColor: '#FFCCC9' }}></div>
              <p>Không được mở bán</p>
            </div>
            <table style={{ width: '100%' }}>
              <tr>
                <th colSpan={4}>
                  <div className="flex justify-between">
                    <div>
                      {/* Lùi năm */}
                      <Button
                        className="yearButton"
                        onClick={() => {
                          setSelectedYear(selectedYear - 1);
                        }}
                        disabled={selectedYear > 0 ? false : true}
                      >
                        &#8826;
                      </Button>
                    </div>
                    <div>{apartmentSales?.length > 0 ? apartmentSales[selectedYear]?.year : today.getFullYear()}</div>
                    <div>
                      {/* Tiến năm */}
                      <Button
                        className="yearButton"
                        onClick={() => {
                          setSelectedYear(selectedYear + 1);
                        }}
                        disabled={selectedYear + 1 === apartmentSales?.length ? true : false}
                      >
                        &#8827;
                      </Button>
                    </div>
                  </div>
                </th>
              </tr>
              <tr>
                {apartmentSales?.length > 0 &&
                  apartmentSales[selectedYear]?.allMonths
                    ?.filter((item: any) => item?.row === 1)
                    ?.map((itemMap: any, index: any) => {
                      return (
                        <td
                          key={index}
                          style={{
                            width: '153px',
                            height: '64px',
                            // backgroundColor:
                            //   itemMap?.status === 1
                            //     ? "#B2B5BC"
                            //     : itemMap?.status === 2
                            //     ? "#C7F09E"
                            //     : itemMap?.status === 3
                            //     ? "#C8D8FF"
                            //     : "#FFCCC9",
                          }}
                        >
                          <div>
                            <div
                              style={{
                                background: '#EFF0F1',
                              }}
                              className="flex justify-between"
                            >
                              <p className="monthLabel">THÁNG {itemMap?.month}</p>
                              <EditRoomIcon
                                onClick={onHandleModalRoom(
                                  apartmentSales[selectedYear]?.year,
                                  itemMap?.month,
                                  itemMap?.status,
                                  itemMap?.price || 0
                                )}
                                style={{ cursor: 'pointer' }}
                              />
                            </div>
                            <div>
                              <div style={{ padding: '5px 0' }} className="flex ">
                                <StatusRoomIcon />
                                <span style={{ marginTop: '-3px' }} className="pl-3">
                                  <p
                                    style={{
                                      color:
                                        itemMap?.status === 1
                                          ? '#B2B5BC'
                                          : itemMap?.status === 2
                                          ? '#C7F09E'
                                          : itemMap?.status === 3
                                          ? '#C8D8FF'
                                          : '#FFCCC9',
                                    }}
                                  >
                                    {roomStatus?.find((item: any) => item?.key === itemMap?.status)?.label}
                                  </p>
                                </span>
                              </div>
                              <div style={{ padding: '6px 0' }} className="flex pt-6 pb-6">
                                <MoneyRoomIcon />
                                <span style={{ marginTop: '-3px' }} className="pl-3">
                                  {formatPrice(itemMap.price)}
                                </span>
                              </div>
                            </div>
                          </div>
                        </td>
                      );
                    })}
              </tr>
              <tr>
                {apartmentSales?.length > 0 &&
                  apartmentSales[selectedYear]?.allMonths
                    ?.filter((item: any) => item?.row === 2)
                    ?.map((itemMap: any, index: any) => (
                      <td
                        key={index}
                        style={{
                          width: '153px',
                          height: '64px',
                          // backgroundColor:
                          //   itemMap?.status === 1
                          //     ? "#B2B5BC"
                          //     : itemMap?.status === 2
                          //     ? "#C7F09E"
                          //     : itemMap?.status === 3
                          //     ? "#C8D8FF"
                          //     : "#FFCCC9",
                        }}
                      >
                        <div>
                          <div
                            style={{
                              background: '#EFF0F1',
                            }}
                            className="flex justify-between"
                          >
                            <p className="monthLabel">THÁNG {itemMap?.month}</p>
                            <img
                              onClick={onHandleModalRoom(
                                apartmentSales[selectedYear]?.year,
                                itemMap?.month,
                                itemMap?.status,
                                itemMap?.price || 0
                              )}
                              style={{ cursor: 'pointer' }}
                              src={'/img/icon/EditIcon.png'}
                            />
                            {/* <DownOutlined style={{ marginTop: "7px" }} /> */}
                          </div>
                          <div>
                            <div style={{ padding: '5px 0' }} className="flex ">
                              <StatusRoomIcon />
                              <span style={{ marginTop: '-3px' }} className="pl-3">
                                <p
                                  style={{
                                    color:
                                      itemMap?.status === 1
                                        ? '#B2B5BC'
                                        : itemMap?.status === 2
                                        ? '#C7F09E'
                                        : itemMap?.status === 3
                                        ? '#C8D8FF'
                                        : '#FFCCC9',
                                  }}
                                >
                                  {roomStatus?.find((item: any) => item?.key === itemMap?.status)?.label}
                                </p>
                              </span>
                            </div>
                            <div style={{ padding: '6px 0' }} className="flex pt-6 pb-6">
                              <MoneyRoomIcon />
                              <span style={{ marginTop: '-3px' }} className="pl-3">
                                {formatPrice(itemMap.price)}
                              </span>
                            </div>
                          </div>
                        </div>
                      </td>
                    ))}
              </tr>
              <tr>
                {apartmentSales?.length > 0 &&
                  apartmentSales[selectedYear]?.allMonths
                    ?.filter((item: any) => item?.row === 3)
                    ?.map((itemMap: any, index: any) => (
                      <td
                        key={index}
                        style={{
                          width: '153px',
                          height: '64px',
                          // backgroundColor:
                          //   itemMap?.status === 1
                          //     ? "#B2B5BC"
                          //     : itemMap?.status === 2
                          //     ? "#C7F09E"
                          //     : itemMap?.status === 3
                          //     ? "#C8D8FF"
                          //     : "#FFCCC9",
                        }}
                      >
                        <div>
                          <div
                            style={{
                              background: '#EFF0F1',
                            }}
                            className="flex justify-between"
                          >
                            <p className="monthLabel">THÁNG {itemMap?.month}</p>
                            <img
                              onClick={onHandleModalRoom(
                                apartmentSales[selectedYear]?.year,
                                itemMap?.month,
                                itemMap?.status,
                                itemMap?.price || 0
                              )}
                              style={{ cursor: 'pointer' }}
                              src={'/img/icon/EditIcon.png'}
                            />
                            {/* <DownOutlined style={{ marginTop: "7px" }} /> */}
                          </div>
                          <div>
                            <div style={{ padding: '5px 0' }} className="flex ">
                              <StatusRoomIcon />
                              <span style={{ marginTop: '-3px' }} className="pl-3">
                                <p
                                  style={{
                                    color:
                                      itemMap?.status === 1
                                        ? '#B2B5BC'
                                        : itemMap?.status === 2
                                        ? '#C7F09E'
                                        : itemMap?.status === 3
                                        ? '#C8D8FF'
                                        : '#FFCCC9',
                                  }}
                                >
                                  {roomStatus?.find((item: any) => item?.key === itemMap?.status)?.label}
                                </p>
                              </span>
                            </div>
                            <div style={{ padding: '6px 0' }} className="flex pt-6 pb-6">
                              <MoneyRoomIcon />
                              <span style={{ marginTop: '-3px' }} className="pl-3">
                                {formatPrice(itemMap.price)}
                              </span>
                            </div>
                          </div>
                        </div>
                      </td>
                    ))}
              </tr>
            </table>
          </div>

          <p className="subSection">Cơ sở vật chất</p>
          <Row gutter={[16, 16]}>
            {dsTienIch
              ?.filter((item: any) => item?.typeAttribute === AttributeType.INFRASTRUCTURE)
              ?.map((itemMap: any, index: number) => {
                return (
                  <Col xs={23} sm={23} md={23} lg={12} xl={12} key={index}>
                    <Form.Item name={itemMap?.id} valuePropName="checked">
                      <Checkbox
                        onChange={(e) => {
                          onHandleInfrastructure(itemMap?.id, e.target.checked);
                        }}
                        className="checkboxValue"
                      >
                        {itemMap?.name}
                      </Checkbox>
                    </Form.Item>
                  </Col>
                );
              })}
          </Row>

          <p className="section">Ảnh phòng</p>
          <p className="subSection">Bao quát</p>
          <div>
            {youtubeLinks.map((link: any, index: any) => (
              <div className="flex" key={Math.random()}>
                <iframe
                  style={{ padding: '20px 0' }}
                  key={index}
                  src={`https://www.youtube.com/embed/${getVideoId(link)}`}
                  width="560"
                  height="315"
                  frameBorder="0"
                  allowFullScreen
                />
                <Button
                  onClick={() => handleDeconsteVideo(index)}
                  type="text"
                  className="flex items-center justify-center m-5"
                  icon={<MinusCircleOutlined />}
                />
              </div>
            ))}
          </div>
          <div className="flex gap-2 pt-4 pb-4">
            <Input onChange={handleInputChangeLinkVideo} placeholder="Nhập liên kết YouTube" style={{ width: '30%' }} />
            <Button onClick={embedVideo}>Hiển thị video</Button>
          </div>
          {/* <Form.List name="videoLinks">
            {(fields, { add, remove }) => {
              return (
                <div>
                  <div style={{ display: "flex" }}>
                    <div className="AllowedProduct-title">Link video</div>
                  </div>
                  {fields.map(({ key, name, ...restField }) => {
                    return (
                      <Row key={1}>
                        <Col span={10}>
                          <Form.Item
                            {...restField}
                            name={[name, "videoLink"]}
                            // label={"Số điện thoại"}
                          >
                            <Input />
                          </Form.Item>
                        </Col>

                        <Col span={2}>
                          <p
                            style={{
                              position: "absolute",
                              top: "-3%",
                            }}
                          >
                            <Button
                              icon={<DeconsteOutlined />}
                              onClick={() => {
                                remove(name);
                              }}
                              type="text"
                            ></Button>
                          </p>
                        </Col>
                      </Row>
                    );
                  })}
                  <Form.Item>
                    <Button
                      className="flex items-center"
                      type="link"
                      onClick={() => add()}
                      icon={<PlusOutlined />}
                    >
                      Thêm
                    </Button>
                  </Form.Item>
                </div>
              );
            }}
          </Form.List> */}
          <Upload
            style={{ width: '100%' }}
            className="imgOverall"
            accept="image/*"
            listType="picture-card"
            fileList={fileListimgOverall}
            // onPreview={handlePreview}
            showUploadList={{
              showPreviewIcon: false,
              // showRemoveIcon: false,
            }}
            onChange={(e) => handleChangeUploadImage('imgOverall', e)}
            onRemove={handleRemoveFile('imgOverall')}
          >
            {fileListimgOverall?.length === 0 ? <PlusOutlined /> : null}
          </Upload>
          <p className="subSection">Ảnh thuộc Album phòng trọ</p>
          <p>Ảnh phòng khách</p>
          <Upload
            accept="image/*"
            listType="picture-card"
            fileList={fileListimgLivingRooms}
            // onPreview={handlePreview}
            showUploadList={{
              showPreviewIcon: false,
              // showRemoveIcon: false,
            }}
            onChange={(e) => handleChangeUploadImage('imgLivingRooms', e)}
            onRemove={handleRemoveFile('imgLivingRooms')}
          >
            {fileListimgLivingRooms?.length <= 2 ? <PlusOutlined /> : null}
          </Upload>
          <p>Ảnh phòng ngủ</p>
          <Upload
            accept="image/*"
            listType="picture-card"
            fileList={fileListimgBedRooms}
            // onPreview={handlePreview}
            showUploadList={{
              showPreviewIcon: false,
              // showRemoveIcon: false,
            }}
            onChange={(e) => handleChangeUploadImage('imgBedRooms', e)}
            onRemove={handleRemoveFile('imgBedRooms')}
          >
            {fileListimgBedRooms?.length <= 2 ? <PlusOutlined /> : null}
          </Upload>
          <p>Ảnh phòng vệ sinh</p>
          <Upload
            accept="image/*"
            listType="picture-card"
            fileList={fileListimgBathRooms}
            // onPreview={handlePreview}
            showUploadList={{
              showPreviewIcon: false,
              // showRemoveIcon: false,
            }}
            onChange={(e) => handleChangeUploadImage('imgBathRooms', e)}
            onRemove={handleRemoveFile('imgBathRooms')}
          >
            {fileListimgBathRooms?.length <= 2 ? <PlusOutlined /> : null}
          </Upload>
          <p>Ảnh ban công</p>
          <Upload
            accept="image/*"
            listType="picture-card"
            fileList={fileListimgBalconys}
            // onPreview={handlePreview}
            showUploadList={{
              showPreviewIcon: false,
              // showRemoveIcon: false,
            }}
            onChange={(e) => handleChangeUploadImage('imgBalconys', e)}
            onRemove={handleRemoveFile('imgBalconys')}
          >
            {fileListimgBalconys?.length <= 2 ? <PlusOutlined /> : null}
          </Upload>
        </Form>

        {/* <p
              style={{ fontSize: "24px", lineHeight: "40px" }}
              className="font-bold"
            >
              Thông tin phòng cho thuê
            </p>
            <Divider /> */}
      </Modal>
      <ModalChild
        openModalRoom={openModalRoom}
        modalRoomStatus={state.modalRoomStatus}
        handleVisibleChangeModalRoom={handleVisibleChangeModalRoom}
        onHandleStatus={onHandleStatus}
      />
    </div>
  );
};

export default ModalRoomInfor;
