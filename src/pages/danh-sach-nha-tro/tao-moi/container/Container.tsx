import {
  Button,
  Checkbox,
  Col,
  Divider,
  Form,
  Input,
  InputNumber,
  Row,
  Select,
  Spin,
  Upload,
  UploadFile,
  message,
} from 'antd';
import React, { FC, useEffect, useState } from 'react';
import { PlusOutlined, MinusCircleOutlined } from '@ant-design/icons';
import styles from './styles.module.scss';
import {
  AttributeType,
  ProductTypeArray,
  combineUrlParams,
  filterOption,
  formatPrice,
  getVideoId,
  roomStatusType,
  today,
} from 'src/common';
import axiosIns from 'src/helpers/request';
import { DISTRICT_API, HOME, PROVINCE_API, SERVICE_API, UPLOAD, WARD_API } from 'src/helpers/api';
import { formValidateMessages } from 'src/pages/constants';
import ModalRoomInfor from '../components/ModalRoomInfor/ModalRoomInfor';
import { HouseIcon, MoneyIcon, UnVerifyIcon, VerifyIcon } from 'src/components/Icon';

const MotelInfor: FC = () => {
  const [form] = Form.useForm();
  const [visibleModal, setVisibleModal] = useState<any>(false);
  const [numFloor, setNumFloor] = useState<any>();
  const [numRoom, setNumRoom] = useState<any>();
  const [towerArr, setTowerArr] = useState<any>([]);
  const [tower, setTowerItem] = useState<any>();
  const [room, setRoomItem] = useState<any>();
  const [firtEdit, setFirtEdit] = useState<boolean>(false);
  const [dsTP, setDsTP] = useState<any>([]);
  const [provinceId, setProvinceId] = useState<any>(null);
  const [dsQuan, setDsQuan] = useState<any>([]);
  const [districtId, setDistrictId] = useState<any>(null);
  const [dsPhuong, setDsPhuong] = useState<any>([]);
  const [dsTienIch, setDsTienIch] = useState<any>([]);
  const [utilitiesItem, setUtilitiesItem] = useState<any>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isLoadingUpload, setIsLoadingUpload] = useState(false);
  const [fileListimgOthers, setFileListimgOthers] = useState<UploadFile[]>([]);
  const [listimgOthers, setListimgOthers] = useState<any>([]);
  const [fileListimgOwners, setFileListimgOwners] = useState<UploadFile[]>([]);
  const [listimgOwners, setListimgOwners] = useState<any>([]);
  const [fileListimgOverall, setFileListimgOverall] = useState<UploadFile[]>([]);
  const [youtubeLinks, setYoutubeLinks] = useState<any>([]);
  const [youtubeLink, setYoutubeLink] = useState('');

  //gọi API thành phố
  function fetchProvince(params = {}) {
    return new Promise((resolve, reject) => {
      axiosIns
        .get(
          combineUrlParams(`${PROVINCE_API.LIST}`, {
            limit: 10,
            pages: 1,
            ...params,
          })
        )
        .then((s) => {
          setDsTP(s?.data?.data);
          resolve(s?.data);
        })
        .catch((e) => {
          reject(e);
        });
    });
    // .then
  }

  //gọi API tiện ích
  function fetchService(params = {}) {
    return new Promise((resolve, reject) => {
      axiosIns
        .get(
          combineUrlParams(`${SERVICE_API.LIST}`, {
            limit: 10,
            pages: 1,
            ...params,
          })
        )
        .then((s) => {
          setDsTienIch(s?.data?.data);
          resolve(s?.data);
        })
        .catch((e) => {
          reject(e);
        });
    });
    // .then
  }

  //gọi API quận huyện
  function fetchDistrict(params = {}) {
    return new Promise((resolve, reject) => {
      axiosIns
        .get(
          combineUrlParams(`${DISTRICT_API.LIST}`, {
            limit: 10,
            pages: 1,
            ...params,
          })
        )
        .then((s) => {
          setDsQuan(s?.data?.data);
          resolve(s?.data);
        })
        .catch((e) => {
          reject(e);
        });
    });
    // .then
  }

  //gọi API phường
  function fetchWard(params = {}) {
    return new Promise((resolve, reject) => {
      axiosIns
        .get(
          combineUrlParams(`${WARD_API.LIST}`, {
            limit: 10,
            pages: 1,
            ...params,
          })
        )
        .then((s) => {
          setDsPhuong(s?.data?.data);
          resolve(s?.data);
        })
        .catch((e) => {
          reject(e);
        });
    });
    // .then
  }

  // API tạo mới tòa
  const fetchCreateTower = async (params: any) => {
    return await new Promise((resolve, reject) => {
      setIsLoading(true);
      axiosIns
        .post(`${HOME.CREATE}`, { ...params })
        .then((s) => {
          if (s?.data?.code === 201) {
            message.success(s?.data?.message);
            setTimeout(() => {
              setIsLoading(false);
              window.location.href = '/home';
              resolve(s?.data?.data);
            }, 500);
          } else if (s?.data?.code >= 400) {
            message.error(s?.data?.message);
            setIsLoading(false);
          }
        })
        .catch((e) => {
          console.log('e', e);

          if (e?.response?.data?.statusCode >= 400 || e?.response?.data?.code >= 400) {
            message.error(e?.response?.data?.message);
            setIsLoading(false);
            reject(e);
          } else {
            reject(e);
          }
        });
    });
    // .then
  };

  useEffect(() => {
    fetchProvince();
    fetchService();
  }, []);

  const handleChangeUploadImage = (type: string, data: any) => {
    setIsLoadingUpload(true);
    const isLt2M = data?.file?.size / 1024 / 1024 < 10;
    if (!isLt2M) {
      message.error('Kích thước ảnh không được vượt quá 10Mb.');
      setIsLoadingUpload(false);
      return;
    }
    if (type === 'imgOthers') {
      setFileListimgOthers(data?.fileList);
    } else if (type === 'imgOwners') {
      setFileListimgOwners(data?.fileList);
    } else if (type === 'imgOverall') {
      setFileListimgOverall(data?.fileList);
    }
    if (data?.file?.status === 'removed') {
      setIsLoadingUpload(false);
    }
    const formData: any = new FormData();
    const file = data?.file?.originFileObj;
    formData.append('file', file);
    if (data?.file?.status === 'error' || data?.file?.status === 'done') {
      return new Promise((resolve, reject) => {
        axiosIns
          .post(`${UPLOAD.SINGLE}`, formData, {
            headers: {
              'Content-Type': 'multipart/form-data',
            },
          })
          .then((s) => {
            setIsLoadingUpload(false);
            if (type === 'imgOthers') {
              setListimgOthers((prevList: any) => [...prevList, s?.data?.data?.fileUrl]);
            } else if (type === 'imgOwners') {
              setListimgOwners((prevList: any) => [...prevList, s?.data?.data?.fileUrl]);
            } else {
              form.setFieldsValue({
                [type]: s?.data?.data?.fileUrl,
              });
            }
            // const image = s?.data?.data?.transactionImageInfors.map(
            //   (item: any) => item.fileUrl
            // );
            // form.setFieldsValue({ imageUrls: image });
          })
          .catch((e) => {
            setIsLoadingUpload(false);
            reject(e);
          });
      });
    }
  };

  const handleRemoveFile = (type: string) => (data: any) => {
    if (type === 'imgOthers') {
      const index = fileListimgOthers?.findIndex((item: any) => item?.uid === data?.uid);
      const arr = listimgOthers;
      arr.splice(index, 1);
      const _arr = [...arr];

      setListimgOthers(_arr);
    } else if (type === 'imgOwners') {
      const index = fileListimgOwners?.findIndex((item: any) => item?.uid === data?.uid);
      const arr = listimgOwners;
      arr.splice(index, 1);
      const _arr = [...arr];
      setListimgOwners(_arr);
    } else if (type === 'imgOverall') {
      form.setFieldsValue({
        imgOverall: undefined,
      });
    }
  };

  // const debounceSearch = useDebounce(
  //   {
  //     numFloor: numFloor,
  //     numRoom: numRoom,
  //   },
  //   500
  // );

  useEffect(() => {
    const numYear = 3;
    const currentYear = today.getFullYear();
    const allMonths = [
      {
        month: 1,
        status: 2,
        row: 1,
        price: 0,
      },
      {
        month: 2,
        status: 2,
        row: 1,
        price: 0,
      },
      {
        month: 3,
        status: 2,
        row: 1,
        price: 0,
      },
      {
        month: 4,
        status: 1,
        row: 1,
        price: 0,
      },
      {
        month: 5,
        status: 2,
        row: 2,
        price: 0,
      },
      {
        month: 6,
        status: 2,
        row: 2,
        price: 0,
      },
      {
        month: 7,
        status: 1,
        row: 2,
        price: 0,
      },
      {
        month: 8,
        status: 1,
        row: 2,
        price: 0,
      },
      {
        month: 9,
        status: 1,
        row: 3,
        price: 0,
      },
      {
        month: 10,
        status: 1,
        row: 3,
        price: 0,
      },
      {
        month: 11,
        status: 1,
        row: 3,
        price: 0,
      },
      {
        month: 12,
        status: 1,
        row: 3,
        price: 0,
      },
    ];
    if (numFloor && numRoom) {
      if (firtEdit === false) {
        let data: any = [];
        data = Array.from({ length: numFloor }, (_, i) => {
          const floor: any = {};
          floor.heading = `Tầng ${i + 1}`;
          floor.key = i + 1;
          floor.rooms = Array.from({ length: numRoom }, (_, j) => {
            const room: any = {};
            room.heading = j + 1 <= 9 ? `${i + 1}0${j + 1}` : `${i + 1}${j + 1}`;
            room.key = j + 1;
            room.isVerify = false;
            room.area = 1;
            room.maxPeople = 1;
            room.attributes = [];
            room.infrastructures = [];
            room.direction = 1;
            const apartmentSale: any = Array.from({ length: numYear }, (_, k) => {
              const apartmentSale = {
                year: currentYear + k,
                allMonths: allMonths,
              };
              return apartmentSale;
            });
            room.apartmentSales = apartmentSale?.map((sale: any) => {
              if (sale.year === today.getFullYear()) {
                const updatedMonths = sale.allMonths.map((month: any) => {
                  if (month.month >= today.getMonth() + 1) {
                    return { ...month, status: 2 };
                  } else {
                    return { ...month, status: 4 };
                  }
                });
                return { ...sale, allMonths: updatedMonths };
              } else if (sale.year > today.getFullYear()) {
                const updatedMonths = sale.allMonths.map((month: any) => {
                  return { ...month, status: 2 };
                });
                return { ...sale, allMonths: updatedMonths };
              }
              return sale;
            });
            room.fileListimgLivingRooms = [];
            room.listImgLivingRooms = [];
            room.fileListimgBedRooms = [];
            room.listImgBedRooms = [];
            room.fileListimgBathRooms = [];
            room.listImgBathRooms = [];
            room.fileListimgBalconys = [];
            room.listImgBalconys = [];
            room.fileListimgOverall = [];
            room.imgOverall = undefined;
            room.videoLinks = [];
            return room;
          });
          return floor;
        });
        setTowerArr(data);
      } else {
        const data: any = towerArr;
        const currentRoom = data[0]?.rooms?.length;
        //tầng lớn hơn tầng hiện tại
        if (numFloor > towerArr?.length) {
          const quantityToAdd = numFloor - towerArr?.length; // Số lượng giá trị cần thêm
          const startingFloor = data.length + 1; // Giá trị floor ban đầu
          const apartmentSale: any = Array.from({ length: numYear }, (_, k) => {
            const apartmentSale = {
              year: currentYear + k,
              allMonths: allMonths,
            };
            return apartmentSale;
          });
          const newEntries = Array.from({ length: quantityToAdd }, (_, index) => ({
            key: startingFloor + index,
            heading: `Tầng ${startingFloor + index}`,
            rooms: Array.from({ length: numRoom }, (_, id) => ({
              heading: id + 1 <= 9 ? `${startingFloor + index}0${id + 1}` : `${startingFloor + index}${id + 1}`,
              key: id + 1,
              isVerify: false,
              area: 1,
              maxPeople: 1,
              attributes: [],
              infrastructures: [],
              direction: 1,
              apartmentSales: apartmentSale?.map((sale: any) => {
                if (sale.year === today.getFullYear()) {
                  const updatedMonths = sale.allMonths.map((month: any) => {
                    if (month.month >= today.getMonth() + 1) {
                      return { ...month, status: 2 };
                    } else {
                      return { ...month, status: 4 };
                    }
                  });
                  return { ...sale, allMonths: updatedMonths };
                } else if (sale.year > today.getFullYear()) {
                  const updatedMonths = sale.allMonths.map((month: any) => {
                    return { ...month, status: 2 };
                  });
                  return { ...sale, allMonths: updatedMonths };
                }
                return sale;
              }),
              fileListimgLivingRooms: [],
              listImgLivingRooms: [],
              fileListimgBedRooms: [],
              listImgBedRooms: [],
              fileListimgBathRooms: [],
              listImgBathRooms: [],
              fileListimgBalconys: [],
              listImgBalconys: [],
              fileListimgOverall: [],
              imgOverall: undefined,
              videoLinks: [],
            })),
          }));
          const shadowTowerArr = [...data, ...newEntries];
          setTowerArr(shadowTowerArr);
        }
        //tầng nhỏ hơn tầng hiện tại
        else if (numFloor < towerArr?.length) {
          const shadowTowerArr = data.filter((item: any) => item?.key <= numFloor);
          setTowerArr(shadowTowerArr);
        }
        //phòng lớn hơn phòng hiện tại
        if (numRoom > currentRoom) {
          const quantityToAdd = numRoom - currentRoom; // Số lượng giá trị cần thêm
          const startingRoom = currentRoom + 1; // Giá trị floor ban đầu
          const apartmentSale: any = Array.from({ length: numYear }, (_, k) => {
            const apartmentSale = {
              year: currentYear + k,
              allMonths: allMonths,
            };
            return apartmentSale;
          });
          const shadowTowerArr = data?.map((item: any) => {
            const addRooms = Array.from({ length: quantityToAdd }, (_, id) => ({
              heading:
                startingRoom + id <= 9 ? `${item?.key}0${startingRoom + id}` : `${item?.key}${startingRoom + id}`,
              key: startingRoom,
              isVerify: false,
              area: 1,
              maxPeople: 1,
              attributes: [],
              infrastructures: [],
              direction: 1,
              apartmentSales: apartmentSale?.map((sale: any) => {
                if (sale.year === today.getFullYear()) {
                  const updatedMonths = sale.allMonths.map((month: any) => {
                    if (month.month >= today.getMonth() + 1) {
                      return { ...month, status: 2 };
                    } else {
                      return { ...month, status: 4 };
                    }
                  });
                  return { ...sale, allMonths: updatedMonths };
                } else if (sale.year > today.getFullYear()) {
                  const updatedMonths = sale.allMonths.map((month: any) => {
                    return { ...month, status: 2 };
                  });
                  return { ...sale, allMonths: updatedMonths };
                }
                return sale;
              }),
              fileListimgLivingRooms: [],
              listImgLivingRooms: [],
              fileListimgBedRooms: [],
              listImgBedRooms: [],
              fileListimgBathRooms: [],
              listImgBathRooms: [],
              fileListimgBalconys: [],
              listImgBalconys: [],
              fileListimgOverall: [],
              imgOverall: undefined,
              videoLinks: [],
            }));
            const afterRoomArr = [...item.rooms, ...addRooms];
            return {
              ...item,
              rooms: afterRoomArr,
            };
          });
          setTowerArr(shadowTowerArr);
        }
        //phòng nhỏ hơn phòng hiện tại
        else if (numRoom < currentRoom) {
          const shadowTowerArr = data?.map((item: any) => {
            return {
              ...item,
              rooms: item?.rooms.filter((it: any) => it?.key <= numRoom),
            };
          });
          setTowerArr(shadowTowerArr);
        }
      }
    }
  }, [numFloor, numRoom]);

  const onHandleEditRoom = (
    value: any,
    apartmentSales: any,
    towerKey: number,
    roomKey: number,
    attributesValue: any,
    infrastructureItem: any,
    fileListimgLivingRooms: any,
    listImgLivingRooms: any,
    fileListimgBedRooms: any,
    listImgBedRooms: any,
    fileListimgBathRooms: any,
    listImgBathRooms: any,
    fileListimgBalconys: any,
    listImgBalconys: any,
    fileListimgOverall: any,
    imgOverallModal: any,
    videoLinks: any
  ) => {
    const data = towerArr;
    if (firtEdit === false) {
      const _data = data?.map((item: any) => {
        if (item?.key === towerKey) {
          return {
            ...item,
            rooms: item?.rooms?.map((roomItem: any) => {
              if (roomItem?.key === roomKey) {
                return {
                  ...roomItem,
                  isVerify: true,
                  area: value.area || 1,
                  maxPeople: value.maxPeople || 1,
                  attributes: attributesValue,
                  direction: value.direction,
                  apartmentSales: apartmentSales,
                  infrastructures: infrastructureItem,
                  fileListimgLivingRooms: [...fileListimgLivingRooms],
                  listImgLivingRooms: [...listImgLivingRooms],
                  fileListimgBedRooms: [...fileListimgBedRooms],
                  listImgBedRooms: [...listImgBedRooms],
                  fileListimgBathRooms: [...fileListimgBathRooms],
                  listImgBathRooms: [...listImgBathRooms],
                  fileListimgBalconys: [...fileListimgBalconys],
                  listImgBalconys: [...listImgBalconys],
                  fileListimgOverall: [...fileListimgOverall],
                  imgOverall: imgOverallModal,
                  videoLinks: videoLinks,
                };
              }
              return {
                ...roomItem,
                isVerify: false,
                area: value.area || 1,
                maxPeople: value.maxPeople || 1,
                attributes: attributesValue,
                direction: value.direction,
                apartmentSales: apartmentSales,
                infrastructures: infrastructureItem,
                fileListimgLivingRooms: [...fileListimgLivingRooms],
                listImgLivingRooms: [...listImgLivingRooms],
                fileListimgBedRooms: [...fileListimgBedRooms],
                listImgBedRooms: [...listImgBedRooms],
                fileListimgBathRooms: [...fileListimgBathRooms],
                listImgBathRooms: [...listImgBathRooms],
                fileListimgBalconys: [...fileListimgBalconys],
                listImgBalconys: [...listImgBalconys],
                fileListimgOverall: [...fileListimgOverall],
                imgOverall: imgOverallModal,
                videoLinks: videoLinks,
              };
            }),
          };
        }
        return {
          ...item,
          rooms: item?.rooms?.map((roomItem: any) => {
            return {
              ...roomItem,
              isVerify: false,
              area: value.area || 1,
              maxPeople: value.maxPeople || 1,
              attributes: attributesValue,
              direction: value.direction,
              apartmentSales: apartmentSales,
              infrastructures: infrastructureItem,
              fileListimgLivingRooms: [...fileListimgLivingRooms],
              listImgLivingRooms: [...listImgLivingRooms],
              fileListimgBedRooms: [...fileListimgBedRooms],
              listImgBedRooms: [...listImgBedRooms],
              fileListimgBathRooms: [...fileListimgBathRooms],
              listImgBathRooms: [...listImgBathRooms],
              fileListimgBalconys: [...fileListimgBalconys],
              listImgBalconys: [...listImgBalconys],
              fileListimgOverall: [...fileListimgOverall],
              imgOverall: imgOverallModal,
              videoLinks: videoLinks,
            };
          }),
        };
      });
      setTowerArr(_data);
      setFirtEdit(true);
    } else {
      data[towerKey - 1].rooms[roomKey - 1] = {
        ...data[towerKey - 1].rooms[roomKey - 1],
        isVerify: true,
        area: value.area || 1,
        maxPeople: value.maxPeople || 1,
        attributes: attributesValue,
        direction: value.direction,
        apartmentSales: apartmentSales,
        infrastructures: infrastructureItem,
        fileListimgLivingRooms: [...fileListimgLivingRooms],
        listImgLivingRooms: [...listImgLivingRooms],
        fileListimgBedRooms: [...fileListimgBedRooms],
        listImgBedRooms: [...listImgBedRooms],
        fileListimgBathRooms: [...fileListimgBathRooms],
        listImgBathRooms: [...listImgBathRooms],
        fileListimgBalconys: [...fileListimgBalconys],
        listImgBalconys: [...listImgBalconys],
        fileListimgOverall: [...fileListimgOverall],
        imgOverall: imgOverallModal,
        videoLinks: videoLinks,
      };
      console.log('data', data);

      setTowerArr(data);
    }
  };

  // useEffect(() => {
  //   const value = {
  //     numFloor: numFloor,
  //     numRoom: numRoom,
  //   };
  // }, [numFloor, numRoom]);

  const onHandleUtilities = (id: any, value: any) => {
    let utilities: any = [];
    if (value === true) {
      utilities.push(...utilitiesItem, id);
      setUtilitiesItem(utilities);
    } else {
      utilities = utilitiesItem?.filter((item: any) => item !== id);
      setUtilitiesItem(utilities);
    }
  };

  const onHandconstower = (type: string) => (e: any) => {
    if (type === 'floor') {
      setNumFloor(e);
    } else {
      setNumRoom(e);
    }
  };

  const onHandleVisibleModal = (value: boolean) => {
    setVisibleModal(value);
  };

  const handleClickCell = (towerItem: any, roomItem: any) => () => {
    console.log('roomItem', roomItem);

    setVisibleModal(true);
    setTowerItem(towerItem);
    setRoomItem(roomItem);
  };

  console.log('towerArr', towerArr);
  console.log('listimgOthers', listimgOthers);

  const onSubmit = () => {
    const input = {
      totalFloor: form.getFieldValue('totalFloor') || 0,
      roomOfFloor: form.getFieldValue('roomOfFloor') || 0,
      productType: form.getFieldValue('productType'),
      provinceId: form.getFieldValue('provinceId'),
      districtId: form.getFieldValue('districtId'),
      wardId: form.getFieldValue('wardId'),
      street: form.getFieldValue('street')?.trim(),
      address: form.getFieldValue('address')?.trim(),
      videoLinks: youtubeLinks,
      generalUtilities: utilitiesItem,
      floors: towerArr?.map((item: any) => {
        return {
          ...item,
          rooms: item?.rooms?.map((itemRoom: any) => {
            return {
              apartmentSales: itemRoom?.apartmentSales,
              area: itemRoom?.area,
              attributes: itemRoom?.attributes,
              direction: itemRoom?.direction,
              heading: itemRoom?.heading,
              imgOverall: itemRoom?.imgOverall,
              infrastructures: itemRoom?.infrastructures,
              isVerify: itemRoom?.isVerify,
              key: itemRoom?.key,
              listImgBathRooms: itemRoom?.listImgBathRooms,
              listImgBalconys: itemRoom?.listImgBalconys,
              listImgBedRooms: itemRoom?.listImgBedRooms,
              listImgLivingRooms: itemRoom?.listImgLivingRooms,
              maxPeople: itemRoom?.maxPeople,
              videoLinks: itemRoom?.videoLinks,
            };
          }),
        };
      }),
      imgOwners: listimgOwners,
      imgOthers: listimgOthers,
      imgOverall: form.getFieldValue('imgOverall'),
    };
    console.log('input', input);
    if (
      !form.getFieldValue('provinceId') ||
      !form.getFieldValue('districtId') ||
      !form.getFieldValue('wardId') ||
      !form.getFieldValue('street') ||
      !form.getFieldValue('address') ||
      form.getFieldValue('street')?.length === 0 ||
      form.getFieldValue('address')?.length === 0 ||
      !form.getFieldValue('totalFloor') ||
      !form.getFieldValue('roomOfFloor') ||
      !form.getFieldValue('productType')
    ) {
      message?.error('Vui lòng nhập đủ các trường thông tin còn thiếu');
      return;
    }
    if (fileListimgOthers?.length === 0 || fileListimgOverall?.length == 0) {
      if (fileListimgOthers?.length === 0) {
        message.error('Còn thiếu ảnh thuộc Album tòa nhà');
      }
      if (fileListimgOverall?.length === 0) {
        message.error('Còn thiếu ảnh bao quát');
      }
    } else if (fileListimgOthers?.length < 4) {
      message.error('Cần tải lên ít nhất 4 ảnh thuộc Album tòa nhà');
    } else {
      fetchCreateTower(input);
    }
  };

  // const onSetAttribute = (value: any) => {
  //   setAtributeItem(value);
  // };

  const handleInputChangeLinkVideo = (event: any) => {
    const link = event.target.value;
    setYoutubeLink(link);
  };

  const embedVideo = () => {
    // Kiểm tra xem đầu vào có phải là liên kết YouTube hợp lệ hay không
    const regExp =
      /^(?:https?:\/\/)?(?:www\.)?(?:youtube\.com\/(?:watch\?v=|embed\/|v\/|channels\/(?:\w+\/)?|user\/\w+|user\/(?:\w+\/)?\/user\/\w+)|youtu\.be\/)([a-zA-Z0-9_-]{11})/;
    const match = youtubeLink.match(regExp);
    if (match) {
      setYoutubeLinks((prevLinks: any) => [...prevLinks, youtubeLink]);
    } else {
      message.error('Link video không đúng định dạng');
    }
  };

  const handleDeconsteVideo = (index: number) => {
    setYoutubeLinks(youtubeLinks.filter((item: any, i: number) => i !== index));
  };

  return (
    <div className="container p-10">
      <Spin spinning={isLoading}>
        <div className={styles.container}>
          <Form
            validateMessages={formValidateMessages}
            form={form}
            layout="vertical"
            // onFinish={onSubmit}
          >
            <p style={{ fontSize: '24px', lineHeight: '40px' }} className="font-bold">
              Thông tin phòng cho thuê
            </p>
            <Divider />
            <p
              style={{
                fontWeight: 700,
                fontSize: '20px',
                lineHeight: '40px',
              }}
            >
              Địa chỉ
            </p>
            <Row gutter={[16, 0]}>
              <Col xs={23} sm={23} md={23} lg={8} xl={6}>
                <Form.Item rules={[{ required: true }]} name="provinceId" label="Tỉnh/Thành phố">
                  <Select
                    showSearch
                    filterOption={filterOption}
                    onChange={(e) => {
                      const input = {
                        id: e,
                      };
                      setProvinceId(e);
                      fetchDistrict(input);
                      form.setFieldsValue({
                        districtId: undefined,
                        wardId: undefined,
                      });
                    }}
                    placeholder="Chọn tỉnh/thành phố"
                  >
                    {(dsTP || [])?.map((item: any) => {
                      return (
                        <Select.Option value={item?.id} key={item?.id}>
                          {item?.name}
                        </Select.Option>
                      );
                    })}
                  </Select>
                </Form.Item>
              </Col>
              <Col xs={23} sm={23} md={23} lg={8} xl={6}>
                <Form.Item rules={[{ required: true }]} name="districtId" label="Quận/Huyện">
                  <Select
                    showSearch
                    filterOption={filterOption}
                    disabled={provinceId ? false : true}
                    onChange={(e) => {
                      const input = {
                        id: e,
                      };
                      setDistrictId(e);
                      fetchWard(input);
                      form.setFieldsValue({
                        wardId: undefined,
                      });
                    }}
                    placeholder="Chọn quận/huyện"
                  >
                    {dsQuan?.map((item: any) => (
                      <Select.Option value={item?.id} key={item?.id}>
                        {item?.name}
                      </Select.Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>
              <Col xs={23} sm={23} md={23} lg={8} xl={6}>
                <Form.Item rules={[{ required: true }]} name="wardId" label="Phường/Xã">
                  <Select
                    showSearch
                    filterOption={filterOption}
                    disabled={provinceId && districtId ? false : true}
                    // onChange={(e) => {
                    //   const input = {
                    //     id: e,
                    //   };
                    //   fetchWard(input);
                    // }}
                    placeholder="Chọn phường/xã"
                  >
                    {dsPhuong?.map((item: any) => (
                      <Select.Option value={item?.id} key={item?.id}>
                        {item?.name}
                      </Select.Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>
              <Col xs={23} sm={23} md={23} lg={8} xl={6}>
                <Form.Item rules={[{ required: true }, { whitespace: true }]} name="street" label="Đường/Phố">
                  <Input placeholder="Nhập tên đường/phố" />
                </Form.Item>
              </Col>
              <Col xs={23} sm={23} md={23} lg={8} xl={6}>
                <Form.Item rules={[{ required: true }, { whitespace: true }]} name="address" label="Số nhà">
                  <Input placeholder="Nhập số nhà" />
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={[16, 16]}>
              <Col xs={23} sm={23} md={23} lg={12} xl={12}>
                <Form.Item rules={[{ required: true }]} name="totalFloor" label="Số tầng">
                  <InputNumber onChange={onHandconstower('floor')} placeholder="Nhập số tầng" min={1} />
                </Form.Item>
              </Col>
              <Col xs={23} sm={23} md={23} lg={12} xl={12}>
                <Form.Item rules={[{ required: true }]} name="roomOfFloor" label="Số phòng mỗi tầng">
                  <InputNumber
                    onChange={onHandconstower('room')}
                    placeholder="Nhập số phòng mỗi tầng"
                    min={1}
                    max={50}
                  />
                </Form.Item>
              </Col>
              {/* <Col xs={23} sm={23} md={23} lg={12} xl={12}>
              <Form.Item label="Giá phòng (đồng/tháng)">
                <InputNumber placeholder="Nhập giá phòng" />
              </Form.Item>
            </Col> */}
              <Col xs={23} sm={23} md={23} lg={12} xl={12}>
                <Form.Item rules={[{ required: true }]} name="productType" label="Loại phòng">
                  <Select placeholder="Chọn loại phòng">
                    {ProductTypeArray?.map((item: any) => (
                      <Select.Option value={item?.key} key={item?.key}>
                        {item?.label}
                      </Select.Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <p className="section">Tên phòng</p>
            <div className="tabconstower">
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'flex-end',
                  gap: '8px',
                  padding: '0 10px 10px 10px',
                  fontSize: '12px',
                }}
              >
                <div className="circle" style={{ backgroundColor: '#B2B5BC' }}></div>
                <p>Đã cho thuê</p>
                <div className="circle" style={{ backgroundColor: '#C7F09E' }}></div>
                <p>Đã mở bán</p>
                <div className="circle" style={{ backgroundColor: '#C8D8FF' }}></div>
                <p>Chưa mở bán</p>
                <div className="circle" style={{ backgroundColor: '#FFCCC9' }}></div>
                <p>Không được mở bán</p>
              </div>
              <div>
                <table
                  style={{
                    maxWidth: '1275px',
                    width: '100%',
                    minWidth: '100%',
                    maxHeight: '300px',
                    display: 'block',
                  }}
                  // className="table-auto"
                >
                  {towerArr.map((item: any, index: number) => (
                    <tr
                      // className="border bg-green-500 text-center font-bold text-white py-3 px-4"
                      key={index}
                    >
                      <th>{item.heading}</th>
                      {item?.rooms.map((roomItem: any, roomIndex: number) => {
                        return (
                          <td
                            style={{
                              backgroundColor:
                                roomItem?.apartmentSales
                                  ?.find((item: any) => item?.year === today.getFullYear())
                                  ?.allMonths?.find((itemMonth: any) => itemMonth?.month === today.getMonth() + 1)
                                  ?.status === roomStatusType.RENTED
                                  ? '#C8CBD0'
                                  : roomItem?.apartmentSales
                                      ?.find((item: any) => item?.year === today.getFullYear())
                                      ?.allMonths?.find((itemMonth: any) => itemMonth?.month === today.getMonth() + 1)
                                      ?.status === roomStatusType.NOT_SOLD
                                  ? '#FFCCC9'
                                  : roomItem?.apartmentSales
                                      ?.find((item: any) => item?.year === today.getFullYear())
                                      ?.allMonths?.find((itemMonth: any) => itemMonth?.month === today.getMonth() + 1)
                                      ?.status === roomStatusType.NOT_SOLD_YET
                                  ? '#C8D8FF'
                                  : '#C7F09E',
                            }}
                            onClick={handleClickCell(item, roomItem)}
                            key={roomIndex}
                          >
                            <div className="underlineDiv">
                              <p className="underlineText">{roomItem.heading}</p>
                              {roomItem.isVerify === true ? <VerifyIcon /> : <UnVerifyIcon />}
                            </div>
                            <div>
                              <div className="flex">
                                <MoneyIcon />{' '}
                                <span className="pl-3">
                                  {formatPrice(
                                    roomItem?.apartmentSales
                                      ?.find((item: any) => item?.year === today.getFullYear())
                                      ?.allMonths?.find((itemMonth: any) => itemMonth?.month === today.getMonth() + 1)
                                      ?.price
                                  )}
                                </span>
                              </div>
                              <div className="flex">
                                <HouseIcon /> <span className="pl-3">{roomItem.area}</span>
                              </div>
                            </div>
                          </td>
                        );
                      })}
                    </tr>
                  ))}
                </table>
              </div>
            </div>

            <p className="section">Tiện ích chung</p>
            <div>
              <Row gutter={[16, 16]}>
                {dsTienIch
                  ?.filter((item: any) => item?.typeAttribute === AttributeType.GENERAL_UTILITIES)
                  ?.map((itemMap: any, indexMap: number) => {
                    return (
                      <Col xs={23} sm={23} md={23} lg={12} xl={12} key={indexMap}>
                        <Form.Item name={itemMap?.id} valuePropName="checked">
                          <Checkbox
                            onChange={(e) => {
                              onHandleUtilities(itemMap?.id, e.target.checked);
                            }}
                            className="checkboxValue"
                          >
                            {itemMap?.name}
                          </Checkbox>
                        </Form.Item>
                      </Col>
                    );
                  })}
              </Row>
            </div>
            <div>
              <p
                style={{
                  fontWeight: 700,
                  fontSize: '20px',
                  lineHeight: '40px',
                }}
              >
                Ảnh giấy chứng nhận sở hữu
              </p>
              <Upload
                className="custom-upload"
                accept="image/*"
                listType="picture-card"
                fileList={fileListimgOwners}
                // onPreview={handlePreview}
                showUploadList={{
                  showPreviewIcon: false,
                }}
                onChange={(e) => handleChangeUploadImage('imgOwners', e)}
                onRemove={handleRemoveFile('imgOwners')}
              >
                <PlusOutlined />
              </Upload>
            </div>

            <p
              style={{
                fontWeight: 700,
                fontSize: '20px',
                lineHeight: '40px',
              }}
            >
              Ảnh phòng
            </p>
            <p className="section">Bao quát</p>
            <div>
              {youtubeLinks.map((link: any, index: any) => (
                <div className="flex" key={Math.random()}>
                  <iframe
                    style={{ padding: '20px 0' }}
                    key={index}
                    src={`https://www.youtube.com/embed/${getVideoId(link)}`}
                    width="560"
                    height="315"
                    frameBorder="0"
                    allowFullScreen
                  />
                  <Button
                    onClick={() => handleDeconsteVideo(index)}
                    type="text"
                    className="flex items-center justify-center m-5"
                    icon={<MinusCircleOutlined />}
                  />
                </div>
              ))}
            </div>
            <div className="flex gap-2 pt-4 pb-4">
              <Input
                onChange={handleInputChangeLinkVideo}
                placeholder="Nhập liên kết YouTube"
                style={{ width: '30%' }}
              />
              <Button onClick={embedVideo}>Hiển thị video</Button>
            </div>
            <Upload
              style={{ width: '1  00%' }}
              className="imgOverall"
              accept="image/*"
              listType="picture-card"
              fileList={fileListimgOverall}
              // onPreview={handlePreview}
              showUploadList={{
                showPreviewIcon: false,
              }}
              onChange={(e) => handleChangeUploadImage('imgOverall', e)}
              onRemove={handleRemoveFile('imgOverall')}
            >
              {fileListimgOverall?.length === 0 ? <PlusOutlined /> : null}
            </Upload>
            <p className="section">Ảnh thuộc Album tòa nhà</p>
            <Upload
              accept="image/*"
              listType="picture-card"
              fileList={fileListimgOthers}
              // onPreview={handlePreview}
              showUploadList={{
                showPreviewIcon: false,
              }}
              onChange={(e) => handleChangeUploadImage('imgOthers', e)}
              onRemove={handleRemoveFile('imgOthers')}
            >
              <PlusOutlined />
            </Upload>
            {/* <Modal visible={previewOpen} footer={null} onCancel={handleCancel}>
              <img alt="example" style={{ width: "100%" }} src={previewImage} />
            </Modal> */}
            <ModalRoomInfor
              visibleModal={visibleModal}
              onHandleVisibleModal={onHandleVisibleModal}
              towerItem={tower}
              roomItem={room}
              onHandleEditRoom={onHandleEditRoom}
              dsTienIch={dsTienIch}
            />
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Form.Item>
                <Button
                  htmlType="submit"
                  loading={isLoadingUpload}
                  style={{
                    alignItems: 'center',
                    padding: '12px 24px',
                    gap: '10px',
                    width: '186px',
                    height: '48px',
                    background: '#F89420',
                    borderRadius: '48px',
                    color: 'white',
                  }}
                  onClick={onSubmit}
                >
                  Đăng ký phòng
                </Button>
              </Form.Item>
            </div>
          </Form>
        </div>
      </Spin>
    </div>
  );
};

export default MotelInfor;
