import { Form, message } from 'antd';
import React, { useEffect, useState } from 'react';
import { combineUrlParams } from 'src/common';
import axiosIns from 'src/helpers/request';
import { FilterForm } from '../components/FilterForm';
import { TableForm } from '../components/Table';
import styles from './styles.module.scss';
import { HOME } from 'src/helpers/api';
import useDebounce from 'src/hook/useDebounce';
import ModalDelete from '../components/ModalDelete';
import { useLocation } from 'react-router-dom';

function Container() {
  const [form] = Form.useForm();
  const [isLoading, setIsLoading] = useState(false);
  const [dsUser, setDsUser] = useState<any>([]);
  const [totalElement, setTotalElement] = useState<any>(0);
  const [filter, setFilter] = useState<any>({ limit: 10, page: 1 });
  const [dsApproveProduct, setDsApproveProduct] = useState<any>([]);
  const [openModalDelete, setOpenModalDelete] = useState(false);
  const [modalValue, setModalValue] = useState(null);
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const paramValue = searchParams.get('paramName');
  //   Gọi API ds
  async function fetchSearchUser(params = {}) {
    return await new Promise((resolve, reject) => {
      setIsLoading(true);
      axiosIns
        .get(combineUrlParams(`${HOME.LIST}`, { ...params }))
        .then((s) => {
          setDsUser(s?.data?.data?.content);
          setTotalElement(s?.data?.data?.totalElements);
          resolve(s?.data?.data?.content);
          setIsLoading(false);
          setFilter(params);
        })
        .catch((e) => {
          setIsLoading(false);
          reject(e);
        });
    });
    // .then
  }

  // API duyệt hàng loạt đơn hàng
  const fetchApporveProduct = async (param?: number) => {
    return await new Promise((resolve, reject) => {
      setIsLoading(true);
      axiosIns
        .put(`${HOME.APPROVE}`, {
          status: param,
          ids: dsApproveProduct,
        })
        .then((s) => {
          debugger;
          if (s?.data?.code === 200) {
            message.success(s?.data?.message);
            setDsApproveProduct([]);
            setTimeout(() => {
              setIsLoading(false);
              fetchSearchUser({ page: 1, limit: 10 });
              resolve(s?.data?.data);
            }, 500);
          } else if (s?.data?.code >= 400) {
            setIsLoading(false);
            message.error(s?.data?.message);
            reject(s?.data);
          } else reject(s?.data);
        })
        .catch((e) => {
          if (e?.response?.data?.code >= 400) {
            message.error(e?.response?.data?.message);
            setIsLoading(false);
            reject(e);
          } else {
            reject(e);
          }
        });
    });
    // .then
  };

  useEffect(() => {
    fetchSearchUser({ ...filter }).catch(console.error);
  }, []);

  const [keyWordValue, setKeyWordValue] = useState('');
  const onFilter = (value: any) => {
    setKeyWordValue(value);
  };
  const debounceSearch = useDebounce(keyWordValue, 500);

  useEffect(() => {
    const value: any = {
      keyword: paramValue,
    };
    setKeyWordValue(value);
  }, [paramValue]);

  useEffect(() => {
    if (debounceSearch) {
      const input = {
        ...filter,
        keyword: debounceSearch?.keyword,
        startTime: debounceSearch?.date ? (debounceSearch?.date[0]).format('YYYY-MM-DD') : null,
        endTime: debounceSearch?.date ? (debounceSearch?.date[1]).format('YYYY-MM-DD') : null,
        status: debounceSearch?.status,
        page: 1,
        limit: 10,
      };
      fetchSearchUser(input);
    }
  }, [debounceSearch]);

  const onShowSizeChange = (current: number, size: number) => {
    fetchSearchUser({
      ...filter,
      limit: size,
    });
  };

  const onChangePage = (page: number) => {
    fetchSearchUser({
      ...filter,
      page: page,
    });
  };
  const handleVisibleChangeModalDelete = (status: any, value: any) => {
    setOpenModalDelete(status);
    setModalValue(value);
  };

  const onApproveProduct = (value: any) => {
    setDsApproveProduct(value);
  };

  const onChangePost = (record: any) => {
    return new Promise((resolve, reject) => {
      setIsLoading(true);
      axiosIns
        .put(`${HOME.CHANGE_STATUS}/${record?.id}`, {})
        .then((s) => {
          if (s?.data?.code === 201) {
            message.success(s?.data?.message);
            setTimeout(() => {
              setIsLoading(false);
              fetchSearchUser({ page: 1, limit: 10 });
              resolve(s?.data?.data);
            }, 500);
          } else if (s?.data?.code >= 400) {
            message.error(s?.data?.message);
            setIsLoading(false);
          }
        })
        .catch((e) => {
          setIsLoading(false);
          message.error(e?.response?.data?.message);
          reject(e);
        });
    });
  };

  return (
    <div className={styles.container}>
      <div className="sub-container">
        <p className="container-title">Danh sách nhà trọ</p>
        <FilterForm
          dsApproveProduct={dsApproveProduct}
          fetchApporveProduct={fetchApporveProduct}
          form={form}
          onFilter={onFilter}
        />
        <TableForm
          onChangePost={onChangePost}
          dsApproveProduct={dsApproveProduct}
          onApproveProduct={onApproveProduct}
          totalElement={totalElement}
          currentPage={filter?.page}
          pageSize={filter?.limit}
          onChangePage={onChangePage}
          onShowSizeChange={onShowSizeChange}
          dsUser={dsUser}
          isLoading={isLoading}
          form={form}
          handleVisibleChangeModalDelete={handleVisibleChangeModalDelete}
        />
        <ModalDelete
          modalValue={modalValue}
          openModalDelete={openModalDelete}
          handleVisibleChangeModalDelete={handleVisibleChangeModalDelete}
          fetchSearchUser={fetchSearchUser}
        />
      </div>
    </div>
  );
}

export default Container;
