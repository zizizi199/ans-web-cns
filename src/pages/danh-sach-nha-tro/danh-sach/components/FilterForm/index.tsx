import React from 'react';
import { Button, Col, Form, FormInstance, Row } from 'antd';
import styles from './styles.module.scss';
import { ApproveIcon, Decline_X_Icon, PlusIcon } from 'src/components/Icon';
import { useNavigate } from 'react-router-dom';
import { HostelRoutes } from 'src/helpers/app.routes';

interface Props {
  form: FormInstance<any>;
  onFilter: (value: object) => void;
  fetchApporveProduct: (type: number) => void;
  dsApproveProduct: any;
}

export const FilterForm: React.FC<Props> = ({ form, fetchApporveProduct, dsApproveProduct }) => {
  const navigate = useNavigate();
  return (
    <div className={styles.FilterForm}>
      <Form form={form}>
        <Row>
          <Col span={18}>
            <Row>
              <Col span={4} style={{ marginRight: '10px' }}>
                <Form.Item name="keyword"></Form.Item>
              </Col>
            </Row>
          </Col>
          <Col span={6}>
            <div className="button-float">
              <Button
                disabled={dsApproveProduct.length === 0}
                onClick={() => {
                  fetchApporveProduct(2);
                }}
                icon={<Decline_X_Icon />}
                style={{ backgroundColor: 'white', color: '#E20D00 !important', border: '1px solid #E20D00' }}
              >
                Từ chối
              </Button>
              <Button
                disabled={dsApproveProduct.length === 0}
                onClick={() => {
                  fetchApporveProduct(1);
                }}
                icon={<ApproveIcon />}
                style={{ backgroundColor: '#1DC951', color: 'white', border: '1px solid #1DC951' }}
              >
                Duyệt
              </Button>
              <Button
                onClick={() => {
                  navigate(HostelRoutes.create);
                }}
                icon={<PlusIcon />}
                style={{ color: '#f89420 ', borderColor: '#f89420 ' }}
              >
                Thêm mới
              </Button>
            </div>
          </Col>
        </Row>
      </Form>
    </div>
  );
};
