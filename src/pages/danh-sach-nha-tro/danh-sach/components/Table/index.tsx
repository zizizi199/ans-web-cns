import { FormInstance, Spin, Switch, Tag } from 'antd';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import { ProductTypeArray, Table, roomStatusApprove, roomStatusApproveType } from 'src/common';
import { DeleteIcon, EditIcon, PreviewIcon } from 'src/components/Icon';
import styles from './styles.module.scss';
import { HostelRoutes } from 'src/helpers/app.routes';

interface Props {
  form: FormInstance<any>;
  dsUser: any;
  isLoading: boolean;
  currentPage: number;
  pageSize: number;
  totalElement: number;
  onChangePage: (page: number) => void;
  onShowSizeChange: (current: number, size: number) => void;
  handleVisibleChangeModalDelete: (status: any, record: any) => void;
  dsApproveProduct: any;
  onApproveProduct: (value: any) => void;
  onChangePost: (record: any) => void;
}

export const TableForm: React.FC<Props> = ({
  dsUser,
  isLoading,
  onChangePage,
  onShowSizeChange,
  currentPage,
  pageSize,
  totalElement,
  handleVisibleChangeModalDelete,
  dsApproveProduct,
  onApproveProduct,
  onChangePost,
}) => {
  const navigate = useNavigate();

  const rowSelection = {
    type: 'checkbox',
    columnWidth: '1%',
    selectedRowKeys: dsApproveProduct,
    onChange: (selectedRowKeys: React.Key[]) => {
      onApproveProduct(selectedRowKeys);
    },

    getCheckboxProps: (record: any) => ({
      disabled: record.isApproved === roomStatusApproveType.NON_APPORVE ? false : true,
      name: record.id,
    }),
  };

  return (
    <div className={styles.TableForm}>
      <Spin spinning={isLoading}>
        <Table
          pagination={{
            size: 'default',
            total: totalElement,
            onChange: onChangePage,
            pageSize: pageSize,
            current: currentPage,
            onShowSizeChange: onShowSizeChange,
            showTotal: (total) => (
              <b>
                Tổng bản ghi <b style={{ color: '#F89420' }}>{total} </b> |
              </b>
            ),
          }}
          rowKey={'id'}
          rowSelection={rowSelection}
          dataSource={dsUser}
          columns={[
            {
              title: <div className="header-table">STT</div>,
              width: '2%',
              dataIndex: 'id',
              render: (value, row, index) => (currentPage - 1) * pageSize + index + 1,
            },
            {
              title: <div className="header-table">Mã nhà trọ</div>,
              width: '5%',
              dataIndex: 'code',
              ellipsis: true,
              render: (value) => value,
            },
            {
              title: (
                <div style={{ textAlign: 'center' }} className="header-table">
                  Địa chỉ
                </div>
              ),
              width: '15%',
              dataIndex: 'addressBuilding',
              align: 'left',
              ellipsis: true,
              render: (value) =>
                `${value?.address} ${value?.street}, ${value?.wards}, ${value?.district}, ${value?.province}`,
            },
            {
              title: <div className="header-table">Diện tích</div>,
              width: '5%',
              dataIndex: 'maxArea',
              render: (value, row) => {
                return (
                  <p>
                    {row?.minArea}m&#x00B2;-{value}m&#x00B2;
                  </p>
                );
              },
            },
            {
              title: <div className="header-table">Loại phòng</div>,
              width: '5%',
              dataIndex: 'productType',
              align: 'center',
              ellipsis: true,
              render: (value) => ProductTypeArray.find((item) => item?.key === value)?.label,
            },
            {
              title: <div className="header-table">Trạng thái</div>,
              width: '5%',
              dataIndex: 'isApproved',
              align: 'center',
              ellipsis: true,
              render: (value) => (
                <Tag
                  color={
                    value === roomStatusApproveType.APPORVE
                      ? 'green'
                      : value === roomStatusApproveType.NON_APPORVE
                      ? 'gold'
                      : 'red'
                  }
                >
                  {roomStatusApprove.find((item: any) => item?.key === value)?.label}
                </Tag>
              ),
            },
            {
              title: <div className="header-table">Thời gian mở bán</div>,
              width: '5%',
              dataIndex: 'periodPaymentInMonths',
              align: 'center',
              ellipsis: true,
              render: (value) => value + ' tháng',
            },
            {
              title: <div className="header-table">Hiển thị</div>,
              width: '5%',
              dataIndex: 'status',
              align: 'center',
              ellipsis: true,
              render: (value: any, row: any) => (
                <Switch onChange={() => onChangePost(row)} checked={value === 1 ? true : false} />
              ),
            },
            {
              title: <div className="header-table">Hành động</div>,
              width: '5%',
              dataIndex: 'id',
              align: 'center',
              render: (value, row) => {
                return (
                  <div style={{ display: 'flex', cursor: 'pointer', justifyContent: 'space-around' }}>
                    <PreviewIcon
                      style={{ marginTop: '5px' }}
                      onClick={() => {
                        navigate(HostelRoutes.details(value, false));
                      }}
                    />
                    <EditIcon
                      onClick={() => {
                        navigate(HostelRoutes.details(value, true));
                      }}
                    />
                    <DeleteIcon
                      onClick={() => {
                        handleVisibleChangeModalDelete(true, row);
                      }}
                    />
                  </div>
                );
              },
            },
          ]}
        />
      </Spin>
    </div>
  );
};
