import { Form, message } from 'antd';
import React, { useEffect, useState } from 'react';
import { combineUrlParams } from 'src/common';
import axiosIns from 'src/helpers/request';
import { FilterForm } from '../components/FilterForm';
import { TableForm } from '../components/Table';
import styles from './styles.module.scss';
import { EVALUATE_API } from 'src/helpers/api';
import useDebounce from 'src/hook/useDebounce';
import { useLocation } from 'react-router-dom';

function Container() {
  const [form] = Form.useForm();
  const [isLoading, setIsLoading] = useState(false);
  const [dsUser, setDsUser] = useState<any>([]);
  const [totalElement, setTotalElement] = useState<any>(0);
  const [filter, setFilter] = useState<any>({ limit: 10, page: 1 });
  const [dsApproveOrder, setDsApproveOrder] = useState<any>([]);
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const paramValue = searchParams.get('paramName');

  //   Gọi API ds
  async function fetchSearchUser(params = {}) {
    return await new Promise((resolve, reject) => {
      setIsLoading(true);
      axiosIns
        .get(combineUrlParams(`${EVALUATE_API.LIST}`, { ...params }))
        .then((s) => {
          setDsUser(s?.data?.data?.content);
          setTotalElement(s?.data?.data?.totalElements);
          resolve(s?.data?.data?.content);
          setIsLoading(false);
          setFilter(params);
        })
        .catch((e) => {
          setIsLoading(false);
          reject(e);
        });
    });
    // .then
  }

  // API duyệt hàng loạt đơn hàng
  const fetchApporveListOrder = async (ds: any, status: any) => {
    return await new Promise((resolve, reject) => {
      setIsLoading(true);
      axiosIns
        .put(`${EVALUATE_API.STATUS}`, {
          ids: ds,
          status: status,
        })
        .then((s) => {
          if (s?.data?.code === 200) {
            message.success(s?.data?.message);
            setDsApproveOrder([]);
            setTimeout(() => {
              setIsLoading(false);
              fetchSearchUser();
              resolve(s?.data?.data);
            }, 500);
          }
        })
        .catch((e) => {
          message.error(e?.response?.data?.message);
          setIsLoading(false);
          reject(e);
        });
    });
    // .then
  };

  useEffect(() => {
    fetchSearchUser({ ...filter }).catch(console.error);
  }, []);

  const [keyWordValue, setKeyWordValue] = useState('');
  const onFilter = (value: any) => {
    setKeyWordValue(value);
  };
  const debounceSearch = useDebounce(keyWordValue, 500);

  useEffect(() => {
    const value: any = {
      ...debounceSearch,
      keyword: paramValue,
    };
    setKeyWordValue(value);
  }, [paramValue]);

  useEffect(() => {
    if (debounceSearch) {
      const input = {
        ...filter,
        keyword: paramValue,
        startTime: debounceSearch?.startTime ? (debounceSearch?.startTime).format('YYYY-MM-DD') : null,
        endTime: debounceSearch?.endTime ? (debounceSearch?.endTime).format('YYYY-MM-DD') : null,
        status: debounceSearch?.status,
        page: 1,
        limit: 10,
      };
      fetchSearchUser(input);
    }
  }, [debounceSearch]);

  const onShowSizeChange = (current: number, size: number) => {
    fetchSearchUser({
      ...filter,
      limit: size,
    });
  };

  const onChangePage = (page: number) => {
    fetchSearchUser({
      ...filter,
      page: page,
    });
  };

  const onApproveOrder = (value: any) => {
    setDsApproveOrder(value);
  };

  return (
    <div className={styles.container}>
      <div className="sub-container">
        <p className="container-title">Danh sách đánh giá</p>
        <FilterForm
          form={form}
          onFilter={onFilter}
          dsApproveOrder={dsApproveOrder}
          fetchApporveListOrder={fetchApporveListOrder}
        />
        <TableForm
          totalElement={totalElement}
          currentPage={filter?.page}
          pageSize={filter?.limit}
          onChangePage={onChangePage}
          onShowSizeChange={onShowSizeChange}
          dsUser={dsUser}
          isLoading={isLoading}
          form={form}
          dsApproveOrder={dsApproveOrder}
          onApproveOrder={onApproveOrder}
        />
      </div>
    </div>
  );
}

export default Container;
