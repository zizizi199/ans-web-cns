import React from 'react';
import { Button, Col, DatePicker, Form, FormInstance, Row, Select } from 'antd';
import styles from './styles.module.scss';
import { AcceptWhiteIcon, DeclineIcon } from 'src/components/Icon';
import { statusCommentArr } from 'src/common';

const { Option } = Select;

interface Props {
  form: FormInstance<any>;
  onFilter: (value: object) => void;
  dsApproveOrder: any;
  fetchApporveListOrder: (ds: any, status: any) => void;
}

export const FilterForm: React.FC<Props> = ({ form, onFilter, dsApproveOrder, fetchApporveListOrder }) => {
  const onChange = () => {
    onFilter(form.getFieldsValue());
  };

  return (
    <div className={styles.FilterForm}>
      <Form form={form}>
        <Row>
          <Col span={18}>
            <Row>
              <Col span={4} style={{ marginRight: '10px' }}>
                <Form.Item name="startTime">
                  <DatePicker inputReadOnly={true} format={'DD-MM-YYYY'} onChange={onChange} placeholder={'Từ ngày'} />
                </Form.Item>
              </Col>{' '}
              <Col span={4} style={{ marginRight: '10px' }}>
                <Form.Item name="endTime">
                  <DatePicker inputReadOnly={true} format={'DD-MM-YYYY'} onChange={onChange} placeholder={'Đến ngày'} />
                </Form.Item>
              </Col>
              <Col span={4} style={{ marginRight: '10px' }}>
                <Form.Item name="status">
                  <Select onChange={onChange} placeholder="Trạng thái">
                    <Option>Tất cả</Option>
                    {statusCommentArr?.map((item: any, index: number) => {
                      return (
                        <Option value={item?.id} key={index}>
                          {item?.name}
                        </Option>
                      );
                    })}
                  </Select>
                </Form.Item>
              </Col>
            </Row>
          </Col>
          <Col span={6}>
            <div className="button-float">
              <Button
                disabled={dsApproveOrder.length <= 0 || !dsApproveOrder}
                icon={<DeclineIcon />}
                onClick={() => fetchApporveListOrder(dsApproveOrder, 2)}
                style={{
                  marginLeft: '15px',
                  width: '145px',
                  backgroundColor: 'white',
                  color: '#E20D00',
                  borderColor: '#E20D00',
                }}
              >
                Từ chối
              </Button>
              <Button
                disabled={dsApproveOrder.length <= 0 || !dsApproveOrder}
                icon={<AcceptWhiteIcon />}
                onClick={() => fetchApporveListOrder(dsApproveOrder, 1)}
                style={{ marginLeft: '15px', width: '145px', backgroundColor: '#1DC951', color: 'white' }}
              >
                Duyệt
              </Button>
            </div>
          </Col>
        </Row>
      </Form>
    </div>
  );
};
