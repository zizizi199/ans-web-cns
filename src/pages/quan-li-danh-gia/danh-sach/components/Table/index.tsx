import { FormInstance, Rate, Spin, Tag } from 'antd';
import React from 'react';
import { Table, statusCommentArr } from 'src/common';
import styles from './styles.module.scss';
import dayjs from 'dayjs';
import 'dayjs/locale/vi';

dayjs.locale('vi');

interface Props {
  form: FormInstance<any>;
  dsUser: any;
  isLoading: boolean;
  currentPage: number;
  pageSize: number;
  totalElement: number;
  onChangePage: (page: number) => void;
  onShowSizeChange: (current: number, size: number) => void;
  dsApproveOrder: any;
  onApproveOrder: (value: any) => void;
}

export const TableForm: React.FC<Props> = ({
  dsUser,
  isLoading,
  onChangePage,
  onShowSizeChange,
  currentPage,
  pageSize,
  totalElement,
  dsApproveOrder,
  onApproveOrder,
}) => {
  const rowSelection = {
    type: 'checkbox',
    columnWidth: '1%',
    selectedRowKeys: dsApproveOrder,
    onChange: (selectedRowKeys: React.Key[]) => {
      onApproveOrder(selectedRowKeys);
    },

    getCheckboxProps: (record: any) => ({
      disabled: record?.status === 1 || record?.status === 2,
      name: record.id,
    }),
  };

  return (
    <div className={styles.TableForm}>
      <Spin spinning={isLoading}>
        <Table
          pagination={{
            size: 'default',
            total: totalElement,
            onChange: onChangePage,
            pageSize: pageSize,
            current: currentPage,
            onShowSizeChange: onShowSizeChange,
            showTotal: (total) => (
              <b>
                Tổng bản ghi <b style={{ color: '#F89420' }}>{total} </b> |
              </b>
            ),
          }}
          dataSource={dsUser}
          rowSelection={rowSelection}
          rowKey={'id'}
          columns={[
            {
              title: <div className="header-table">STT</div>,
              width: '2%',
              dataIndex: 'id',
              render: (value, row, index) => (currentPage - 1) * pageSize + index + 1,
            },
            {
              title: <div className="header-table">Người viết</div>,
              width: '5%',
              dataIndex: 'reviewer',
              ellipsis: true,
              render: (value) => value?.fullname,
            },
            {
              title: <div className="header-table">Thời gian</div>,
              width: '5%',
              dataIndex: 'createdAt',
              render: (value) => dayjs(value, 'DD/MM/YYYY').format('DD/MM/YYYY'),
            },

            {
              title: <div className="header-table">Đánh giá</div>,
              width: '5%',
              dataIndex: 'rate',
              align: 'center',
              ellipsis: true,
              render: (value) => <Rate value={value} disabled />,
            },
            {
              title: <div className="header-table">Nội dung đánh giá</div>,
              width: '20%',
              dataIndex: 'comment',
              align: 'center',
              render: (value) => value,
            },
            {
              title: <div className="header-table">Trạng thái</div>,
              width: '5%',
              dataIndex: 'status',
              align: 'center',
              ellipsis: true,
              render: (value) => (
                <Tag color={value === 1 ? 'green' : value === 0 ? 'blue' : 'red'}>
                  {statusCommentArr.find((item: any) => item?.id === value)?.name}
                </Tag>
              ),
            },
            {
              title: <div className="header-table">Người xử lí</div>,
              width: '5%',
              dataIndex: 'handler',
              align: 'center',
              ellipsis: true,
              render: (value) => (value ? value?.fullname : ''),
            },
          ]}
        />
      </Spin>
    </div>
  );
};
