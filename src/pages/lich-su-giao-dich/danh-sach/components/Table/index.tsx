import { FormInstance, Spin, Tag } from 'antd';
import moment from 'moment';
import React from 'react';
import { Table, formatPrice, statusTransactionArr, statusTransactionType } from 'src/common';
import styles from './styles.module.scss';

interface Props {
  form: FormInstance<any>;
  dsUser: any;
  isLoading: boolean;
  currentPage: number;
  pageSize: number;
  totalElement: number;
  onChangePage: (page: number) => void;
  onShowSizeChange: (current: number, size: number) => void;
}

export const TableForm: React.FC<Props> = ({
  dsUser,
  isLoading,
  onChangePage,
  onShowSizeChange,
  currentPage,
  pageSize,
  totalElement,
}) => {
  return (
    <div className={styles.TableForm}>
      <Spin spinning={isLoading}>
        <Table
          pagination={{
            size: 'default',
            total: totalElement,
            onChange: onChangePage,
            pageSize: pageSize,
            current: currentPage,
            onShowSizeChange: onShowSizeChange,
            showTotal: (total) => (
              <b>
                Tổng bản ghi <b style={{ color: '#F89420' }}>{total} </b> |
              </b>
            ),
          }}
          dataSource={dsUser}
          columns={[
            {
              title: <div className="header-table">STT</div>,
              width: '2%',
              dataIndex: 'id',
              render: (value, row, index) => (currentPage - 1) * pageSize + index + 1,
            },
            {
              title: <div className="header-table">Thời gian</div>,
              width: '5%',
              dataIndex: 'createdAt',
              render: (value) => moment(value).format('HH:mm DD/MM/YYYY'),
            },
            {
              title: <div className="header-table">Mã giao dịch</div>,
              width: '5%',
              dataIndex: 'transactionCode',
              ellipsis: true,
            },
            {
              title: <div className="header-table">Số tiền</div>,
              width: '8%',
              dataIndex: 'amount',
              align: 'center',
              ellipsis: true,
              render: (value) => formatPrice(value),
            },
            {
              title: <div className="header-table">Tài khoản chuyển đến</div>,
              width: '5%',
              dataIndex: 'bankNumber',
              align: 'center',
              render: (value) => value,
            },
            {
              title: <div className="header-table">Nội dung chuyển khoản</div>,
              width: '5%',
              dataIndex: 'contentTransaction',
              align: 'center',
              ellipsis: true,
              render: (value) => value,
            },
            {
              title: <div className="header-table">Trạng thái</div>,
              width: '5%',
              dataIndex: 'status',
              align: 'center',
              render: (value) => (
                <Tag
                  color={
                    value === statusTransactionType.COMPLETE
                      ? 'green'
                      : value === statusTransactionType.PENDING
                      ? 'gold'
                      : 'red'
                  }
                >
                  {statusTransactionArr.find((item: any) => item?.id === value)?.name}
                </Tag>
              ),
            },
          ]}
        />
      </Spin>
    </div>
  );
};
