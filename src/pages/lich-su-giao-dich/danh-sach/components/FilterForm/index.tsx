import React from 'react';
import { Col, DatePicker, Form, FormInstance, Row, Select } from 'antd';
import styles from './styles.module.scss';
import { statusTransactionArr } from 'src/common';

const { Option } = Select;
interface Props {
  form: FormInstance<any>;
  onFilter: (value: object) => void;
}

export const FilterForm: React.FC<Props> = ({ form, onFilter }) => {
  const onChange = () => {
    onFilter(form.getFieldsValue());
  };

  return (
    <div className={styles.FilterForm}>
      <Form form={form}>
        <Row>
          <Col span={18}>
            <Row>
              <Col span={4} style={{ marginRight: '10px' }}>
                <Form.Item name="startTime">
                  <DatePicker inputReadOnly={true} format={'DD-MM-YYYY'} onChange={onChange} placeholder={'Từ ngày'} />
                </Form.Item>
              </Col>{' '}
              <Col span={4} style={{ marginRight: '10px' }}>
                <Form.Item name="endTime">
                  <DatePicker inputReadOnly={true} format={'DD-MM-YYYY'} onChange={onChange} placeholder={'Đến ngày'} />
                </Form.Item>
              </Col>
              <Col span={4} style={{ marginRight: '10px' }}>
                <Form.Item name="status">
                  <Select onChange={onChange} placeholder="Trạng thái">
                    <Option>Tất cả</Option>
                    {statusTransactionArr?.map((item: any, index: number) => {
                      return (
                        <Option value={item?.id} key={index}>
                          {item?.name}
                        </Option>
                      );
                    })}
                  </Select>
                </Form.Item>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
    </div>
  );
};
