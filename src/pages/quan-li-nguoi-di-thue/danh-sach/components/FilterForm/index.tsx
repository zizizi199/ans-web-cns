import React from 'react';
import { Button, Col, Form, FormInstance, Row } from 'antd';
import styles from './styles.module.scss';
import { PlusIcon } from 'src/components/Icon';
interface Props {
  form: FormInstance<any>;
  onFilter: (value: object) => void;
  handleVisibleChangeModalOwner: (status: any, id: any) => void;
}

export const FilterForm: React.FC<Props> = ({ form, handleVisibleChangeModalOwner }) => {
  return (
    <div className={styles.FilterForm}>
      <Form form={form}>
        <Row>
          <Col span={18}>
            <Row>
              <Col span={4}>
                <Form.Item name="keyword"></Form.Item>
              </Col>
            </Row>
          </Col>
          <Col span={6}>
            <div className="button-float">
              <Button
                onClick={() => {
                  handleVisibleChangeModalOwner(true, null);
                }}
                icon={<PlusIcon />}
                style={{ backgroundColor: '#1A94FF', color: 'white' }}
              >
                Thêm mới
              </Button>
            </div>
          </Col>
        </Row>
      </Form>
    </div>
  );
};
