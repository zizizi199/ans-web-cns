import { Button, Col, Form, Modal, Row, message } from 'antd';
import React from 'react';
import styles from './styles.module.scss';
import { formValidateMessages } from 'src/pages/constants';
import axiosIns from 'src/helpers/request';
import { combineUrlParams } from 'src/common';
import { TENANT_API } from 'src/helpers/api';
import deleteImg from 'src/components/Icon/warning.png';

interface Props {
  modalValue: any;
  openModalDelete: boolean;
  handleVisibleChangeModalDelete: (status: any, value: any) => void;
  fetchSearchUser: (value: any) => void;
}

const ModalDelete: React.FC<Props> = ({
  modalValue,
  openModalDelete,
  handleVisibleChangeModalDelete,
  fetchSearchUser,
}) => {
  const [form] = Form.useForm();

  const handleCancel = () => {
    handleVisibleChangeModalDelete(false, null);
    form.resetFields();
  };

  const onSubmit = async () => {
    return await new Promise((resolve, reject) => {
      axiosIns
        .delete(combineUrlParams(`${TENANT_API.DELETE}/${modalValue.id}`))
        .then((s) => {
          message.success('Xóa người đi thuê thành công');
          resolve(s);
          handleCancel();
          fetchSearchUser({ page: 1, limit: 10 });
        })
        .catch((e) => {
          if (e?.response?.data?.statusCode >= 400) {
            message.error(e?.response?.data?.message);
          }
          reject(e);
        });
    });
    // .then
  };

  return (
    <Modal
      onCancel={handleCancel}
      className={styles.container}
      open={openModalDelete}
      title={`XÁC NHẬN`}
      footer={[
        <div
          key={Math.random()}
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            paddingBottom: 20,
          }}
        >
          <Button
            style={{
              backgroundColor: '#C8CBD0',
              color: 'white',
              width: '60px',
              borderRadius: '48px',
              // height: "48px",
            }}
            onClick={handleCancel}
            key={'cancel'}
          >
            Hủy
          </Button>
          <Button
            style={{
              backgroundColor: '#F89420',
              color: 'white',
              width: '60px',
              borderRadius: '48px',
              // height: "48px",
            }}
            onClick={onSubmit}
            type="primary"
            key={'ok'}
          >
            Xóa
          </Button>
        </div>,
      ]}
    >
      <Form
        form={form}
        validateMessages={formValidateMessages}
        initialValues={{ remember: true }}
        autoComplete="off"
        onFinish={onSubmit}
        layout="vertical"
        labelAlign="left"
        labelCol={{ span: 24, offset: 0 }}
        wrapperCol={{ span: 23 }}
      >
        <Row>
          <Col span={6} style={{ display: 'flex', alignItems: 'center' }}>
            <img src={deleteImg} width={80} height={80}></img>
          </Col>
          <Col span={18} style={{ display: 'flex', alignItems: 'center' }}>
            <p>
              Bạn có chắc chắn muốn xóa người đi thuê <span className="ellipsis">{modalValue?.fullName}</span> không?
            </p>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

export default ModalDelete;
