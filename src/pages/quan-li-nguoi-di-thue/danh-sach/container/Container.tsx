import { Form, message } from 'antd';
import React, { useEffect, useState } from 'react';
import { combineUrlParams } from 'src/common';
import axiosIns from 'src/helpers/request';
import { FilterForm } from '../components/FilterForm';
import { TableForm } from '../components/Table';
import styles from './styles.module.scss';
import { OWNER_API, TENANT_API } from 'src/helpers/api';
import useDebounce from 'src/hook/useDebounce';
import ModalOwner from '../components/ModalOwner';
import ModalDelete from '../components/ModalDelete';
import { useLocation } from 'react-router-dom';

function Container() {
  const [form] = Form.useForm();
  const [isLoading, setIsLoading] = useState(false);
  const [isDetail, setIsDetail] = useState(false);
  const [dsUser, setDsUser] = useState<any>([]);
  const [totalElement, setTotalElement] = useState<number>(0);
  const [filter, setFilter] = useState<any>({ limit: 10, page: 1 });
  const [openModalOwner, setOpenModalOwner] = useState(false);
  const [openModalDelete, setOpenModalDelete] = useState(false);
  const [modalValue, setModalValue] = useState<any>(null);
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const paramValue = searchParams.get('paramName');

  async function fetchSearchUser(params = {}) {
    return await new Promise((resolve, reject) => {
      setIsLoading(true);
      axiosIns
        .get(combineUrlParams(`${TENANT_API.LIST}`, { ...params, type: 1 }))
        .then((s) => {
          setDsUser(s?.data?.data?.content);
          setTotalElement(s?.data?.data?.totalElements);
          resolve(s?.data?.data?.content);
          setIsLoading(false);
          setFilter(params);
        })
        .catch((e) => {
          setIsLoading(false);
          reject(e);
        });
    });
    // .then
  }

  // API chi tiết
  const fetchDetail = async (value) => {
    return await new Promise((resolve, reject) => {
      setIsLoading(true);
      setIsDetail(true);
      axiosIns
        .get(`${TENANT_API.DETAIL}/${value}`)
        .then((s) => {
          setIsLoading(false);
          setModalValue(s?.data?.data);
          resolve(s?.data?.data);
        })
        .catch((e) => {
          setIsLoading(false);
          reject(e);
        });
    });
  };

  useEffect(() => {
    fetchSearchUser({ ...filter }).catch(console.error);
  }, []);

  const [keyWordValue, setKeyWordValue] = useState('');
  const onFilter = (value: any) => {
    setKeyWordValue(value);
  };
  const debounceSearch = useDebounce(keyWordValue, 500);

  useEffect(() => {
    const value: any = {
      keyword: paramValue,
    };
    setKeyWordValue(value);
  }, [paramValue]);

  useEffect(() => {
    if (debounceSearch) {
      const input = {
        ...filter,
        keyword: debounceSearch?.keyword,
        page: 1,
        limit: 10,
      };
      fetchSearchUser(input);
    }
  }, [debounceSearch]);

  const onShowSizeChange = (current: number, size: number) => {
    fetchSearchUser({
      ...filter,
      limit: size,
    });
  };

  const onChangePage = (page: number) => {
    fetchSearchUser({
      ...filter,
      page: page,
    });
  };

  const handleVisibleChangeModalOwner = async (status: any, id: any) => {
    setOpenModalOwner(status);
    if (id) {
      await fetchDetail(id);
    } else {
      setModalValue(null);
      setIsDetail(false);
    }
  };

  const handleVisibleChangeModalDelete = (status: any, value: any) => {
    setOpenModalDelete(status);
    setModalValue(value);
  };

  const onChangePost = (record: any) => {
    return new Promise((resolve, reject) => {
      setIsLoading(true);
      axiosIns
        .put(`${OWNER_API.BLOCKUSER}/${record?.id}`, {})
        .then((s) => {
          if (s?.data?.code === 200) {
            message.success(s?.data?.message);
            setTimeout(() => {
              setIsLoading(false);
              fetchSearchUser({ page: 1, limit: 10 });
              resolve(s?.data?.data);
            }, 500);
          } else if (s?.data?.code >= 400) {
            message.error(s?.data?.message);
            setIsLoading(false);
          }
        })
        .catch((e) => {
          setIsLoading(false);
          message.error(e?.response?.data?.message);
          reject(e);
        });
    });
  };

  return (
    <div className={styles.container}>
      <div className="sub-container">
        <p className="container-title">Danh sách người đi thuê</p>
        <FilterForm form={form} onFilter={onFilter} handleVisibleChangeModalOwner={handleVisibleChangeModalOwner} />
        <TableForm
          onChangePost={onChangePost}
          totalElement={totalElement}
          currentPage={filter?.page}
          pageSize={filter?.limit}
          onChangePage={onChangePage}
          onShowSizeChange={onShowSizeChange}
          dsUser={dsUser}
          isLoading={isLoading}
          form={form}
          handleVisibleChangeModalOwner={handleVisibleChangeModalOwner}
          handleVisibleChangeModalDelete={handleVisibleChangeModalDelete}
        />
        <ModalOwner
          openModalOwner={openModalOwner}
          handleVisibleChangeModalOwner={handleVisibleChangeModalOwner}
          fetchSearchUser={fetchSearchUser}
          modalValue={modalValue}
          isLoadingDetail={isLoading}
          isDetail={isDetail}
        />
        <ModalDelete
          modalValue={modalValue}
          openModalDelete={openModalDelete}
          handleVisibleChangeModalDelete={handleVisibleChangeModalDelete}
          fetchSearchUser={fetchSearchUser}
        />
      </div>
    </div>
  );
}

export default Container;
