import { Spin } from 'antd';
import React from 'react';
import styles from './styles.module.scss';

const Dashboard = () => {
  return (
    <>
      <Spin spinning={false}>
        <div className={styles.container}></div>
      </Spin>
    </>
  );
};

export default Dashboard;
