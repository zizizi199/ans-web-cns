import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button, Checkbox, Form, Input, Spin, Typography } from 'antd';
import { AUTH } from 'src/helpers/api';
import { AppRoutes } from 'src/helpers/app.routes';
import styles from './styles.module.scss';
import axiosIns from 'src/helpers/request';
import { formValidateMessages } from 'src/pages/constants';
import imgLogin from 'src/components/Icon/img-logo.png';

export const LoginContainer = ({ isLogOut }) => {
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState('');
  const [form] = Form.useForm();

  useEffect(() => {
    if (isLogOut) {
      localStorage.clear();
    }
  }, []);

  const onLogin = (values: any) => {
    setIsLoading(true);
    axiosIns
      .post(AUTH.LOGIN, {
        ...values,
      })
      .then(({ data }) => {
        setIsLoading(false);
        localStorage.setItem('accessToken', data.data?.accessToken);
        localStorage.setItem('userId', data?.data?.id);
        localStorage.setItem('username', data?.data?.username);
        localStorage.setItem('fullName', data?.data?.fullName ? data?.data?.fullName : '');
        localStorage.setItem('userPhoneNumber', data?.data?.phone);
        localStorage.setItem('userType', data?.data?.userType);
        localStorage.setItem('status', data.data?.status);
        localStorage.setItem('avatar', data.data?.avatar ? data.data?.avatar : '');
        localStorage.setItem('isListPage', 'true');
        localStorage.setItem('isVerify', data.data?.isVerify);
        setIsLoading(false);
        window.location.href = AppRoutes.home;
      })
      .catch((e) => {
        setIsLoading(false);
        setError(e?.response?.data?.message);
      });
  };

  // const validatePassword = (rule: any, value: any, callback: any) => {
  //   if (value?.length < 8) {
  //     callback(new Error('Mật khẩu phải ít nhất 8 kí tự'));
  //   } else if (value?.length > 255) {
  //     callback(new Error('Mật khẩu không vượt quá 255 ký tự'));
  //   }
  // };

  // const validateEmail = (rule: any, value: any, callback: any) => {
  //   const re = PATTERN_EMAIL;
  //   if (!value) {
  //     callback(new Error('Thông tin bắt buộc'));
  //   } else {
  //     if (!re.test(value.trim().toLowerCase())) {
  //       callback(new Error('Email không đúng định dạng'));
  //     }
  //   }
  // };

  const onFinish = (values: any) => {
    const input = {
      username: values.username?.trim(),
      password: values.password,
      userType: 3,
    };
    onLogin(input);
    // }
  };

  return (
    <div>
      <div className={styles.login}>
        {/* <MSBLogoIcon className={styles.logo} /> */}
        <Form
          name="basic"
          initialValues={{ remember: true }}
          autoComplete="off"
          onFinish={onFinish}
          className={styles.form}
          validateMessages={formValidateMessages}
          form={form}
        >
          <div className={styles.topLogin}>
            <img src={imgLogin} alt="imgLogin" className={styles.imgLogin} />
            <Typography.Title level={4} className={styles.text} style={{ textAlign: 'center' }}>
              BẮT ĐẦU TRẢI NGHIỆM CỦA BẠN
              <br />
              <Typography.Title level={4} style={{ textAlign: 'center', color: '#F89420' }}>
                NGAY BÂY GIỜ
              </Typography.Title>
            </Typography.Title>
          </div>
          <Typography.Title level={5} className={styles.text}>
            Email hoặc số điện thoại
          </Typography.Title>
          <Form.Item
            name="username"
            rules={[{ required: true }, { max: 255, message: 'Email không vượt quá 255 ký tự' }]}
          >
            <Input placeholder="Email hoặc số điện thoại" />
          </Form.Item>
          <Typography.Title level={5} className={styles.text}>
            Mật khẩu
          </Typography.Title>
          <Form.Item name="password" rules={[{ required: true }]}>
            <Input.Password placeholder="Mật khẩu" />
          </Form.Item>
          <div className={styles.error}>{error}</div>
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
            }}
          >
            <Form.Item name="checkbox">
              <Checkbox>Duy trì đăng nhập</Checkbox>
            </Form.Item>
            <Form.Item>
              <a style={{ color: '#F89420' }} onClick={() => navigate(AppRoutes.forgot_password)}>
                {' '}
                Quên mật khẩu?
              </a>
            </Form.Item>
          </div>
          <Form.Item>
            <Spin spinning={isLoading}>
              <Button htmlType="submit" block className={styles.button}>
                Đăng nhập
              </Button>
            </Spin>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};
