import React from 'react';
import { LoginContainer } from './containers/LoginContainer';
import LogoBig from 'src/components/Icon/logo-big.png';
import styles from './styles.module.scss';

export default function LoginPage({ isLogOut }) {
  return (
    <div className={styles.container}>
      <div className={styles.backGround}>
        <img src={LogoBig} alt={'Ans Logo'} className={styles.logoBig} />
      </div>
      <div className={styles.loginForm}>
        <LoginContainer isLogOut={isLogOut} />;
      </div>
    </div>
  );
}
