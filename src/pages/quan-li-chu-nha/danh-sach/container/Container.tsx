import { Form, message } from 'antd';
import React, { useEffect, useState } from 'react';
import { combineUrlParams } from 'src/common';
import axiosIns from 'src/helpers/request';
import { FilterForm } from '../components/FilterForm';
import { TableForm } from '../components/Table';
import styles from './styles.module.scss';
import { OWNER_API } from 'src/helpers/api';
import useDebounce from 'src/hook/useDebounce';
import ModalOwner from '../components/ModalOwner';
import ModalFeature from '../components/ModalFeature';
import ModalDelete from '../components/ModalDelete';
import { useLocation } from 'react-router-dom';

function Container() {
  const [form] = Form.useForm();
  const [isLoading, setIsLoading] = useState(false);
  const [isDetail, setIsDetail] = useState(false);
  const [dsUser, setDsUser] = useState<any>([]);
  const [totalElement, setTotalElement] = useState<any>(0);
  const [filter, setFilter] = useState<any>({ limit: 10, page: 1 });
  const [openModalFeature, setOpenModalFeature] = useState(false);
  const [openModalOwner, setOpenModalOwner] = useState(false);
  const [openModalDelete, setOpenModalDelete] = useState(false);
  const [type, setType] = useState();
  const [id, setId] = useState();
  const [modalValue, setModalValue] = useState(null);
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const paramValue = searchParams.get('paramName');

  //   Gọi API ds user
  async function fetchSearchUser(params = {}) {
    return await new Promise((resolve, reject) => {
      setIsLoading(true);
      axiosIns
        .get(combineUrlParams(`${OWNER_API.LIST}`, { ...params, type: 2 }))
        .then((s) => {
          setDsUser(s?.data?.data?.content);
          setTotalElement(s?.data?.data?.totalElements);
          resolve(s?.data?.data?.content);
          setIsLoading(false);
          setFilter(params);
        })
        .catch((e) => {
          setIsLoading(false);
          reject(e);
        });
    });
    // .then
  }

  // API chi tiết
  const fetchDetail = async (value) => {
    return await new Promise((resolve, reject) => {
      setIsLoading(true);
      setIsDetail(true);
      axiosIns
        .get(`${OWNER_API.DETAIL}/${value}`)
        .then((s) => {
          setIsLoading(false);
          setModalValue(s?.data?.data);
          resolve(s?.data?.data);
        })
        .catch((e) => {
          setIsLoading(false);
          reject(e);
        });
    });
  };

  useEffect(() => {
    fetchSearchUser({ ...filter }).catch(console.error);
  }, []);

  const [keyWordValue, setKeyWordValue] = useState('');
  const onFilter = (value: any) => {
    setKeyWordValue(value);
  };
  const debounceSearch = useDebounce(keyWordValue, 500);

  useEffect(() => {
    const value: any = {
      keyword: paramValue,
    };
    setKeyWordValue(value);
  }, [paramValue]);

  useEffect(() => {
    if (debounceSearch) {
      const input = {
        ...filter,
        keyword: debounceSearch?.keyword,
        page: 1,
        limit: 10,
      };
      fetchSearchUser(input);
    }
  }, [debounceSearch]);

  const onShowSizeChange = (current: number, size: number) => {
    fetchSearchUser({
      ...filter,
      limit: size,
    });
  };

  const onChangePage = (page: number) => {
    fetchSearchUser({
      ...filter,
      page: page,
    });
  };
  const handleVisibleChangeModalFeature = (status: any, type: any) => {
    setType(type);
    setOpenModalFeature(status);
  };

  const handleVisibleChangeModalOwner = (status: any, id: any) => {
    setOpenModalOwner(status);
    if (id) {
      fetchDetail(id);
    } else {
      setModalValue(null);
      setIsDetail(false);
    }
  };

  const handleVisibleChangeModalDelete = (status: any, value: any) => {
    setOpenModalDelete(status);
    setModalValue(value);
  };

  const onChangePost = (checked, record, type) => {
    setId(record.id);
    if (checked === false) {
      handleVisibleChangeModalFeature(true, type);
    } else {
      if (type === 1) {
        return new Promise((resolve, reject) => {
          setIsLoading(true);
          axiosIns
            .put(`${OWNER_API.BLOCKPOST}/${record?.id}`, {})
            .then((s) => {
              if (s?.data?.code === 200) {
                message.success(s?.data?.message);
                setTimeout(() => {
                  setIsLoading(false);
                  fetchSearchUser({ page: 1, limit: 10 });
                  resolve(s?.data?.data);
                }, 500);
              } else if (s?.data?.code >= 400) {
                message.error(s?.data?.message);
                setIsLoading(false);
              }
            })
            .catch((e) => {
              setIsLoading(false);
              message.error(e?.response?.data?.message);
              reject(e);
            });
        });
      } else {
        return new Promise((resolve, reject) => {
          setIsLoading(true);
          axiosIns
            .put(`${OWNER_API.BLOCKUSER}/${record?.id}`, {})
            .then((s) => {
              if (s?.data?.code === 200) {
                message.success(s?.data?.message);
                setTimeout(() => {
                  setIsLoading(false);
                  fetchSearchUser({ page: 1, limit: 10 });
                  resolve(s?.data?.data);
                }, 500);
              } else if (s?.data?.code >= 400) {
                message.error(s?.data?.message);
                setIsLoading(false);
              }
            })
            .catch((e) => {
              setIsLoading(false);
              message.error(e?.response?.data?.message);
              reject(e);
            });
        });
      }
    }
    // const newData = dsUser.map((item) =>
    //   item.id === record.id ? { ...item, lockedPostForHost: checked === true ? 2 : 1 } : item
    // );
    // setDsUser(newData);
  };

  const handleSubmitPost = (value, type) => {
    const payload = {
      reason: value,
    };
    if (type === 1) {
      return new Promise((resolve, reject) => {
        setIsLoading(true);
        axiosIns
          .put(`${OWNER_API.BLOCKPOST}/${id}`, { ...payload })
          .then((s) => {
            if (s?.data?.code === 200) {
              message.success(s?.data?.message);
              setTimeout(() => {
                setIsLoading(false);
                handleVisibleChangeModalFeature(false, null);
                fetchSearchUser({ page: 1, limit: 10 });
                resolve(s?.data?.data);
              }, 500);
            } else if (s?.data?.code >= 400) {
              message.error(s?.data?.message);
              setIsLoading(false);
            }
          })
          .catch((e) => {
            setIsLoading(false);
            message.error(e?.response?.data?.message);
            reject(e);
          });
      });
    } else {
      return new Promise((resolve, reject) => {
        setIsLoading(true);
        axiosIns
          .put(`${OWNER_API.BLOCKUSER}/${id}`, { ...payload })
          .then((s) => {
            if (s?.data?.code === 200) {
              message.success(s?.data?.message);
              setTimeout(() => {
                setIsLoading(false);
                fetchSearchUser({ page: 1, limit: 10 });
                handleVisibleChangeModalFeature(false, null);
                resolve(s?.data?.data);
              }, 500);
            } else if (s?.data?.code >= 400) {
              message.error(s?.data?.message);
              setIsLoading(false);
            }
          })
          .catch((e) => {
            setIsLoading(false);
            message.error(e?.response?.data?.message);
            reject(e);
          });
      });
    }
  };

  return (
    <div className={styles.container}>
      <div className="sub-container">
        <p className="container-title">Danh sách chủ nhà</p>
        <FilterForm form={form} onFilter={onFilter} handleVisibleChangeModalOwner={handleVisibleChangeModalOwner} />
        <TableForm
          totalElement={totalElement}
          currentPage={filter?.page}
          pageSize={filter?.limit}
          onChangePage={onChangePage}
          onShowSizeChange={onShowSizeChange}
          dsUser={dsUser}
          isLoading={isLoading}
          form={form}
          handleVisibleChangeModalOwner={handleVisibleChangeModalOwner}
          handleVisibleChangeModalDelete={handleVisibleChangeModalDelete}
          onChangePost={onChangePost}
        />
        <ModalOwner
          openModalOwner={openModalOwner}
          handleVisibleChangeModalOwner={handleVisibleChangeModalOwner}
          fetchSearchUser={fetchSearchUser}
          modalValue={modalValue}
          isLoadingDetail={isLoading}
          isDetail={isDetail}
        />
        <ModalFeature
          type={type}
          openModalFeature={openModalFeature}
          handleVisibleChangeModalFeature={handleVisibleChangeModalFeature}
          handleSubmitPost={handleSubmitPost}
        />
        <ModalDelete
          modalValue={modalValue}
          openModalDelete={openModalDelete}
          handleVisibleChangeModalDelete={handleVisibleChangeModalDelete}
          fetchSearchUser={fetchSearchUser}
        />
      </div>
    </div>
  );
}

export default Container;
