import { Button, Col, DatePicker, Form, Input, Modal, Row, Spin, Upload, UploadFile, message } from 'antd';
import React, { useEffect, useState } from 'react';
import styles from './styles.module.scss';
import { disableDateAfter, disableDateEnd, formValidateMessages } from 'src/pages/constants';
import { PlusIcon } from 'src/components/Icon';
import axiosIns, { HOST } from 'src/helpers/request';
import { OWNER_API, UPLOAD } from 'src/helpers/api';
import { PATTERN_EMAIL, PATTERN_PHONE_NUMBER } from 'src/common/pattern';
import dayjs from 'dayjs';
import 'dayjs/locale/vi';

dayjs.locale('vi');
interface Props {
  openModalOwner: boolean;
  handleVisibleChangeModalOwner: (status: any, id: any) => void;
  fetchSearchUser: (value: any) => void;
  modalValue: any;
  isLoadingDetail: boolean;
  isDetail: boolean;
}

const ModalOwner: React.FC<Props> = ({
  openModalOwner,
  handleVisibleChangeModalOwner,
  fetchSearchUser,
  modalValue,
  isLoadingDetail,
  isDetail,
}) => {
  const [listimgFront, setListimgFront] = useState<any>(null);
  const [fileListimgFront, setFileListimgFront] = useState<UploadFile[]>([]);
  const [listimgBack, setListimgBack] = useState<any>(null);
  const [fileListimgBack, setFileListimgBack] = useState<UploadFile[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [form] = Form.useForm();

  useEffect(() => {
    setIsLoading(isLoadingDetail);
    if (modalValue !== null) {
      form.setFieldsValue({
        ...modalValue,
        birthday: dayjs(modalValue.birthday, 'YYYY/MM/DD'),
        releaseDateOfIdentify: dayjs(modalValue.releaseDateOfIdentify, 'YYYY/MM/DD'),
        expirationDateOfIdentify: dayjs(modalValue.expirationDateOfIdentify, 'YYYY/MM/DD'),
      });

      const imgBack: any = [];
      const idx = modalValue?.identityBackImage?.lastIndexOf('/') + 1;
      const filename = modalValue?.identityBackImage?.substr(idx);

      imgBack.push({
        uid: modalValue?.identityBackImage,
        name: filename,
        status: 'done',
        url:
          modalValue?.identityBackImage &&
          `${HOST}${UPLOAD.GET}/${new URL(modalValue?.identityBackImage).pathname.split('/').pop()}`,
        type: 'image/*',
        webkitRelativePath: '',
      });
      setFileListimgBack(imgBack);
      setListimgBack(modalValue?.identityBackImage);

      const imgFront: any = [];
      const idx2 = modalValue?.identityFrontImage?.lastIndexOf('/') + 1;
      const filename2 = modalValue?.identityFrontImage?.substr(idx2);

      imgFront.push({
        uid: modalValue?.identityFrontImage,
        name: filename2,
        status: 'done',
        url:
          modalValue?.identityFrontImage &&
          `${HOST}${UPLOAD.GET}/${new URL(modalValue?.identityFrontImage).pathname.split('/').pop()}`,
        type: 'image/*',
        webkitRelativePath: '',
      });
      setFileListimgFront(imgFront);
      setListimgFront(modalValue?.identityFrontImage);
    } else {
      form.resetFields();
    }
  }, [modalValue, isLoadingDetail]);

  const handleCancel = () => {
    handleVisibleChangeModalOwner(false, null);
    form.resetFields();
    setFileListimgFront([]);
    setFileListimgBack([]);
  };

  const handleChangeUploadImage = (type: string, data: any) => {
    setIsLoading(true);
    if (data?.file?.status !== 'removed') {
      const isLt2M = data?.file?.size / 1024 / 1024 < 10;
      if (!isLt2M) {
        message.error('Kích thước ảnh không được vượt quá 10Mb.');
        setIsLoading(false);
        return;
      }
    }
    if (type === 'identityFrontImage') {
      setFileListimgFront(data?.fileList);
    } else if (type === 'identityBackImage') {
      setFileListimgBack(data?.fileList);
    }

    const formData: any = new FormData();
    const file = data?.file?.originFileObj;
    formData.append('file', file);

    if (data?.file?.status === 'removed') {
      setIsLoading(false);
    }
    if (data?.file?.status === 'uploading') {
      return new Promise((resolve, reject) => {
        axiosIns
          .post(`${UPLOAD.SINGLE}`, formData, {
            headers: {
              'Content-Type': 'multipart/form-data',
            },
          })
          .then((s) => {
            setIsLoading(false);
            if (type === 'identityFrontImage') {
              setListimgFront(s?.data?.data?.fileUrl);
            } else if (type === 'identityBackImage') {
              setListimgBack(s?.data?.data?.fileUrl);
            }
          })
          .catch((e) => {
            setIsLoading(false);
            reject(e);
          });
      });
    }
  };

  const handleRemoveFile = (type: string) => () => {
    if (type === 'identityFrontImage') {
      setListimgFront(undefined);
    } else if (type === 'identityBackImage') {
      setListimgBack(undefined);
    }
  };

  const onFinish = async (value) => {
    if (!fileListimgFront || fileListimgFront?.length === 0) {
      message.error('Bạn chưa upload ảnh cccd mặt trước');
      return;
    }

    if (!fileListimgBack || fileListimgBack?.length === 0) {
      message.error('Bạn chưa upload ảnh cccd mặt sau');
      return;
    }

    const payload = {
      ...value,
      birthday: dayjs(form.getFieldValue('birthday')).format('YYYY/MM/DD'),
      releaseDateOfIdentify: dayjs(form.getFieldValue('releaseDateOfIdentify')).format('YYYY/MM/DD'),
      expirationDateOfIdentify: dayjs(form.getFieldValue('expirationDateOfIdentify')).format('YYYY/MM/DD'),
      identityFrontImage: listimgFront,
      identityBackImage: listimgBack,
    };
    if (modalValue) {
      return await new Promise((resolve, reject) => {
        setIsLoading(true);
        axiosIns
          .put(`${OWNER_API.UPDATE}/${modalValue.id}?type=2`, { ...payload })
          .then((s) => {
            if (s?.data?.code === 200) {
              message.success(s?.data?.message);
              setTimeout(() => {
                setIsLoading(false);
                handleCancel();
                fetchSearchUser({ page: 1, limit: 10 });
                resolve(s?.data?.data);
              }, 500);
            } else if (s?.data?.code >= 400) {
              message.error(s?.data?.message);
              setIsLoading(false);
            }
          })
          .catch((e) => {
            setIsLoading(false);
            message.error(e?.response?.data?.message);
            reject(e);
          });
      });
    } else {
      return await new Promise((resolve, reject) => {
        setIsLoading(true);
        axiosIns
          .post(`${OWNER_API.CREATE}?type=2`, { ...payload })
          .then((s) => {
            if (s?.data?.code === 201) {
              message.success(s?.data?.message);
              setTimeout(() => {
                setIsLoading(false);
                handleCancel();
                fetchSearchUser({ page: 1, limit: 10 });
                resolve(s?.data?.data);
              }, 500);
            } else if (s?.data?.code >= 400) {
              message.error(s?.data?.message);
              setIsLoading(false);
            }
          })
          .catch((e) => {
            setIsLoading(false);
            message.error(e?.response?.data?.message);
            reject(e);
          });
      });
    }
  };

  return (
    <Modal
      onCancel={handleCancel}
      className={styles.container}
      visible={openModalOwner}
      title={isDetail ? 'CHỈNH SỬA CHỦ NHÀ' : `THÊM MỚI CHỦ NHÀ`}
      footer={null}
    >
      <Spin spinning={isLoading}>
        <Form
          form={form}
          validateMessages={formValidateMessages}
          autoComplete="off"
          onFinish={onFinish}
          layout="vertical"
          labelAlign="left"
          labelCol={{ span: 24, offset: 0 }}
          wrapperCol={{ span: 23 }}
        >
          <Row gutter={16}>
            <Col className="gutter-row" span={12}>
              <Form.Item rules={[{ required: true }]} label="Họ và tên" name="fullName">
                <Input placeholder="Nhập họ và tên"></Input>
              </Form.Item>
            </Col>
            <Col className="gutter-row" span={12}>
              <Form.Item
                label="Số điện thoại"
                name="phone"
                rules={[
                  {
                    pattern: PATTERN_PHONE_NUMBER,
                    message: 'Định dạng số điện thoại không đúng',
                    required: true,
                  },
                ]}
              >
                <Input disabled={modalValue !== null ? true : false} placeholder="Nhập số điện thoại"></Input>
              </Form.Item>
            </Col>
            <Col className="gutter-row" span={12}>
              <Form.Item
                label="Email"
                name="email"
                rules={[
                  {
                    pattern: PATTERN_EMAIL,
                    message: 'Email không đúng định dạng',
                    required: true,
                  },
                ]}
              >
                <Input placeholder="Nhập email"></Input>
              </Form.Item>
            </Col>
            <Col className="gutter-row" span={12}>
              <Form.Item rules={[{ required: true }]} label="Số CCCD" name="identityNumber">
                <Input placeholder="Nhập số CCCD"></Input>
              </Form.Item>
            </Col>
            <Col className="gutter-row" span={12}>
              <Form.Item rules={[{ required: true }]} label="Ngày sinh" name="birthday">
                <DatePicker disabledDate={disableDateAfter} format={'DD/MM/YYYY'} placeholder="Nhập ngày sinh" />
              </Form.Item>
            </Col>
            <Col className="gutter-row" span={12}>
              <Form.Item rules={[{ required: true }]} label="Quê quán" name="hometown">
                <Input placeholder="Nhập quê quán"></Input>
              </Form.Item>
            </Col>
            <Col className="gutter-row" span={12}>
              <Form.Item rules={[{ required: true }]} label="Địa chỉ thường trú" name="permanentAddress">
                <Input placeholder="Nhập địa chỉ thường trú"></Input>
              </Form.Item>
            </Col>
            <Col className="gutter-row" span={12}>
              <Form.Item rules={[{ required: true }]} label="Ngày cấp" name="releaseDateOfIdentify">
                <DatePicker disabledDate={disableDateAfter} format={'DD/MM/YYYY'} placeholder="Ngày cấp" />
              </Form.Item>
            </Col>
            <Col className="gutter-row" span={12}>
              <Form.Item rules={[{ required: true }]} label="Ngày hết hạn" name="expirationDateOfIdentify">
                <DatePicker disabledDate={disableDateEnd} format={'DD/MM/YYYY'} placeholder="Ngày hết hạn" />
              </Form.Item>
            </Col>
            <Col className="gutter-row" span={12}>
              <Form.Item rules={[{ required: true }]} label="Nơi cấp" name="placeOfIdentify">
                <Input placeholder="Nhập nơi cấp"></Input>
              </Form.Item>
            </Col>
            <Col className="gutter-row" span={24}>
              <b style={{ marginLeft: '10px' }}>Ảnh CCCD</b>
            </Col>
            <Col className="gutter-row" span={12}>
              <Form.Item name="identityFrontImage" label="Mặt trước">
                <Upload
                  accept="image/*"
                  listType="picture-card"
                  showUploadList={{
                    showPreviewIcon: false,
                  }}
                  fileList={fileListimgFront}
                  onChange={(e) => handleChangeUploadImage('identityFrontImage', e)}
                  onRemove={handleRemoveFile('identityFrontImage')}
                >
                  {fileListimgFront?.length === 0 ? <PlusIcon /> : null}
                </Upload>
              </Form.Item>
            </Col>
            <Col className="gutter-row" span={12}>
              <Form.Item name="identityBackImage" label="Mặt sau">
                <Upload
                  accept="image/*"
                  listType="picture-card"
                  onChange={(e) => handleChangeUploadImage('identityBackImage', e)}
                  onRemove={handleRemoveFile('identityBackImage')}
                  fileList={fileListimgBack}
                  showUploadList={{
                    showPreviewIcon: false,
                  }}
                >
                  {fileListimgBack?.length === 0 ? <PlusIcon /> : null}
                </Upload>
              </Form.Item>
            </Col>
            <Col className="gutter-row" span={12}>
              <Form.Item rules={[{ required: true }]} label="Tên đăng nhập" name="username">
                <Input autoComplete="nope" placeholder="Nhập tên đăng nhập"></Input>
              </Form.Item>
            </Col>
            <Col className="gutter-row" span={12}>
              <Form.Item rules={[{ required: !isDetail ? true : false }]} label="Mật khẩu" name="password">
                <Input.Password autoComplete="new-password" placeholder="Nhập mật khẩu"></Input.Password>
              </Form.Item>
            </Col>
          </Row>
          <Form.Item wrapperCol={{ span: 24 }} style={{ margin: '10px' }}>
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <Button
                style={{
                  backgroundColor: '#C8CBD0',
                  color: 'white',
                  width: '60px',
                  borderRadius: '48px',
                  margin: '10px',
                  // height: "48px",
                }}
                type="primary"
                onClick={() => handleCancel()}
              >
                Hủy
              </Button>
              <Button
                style={{
                  backgroundColor: '#F89420',
                  color: 'white',
                  width: '60px',
                  borderRadius: '48px',
                  margin: '10px',
                  // height: "48px",
                }}
                type="primary"
                htmlType="submit"
              >
                Lưu
              </Button>
            </div>
          </Form.Item>
        </Form>
      </Spin>
    </Modal>
  );
};

export default ModalOwner;
