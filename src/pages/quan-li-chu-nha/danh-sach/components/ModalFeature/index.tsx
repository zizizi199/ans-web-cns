import { Button, Form, Input, Modal } from 'antd';
import React, { useEffect } from 'react';
import styles from './styles.module.scss';
import { formValidateMessages } from 'src/pages/constants';
interface Props {
  type: any;
  openModalFeature: boolean;
  handleVisibleChangeModalFeature: (value: boolean, type: any) => void;
  handleSubmitPost: (value: any, type: any) => void;
}

const ModalFeature: React.FC<Props> = ({
  type,
  openModalFeature,
  handleVisibleChangeModalFeature,
  handleSubmitPost,
}) => {
  const [form] = Form.useForm();

  const handleCancel = () => {
    handleVisibleChangeModalFeature(false, null);
    form.resetFields();
  };

  useEffect(() => {
    form.setFieldsValue({ type: type === 1 ? 'Đăng bài' : 'Tài khoản' });
  }, [openModalFeature]);

  useEffect(() => {
    form.setFieldsValue({ reason: null });
  }, [type]);

  const onSubmit = () => {
    handleSubmitPost(form.getFieldValue('reason'), type);
  };

  return (
    <Modal
      onCancel={handleCancel}
      className={styles.container}
      open={openModalFeature}
      title={`KHÓA TÍNH NĂNG`}
      afterClose={() => {
        form.resetFields();
      }}
      footer={[]}
    >
      <Form
        form={form}
        validateMessages={formValidateMessages}
        initialValues={{ remember: true }}
        autoComplete="off"
        onFinish={onSubmit}
        layout="vertical"
        labelAlign="left"
        labelCol={{ span: 24, offset: 0 }}
        wrapperCol={{ span: 23 }}
      >
        <Form.Item label="Tính năng" name="type">
          <Input placeholder="Chọn tính năng" disabled></Input>
        </Form.Item>
        <Form.Item rules={[{ required: true }]} label="Lý do" name="reason">
          <Input.TextArea placeholder="Nhập lí do"></Input.TextArea>
        </Form.Item>
        <Form.Item>
          <div
            key={Math.random()}
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              paddingBottom: 20,
              gap: '8px',
            }}
          >
            <Button
              style={{
                backgroundColor: '#C8CBD0',
                color: 'white',
                width: '60px',
                borderRadius: '48px',
                // height: "48px",
              }}
              onClick={() => handleVisibleChangeModalFeature(false, null)}
              key={'cancel'}
            >
              Hủy
            </Button>
            <Button
              style={{
                backgroundColor: '#F89420',
                color: 'white',
                width: '60px',
                borderRadius: '48px',
                // height: "48px",
              }}
              type="primary"
              htmlType="submit"
              key={'ok'}
            >
              Lưu
            </Button>
          </div>
          ,
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default ModalFeature;
