import { FormInstance, Spin, Switch } from 'antd';
import moment from 'moment';
import React from 'react';
import { Table } from 'src/common';
import { DeleteIcon, EditIcon } from 'src/components/Icon';
import styles from './styles.module.scss';

interface Props {
  form: FormInstance<any>;
  dsUser: any;
  isLoading: boolean;
  currentPage: number;
  pageSize: number;
  totalElement: number;
  onChangePage: (page: number) => void;
  onShowSizeChange: (current: number, size: number) => void;
  handleVisibleChangeModalOwner: (status: any, id: any) => void;
  handleVisibleChangeModalDelete: (status: any, record: any) => void;
  onChangePost: (checked: any, record: any, type: any) => void;
}

export const TableForm: React.FC<Props> = ({
  dsUser,
  isLoading,
  onChangePage,
  onShowSizeChange,
  currentPage,
  pageSize,
  totalElement,
  handleVisibleChangeModalOwner,
  handleVisibleChangeModalDelete,
  onChangePost,
}) => {
  return (
    <div className={styles.TableForm}>
      <Spin spinning={isLoading}>
        <Table
          pagination={{
            size: 'default',
            total: totalElement,
            onChange: onChangePage,
            pageSize: pageSize,
            current: currentPage,
            onShowSizeChange: onShowSizeChange,
            showTotal: (total) => (
              <b>
                Tổng bản ghi <b style={{ color: '#F89420' }}>{total} </b> |
              </b>
            ),
          }}
          dataSource={dsUser}
          columns={[
            {
              title: <div className="header-table">STT</div>,
              width: '3%',
              dataIndex: 'id',
              render: (value, row, index) => (currentPage - 1) * pageSize + index + 1,
            },
            {
              title: <div className="header-table">Họ và tên</div>,
              width: '5%',
              dataIndex: 'fullName',
              ellipsis: true,
              render: (value) => value,
            },
            {
              title: <div className="header-table">Số điện thoại</div>,
              width: '5%',
              dataIndex: 'phone',
              ellipsis: true,
            },
            {
              title: <div className="header-table">Email</div>,
              width: '8%',
              dataIndex: 'email',
              align: 'center',
              ellipsis: true,
              render: (value) => value,
            },
            {
              title: <div className="header-table">Số CMT</div>,
              width: '5%',
              dataIndex: 'identityNumber',
              align: 'center',
              ellipsis: true,
              render: (value) => value,
            },
            {
              title: <div className="header-table">Ngày sinh</div>,
              width: '5%',
              dataIndex: 'birthday',
              align: 'center',
              ellipsis: true,
              render: (value) => (value ? moment(value).format('DD/MM/YYYY') : ''),
            },
            {
              title: <div className="header-table">Quê quán</div>,
              width: '5%',
              dataIndex: 'placeOfIdentify',
              align: 'center',
              ellipsis: true,
              render: (value) => value,
            },
            {
              title: <div className="header-table">Địa chỉ thường trú</div>,
              width: '5%',
              dataIndex: 'permanentAddress',
              align: 'center',
              ellipsis: true,
              render: (value) => value,
            },
            {
              title: <div className="header-table">Tên đăng nhập</div>,
              width: '6%',
              dataIndex: 'username',
              align: 'center',
              ellipsis: true,
              render: (value) => value,
            },
            {
              title: <div className="header-table">Tính năng</div>,
              width: '5%',
              dataIndex: 'id',
              align: 'center',
              render: (value, row) => {
                return (
                  <div>
                    <div
                      style={{
                        display: 'flex',
                        cursor: 'pointer',
                        justifyContent: 'space-between',
                        marginBottom: '2px',
                      }}
                    >
                      Đăng bài
                      <Switch
                        onChange={(checked) => onChangePost(checked, row, 1)}
                        checked={row.lockedPostForHost === 1 ? true : false}
                      />
                    </div>
                    <div style={{ display: 'flex', cursor: 'pointer', justifyContent: 'space-between' }}>
                      Tài khoản
                      <Switch
                        onChange={(checked) => onChangePost(checked, row, 2)}
                        checked={row.lockedByCms === 1 ? true : false}
                      />
                    </div>
                  </div>
                );
              },
            },
            {
              title: <div className="header-table">Hành động</div>,
              width: '5%',
              dataIndex: 'id',
              align: 'center',
              render: (value, row) => {
                return (
                  <div style={{ display: 'flex', cursor: 'pointer', justifyContent: 'space-around' }}>
                    <EditIcon
                      onClick={() => {
                        handleVisibleChangeModalOwner(true, value);
                      }}
                    />
                    <DeleteIcon
                      onClick={() => {
                        handleVisibleChangeModalDelete(true, row);
                      }}
                    />
                  </div>
                );
              },
            },
          ]}
        />
      </Spin>
    </div>
  );
};
