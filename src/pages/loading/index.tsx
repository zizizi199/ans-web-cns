import { Spin } from 'antd'
import React from 'react'
import styles from './styles.module.scss'

export const SpinLoading: React.FC = () => (
  <div className={styles.spin}>
    <Spin />
  </div>
)
