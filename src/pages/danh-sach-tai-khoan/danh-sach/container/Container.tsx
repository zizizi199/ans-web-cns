import { Form } from 'antd';
import React, { useEffect, useState } from 'react';
import { combineUrlParams } from 'src/common';
import axiosIns from 'src/helpers/request';
import { FilterForm } from '../components/FilterForm';
import { TableForm } from '../components/Table';
import styles from './styles.module.scss';
import { USER_API } from 'src/helpers/api';
import useDebounce from 'src/hook/useDebounce';
import ModalAccount from '../components/ModalAccount';
import ModalDelete from '../components/ModalDelete';
import { useLocation } from 'react-router-dom';

function Container() {
  const [form] = Form.useForm();
  const [isLoading, setIsLoading] = useState(false);
  const [isDetail, setIsDetail] = useState(false);
  const [dsUser, setDsUser] = useState<any>([]);
  const [totalElement, setTotalElement] = useState<any>(0);
  const [filter, setFilter] = useState<any>({ limit: 10, page: 1 });
  const [openModalAccount, setOpenModalAccount] = useState(false);
  const [modalValue, setModalValue] = useState(null);
  const [openModalDelete, setOpenModalDelete] = useState(false);
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const paramValue = searchParams.get('paramName');

  //   Gọi API ds
  async function fetchSearchUser(params = {}) {
    return await new Promise((resolve, reject) => {
      setIsLoading(true);
      axiosIns
        .get(combineUrlParams(`${USER_API.LIST}`, { ...params }))
        .then((s) => {
          setDsUser(s?.data?.data?.content);
          setTotalElement(s?.data?.data?.totalElements);
          resolve(s?.data?.data?.content);
          setIsLoading(false);
          setFilter(params);
        })
        .catch((e) => {
          setIsLoading(false);
          reject(e);
        });
    });
    // .then
  }

  // API chi tiết
  const fetchDetail = async (value) => {
    return await new Promise((resolve, reject) => {
      setIsLoading(true);
      setIsDetail(true);
      axiosIns
        .get(`${USER_API.DETAIL}/${value}`)
        .then((s) => {
          setIsLoading(false);
          setModalValue(s?.data?.data);
          resolve(s?.data?.data);
        })
        .catch((e) => {
          setIsLoading(false);
          reject(e);
        });
    });
  };

  useEffect(() => {
    fetchSearchUser({ ...filter }).catch(console.error);
  }, []);

  const [keyWordValue, setKeyWordValue] = useState('');
  const onFilter = (value: any) => {
    setKeyWordValue(value);
  };
  const debounceSearch = useDebounce(keyWordValue, 500);

  useEffect(() => {
    const value: any = {
      ...debounceSearch,
      keyword: paramValue,
    };
    setKeyWordValue(value);
  }, [paramValue]);

  useEffect(() => {
    if (debounceSearch) {
      const input = {
        ...filter,
        keyword: debounceSearch?.keyword,
        startTime: debounceSearch?.date ? (debounceSearch?.date[0]).format('YYYY-MM-DD') : null,
        endTime: debounceSearch?.date ? (debounceSearch?.date[1]).format('YYYY-MM-DD') : null,
        status: debounceSearch?.status,
        page: 1,
        limit: 10,
      };
      fetchSearchUser(input);
    }
  }, [debounceSearch]);

  const onShowSizeChange = (current: number, size: number) => {
    fetchSearchUser({
      ...filter,
      limit: size,
    });
  };

  const onChangePage = (page: number) => {
    fetchSearchUser({
      ...filter,
      page: page,
    });
  };

  const handleVisibleChangeModalAccount = (status: any, id: any) => {
    setOpenModalAccount(status);
    if (id) {
      fetchDetail(id);
    } else {
      setModalValue(null);
      setIsDetail(false);
    }
  };

  const handleVisibleChangeModalDelete = (status: any, value: any) => {
    setOpenModalDelete(status);
    setModalValue(value);
  };

  return (
    <div className={styles.container}>
      <div className="sub-container">
        <p className="container-title">Danh sách tài khoản</p>
        <FilterForm form={form} onFilter={onFilter} handleVisibleChangeModalAccount={handleVisibleChangeModalAccount} />
        <TableForm
          totalElement={totalElement}
          currentPage={filter?.page}
          pageSize={filter?.limit}
          onChangePage={onChangePage}
          onShowSizeChange={onShowSizeChange}
          dsUser={dsUser}
          isLoading={isLoading}
          form={form}
          handleVisibleChangeModalAccount={handleVisibleChangeModalAccount}
          handleVisibleChangeModalDelete={handleVisibleChangeModalDelete}
        />
        <ModalAccount
          openModalAccount={openModalAccount}
          handleVisibleChangeModalAccount={handleVisibleChangeModalAccount}
          fetchSearchUser={fetchSearchUser}
          modalValue={modalValue}
          isDetail={isDetail}
        />
        <ModalDelete
          modalValue={modalValue}
          openModalDelete={openModalDelete}
          handleVisibleChangeModalDelete={handleVisibleChangeModalDelete}
          fetchSearchUser={fetchSearchUser}
        />
      </div>
    </div>
  );
}

export default Container;
