'use client';

import { Button, Form, Input, Modal, Select, Spin, message } from 'antd';
import React, { useEffect, useState } from 'react';
import styles from './styles.module.scss';
import { formValidateMessages } from 'src/pages/constants';
import axiosIns from 'src/helpers/request';
import { USER_API } from 'src/helpers/api';
import { NEW_PATTERN_PASSWORD, PATTERN_EMAIL, PATTERN_PHONE_NUMBER } from 'src/common/pattern';
import { accountTypeArr, statusAccountArr } from 'src/common';

interface Props {
  openModalAccount: boolean;
  handleVisibleChangeModalAccount: (status: any, id: any) => void;
  fetchSearchUser: (value: any) => void;
  modalValue: any;
  isDetail: any;
}

const ModalAccount: React.FC<Props> = ({
  openModalAccount,
  handleVisibleChangeModalAccount,
  fetchSearchUser,
  modalValue,
  isDetail,
}) => {
  const [isLoading, setIsLoading] = useState(false);
  const [form] = Form.useForm();

  useEffect(() => {
    if (modalValue !== null) {
      form.setFieldsValue({
        ...modalValue,
      });
    } else {
      form.resetFields();
    }
  }, [modalValue]);

  const handleCancel = () => {
    handleVisibleChangeModalAccount(false, null);
    form.resetFields();
  };

  const onFinish = async (value) => {
    const payload = {
      ...value,
    };
    if (modalValue) {
      return await new Promise((resolve, reject) => {
        setIsLoading(true);
        axiosIns
          .put(`${USER_API.UPDATE}/${modalValue.id}`, { ...payload })
          .then((s) => {
            if (s?.data?.code === 200) {
              message.success(s?.data?.message);
              setTimeout(() => {
                setIsLoading(false);
                handleCancel();
                fetchSearchUser({ page: 1, limit: 10 });
                resolve(s?.data?.data);
              }, 500);
            } else if (s?.data?.code >= 400) {
              message.error(s?.data?.message);
              setIsLoading(false);
            }
          })
          .catch((e) => {
            setIsLoading(false);
            message.error(e?.response?.data?.message);
            reject(e);
          });
      });
    } else {
      return await new Promise((resolve, reject) => {
        setIsLoading(true);
        axiosIns
          .post(`${USER_API.CREATE}`, { ...payload })
          .then((s) => {
            if (s?.data?.code === 201) {
              message.success(s?.data?.message);
              setTimeout(() => {
                setIsLoading(false);
                handleCancel();
                fetchSearchUser({ page: 1, limit: 10 });
                resolve(s?.data?.data);
              }, 500);
            } else if (s?.data?.code >= 400) {
              message.error(s?.data?.message);
              setIsLoading(false);
            }
          })
          .catch((e) => {
            setIsLoading(false);
            message.error(e?.response?.data?.message);
            reject(e);
          });
      });
    }
  };

  return (
    <Modal
      onCancel={handleCancel}
      className={styles.container}
      visible={openModalAccount}
      title={isDetail ? 'CHỈNH SỬA TÀI KHOẢN' : `THÊM MỚI TÀI KHOẢN`}
      footer={null}
    >
      <Spin spinning={isLoading}>
        <Form
          form={form}
          validateMessages={formValidateMessages}
          autoComplete="off"
          onFinish={onFinish}
          layout="vertical"
          labelAlign="left"
          labelCol={{ span: 24, offset: 0 }}
          wrapperCol={{ span: 23 }}
        >
          <Form.Item rules={[{ required: true }]} label="Tên tài khoản" name="fullName">
            <Input placeholder="Nhập họ và tên"></Input>
          </Form.Item>
          <Form.Item
            label="Email"
            name="email"
            rules={[
              {
                pattern: PATTERN_EMAIL,
                message: 'Email không đúng định dạng',
                required: true,
              },
            ]}
          >
            <Input placeholder="Nhập email"></Input>
          </Form.Item>
          <Form.Item
            label="Số điện thoại"
            name="phone"
            rules={[
              {
                pattern: PATTERN_PHONE_NUMBER,
                message: 'Định dạng số điện thoại không đúng',
                required: true,
              },
            ]}
          >
            <Input placeholder="Nhập số điện thoại"></Input>
          </Form.Item>
          <Form.Item rules={[{ required: true }]} label="Chức danh" name="positionCms">
            <Select placeholder="Chọn chức danh">
              {accountTypeArr?.map((item: any) => {
                return (
                  <Select.Option value={item?.id} key={item?.id}>
                    {item?.name}
                  </Select.Option>
                );
              })}
            </Select>
          </Form.Item>
          {modalValue && (
            <Form.Item rules={[{ required: true }]} label="Trạng thái" name="status">
              <Select placeholder="Chọn trạng thái">
                {statusAccountArr?.map((item: any) => {
                  return (
                    <Select.Option value={item?.id} key={item?.id}>
                      {item?.name}
                    </Select.Option>
                  );
                })}
              </Select>
            </Form.Item>
          )}
          <Form.Item
            label="Mật khẩu"
            name="password"
            rules={[
              {
                pattern: NEW_PATTERN_PASSWORD,
                message: 'Định dạng mật khẩu không đúng',
                required: !isDetail ? true : false,
              },
            ]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item wrapperCol={{ span: 24 }} style={{ margin: '10px' }}>
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <Button
                style={{
                  backgroundColor: '#C8CBD0',
                  color: 'white',
                  width: '60px',
                  borderRadius: '48px',
                  margin: '10px',
                  // height: "48px",
                }}
                type="primary"
                onClick={() => handleCancel()}
              >
                Hủy
              </Button>
              <Button
                style={{
                  backgroundColor: '#F89420',
                  color: 'white',
                  width: '60px',
                  borderRadius: '48px',
                  margin: '10px',
                  // height: "48px",
                }}
                type="primary"
                htmlType="submit"
              >
                Lưu
              </Button>
            </div>
          </Form.Item>
        </Form>
      </Spin>
    </Modal>
  );
};

export default ModalAccount;
