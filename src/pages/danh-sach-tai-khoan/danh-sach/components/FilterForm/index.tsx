import React from 'react';
import { Button, Col, Form, FormInstance, Row } from 'antd';
import styles from './styles.module.scss';
import { PlusIcon } from 'src/components/Icon';
interface Props {
  form: FormInstance<any>;
  onFilter: (value: object) => void;
  handleVisibleChangeModalAccount: (status: any, id: any) => void;
}

export const FilterForm: React.FC<Props> = ({ form, handleVisibleChangeModalAccount }) => {
  return (
    <div className={styles.FilterForm}>
      <Form form={form}>
        <Row>
          <Col span={18}>
            <Row>
              <Col span={4} style={{ marginRight: '10px' }}></Col>
            </Row>
          </Col>
          <Col span={6}>
            <div className="button-float">
              <Button
                onClick={() => {
                  handleVisibleChangeModalAccount(true, null);
                }}
                icon={<PlusIcon />}
                style={{ backgroundColor: '#1A94FF', color: 'white' }}
              >
                Thêm mới
              </Button>
            </div>
          </Col>
        </Row>
      </Form>
    </div>
  );
};
