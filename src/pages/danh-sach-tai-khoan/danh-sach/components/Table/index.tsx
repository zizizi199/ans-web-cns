import { FormInstance, Spin, Tag } from 'antd';
import React from 'react';
import { Table, accountTypeArr, statusAccountArr } from 'src/common';
import { DeleteIcon, EditIcon } from 'src/components/Icon';
import styles from './styles.module.scss';

interface Props {
  form: FormInstance<any>;
  dsUser: any;
  isLoading: boolean;
  currentPage: number;
  pageSize: number;
  totalElement: number;
  onChangePage: (page: number) => void;
  onShowSizeChange: (current: number, size: number) => void;
  handleVisibleChangeModalAccount: (status: any, id: any) => void;
  handleVisibleChangeModalDelete: (status: any, record: any) => void;
}

export const TableForm: React.FC<Props> = ({
  dsUser,
  isLoading,
  onChangePage,
  onShowSizeChange,
  currentPage,
  pageSize,
  totalElement,
  handleVisibleChangeModalAccount,
  handleVisibleChangeModalDelete,
}) => {
  return (
    <div className={styles.TableForm}>
      <Spin spinning={isLoading}>
        <Table
          pagination={{
            size: 'default',
            total: totalElement,
            onChange: onChangePage,
            pageSize: pageSize,
            current: currentPage,
            onShowSizeChange: onShowSizeChange,
            showTotal: (total) => (
              <b>
                Tổng bản ghi <b style={{ color: '#F89420' }}>{total} </b> |
              </b>
            ),
          }}
          dataSource={dsUser}
          columns={[
            {
              title: <div className="header-table">STT</div>,
              width: '2%',
              dataIndex: 'id',
              render: (value, row, index) => (currentPage - 1) * pageSize + index + 1,
            },
            {
              title: <div className="header-table">Tên tài khoản</div>,
              width: '5%',
              dataIndex: 'fullName',
              ellipsis: true,
              render: (value) => value,
            },
            {
              title: <div className="header-table">Email</div>,
              width: '10%',
              dataIndex: 'email',
              align: 'center',
              ellipsis: true,
              render: (value) => value,
            },
            {
              title: <div className="header-table">Số điện thoại</div>,
              width: '5%',
              dataIndex: 'phone',
              align: 'center',
              ellipsis: true,
              render: (value) => value,
            },
            {
              title: <div className="header-table">Chức danh</div>,
              width: '5%',
              dataIndex: 'positionCms',
              render: (value) => accountTypeArr.find((item: any) => item?.id === value)?.name,
            },
            {
              title: <div className="header-table">Trạng thái</div>,
              width: '5%',
              dataIndex: 'status',
              align: 'center',
              ellipsis: true,
              render: (value) => (
                <Tag color={value === 0 ? 'red' : 'green'}>
                  {statusAccountArr.find((item: any) => item?.id === value)?.name}
                </Tag>
              ),
            },
            {
              title: <div className="header-table">Hành động</div>,
              width: '5%',
              dataIndex: 'id',
              align: 'center',
              render: (value, row) => {
                return (
                  <div style={{ display: 'flex', cursor: 'pointer', justifyContent: 'space-around' }}>
                    <EditIcon
                      onClick={() => {
                        handleVisibleChangeModalAccount(true, value);
                      }}
                    />
                    <DeleteIcon
                      onClick={() => {
                        handleVisibleChangeModalDelete(true, row);
                      }}
                    />
                  </div>
                );
              },
            },
          ]}
        />
      </Spin>
    </div>
  );
};
