import React, { Suspense, memo } from 'react';

import { SpinLoading } from './pages/loading';
import { RootRouter } from './routes';
import './styles/app.scss';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';

const App = memo(() => {
  const queryClient = new QueryClient();

  return (
    <QueryClientProvider client={queryClient}>
        <Suspense fallback={<SpinLoading />}>
          <RootRouter />
        </Suspense>
    </QueryClientProvider>
  );
});

App.displayName = 'MyApp';

export default App;
