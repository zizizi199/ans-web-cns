import { Layout } from 'antd';
import React, { useState } from 'react';
import styles from './styles.module.scss';
import { SideBar } from '../SideBar';
import { Outlet } from 'react-router-dom';
import classNames from 'classnames';
import { Header } from '../Header';

interface Props {
  allowModules?: any;
  currentUser?: any;
}

const MainLayout = ({ allowModules, currentUser }: Props) => {
  const [collapsed, setCollapsed] = useState<boolean>(false);

  return (
    <Layout>
      <SideBar setCollapsed={setCollapsed} collapsed={collapsed} currentUser={currentUser} />
      <Layout>
        <div className={classNames(styles.section, { [styles.collapsed]: collapsed, [styles.expanded]: !collapsed })}>
          <Header />
          <div className={styles.content}>
            <Outlet context={allowModules} />
          </div>
        </div>
      </Layout>
    </Layout>
  );
};

export default MainLayout;
