import type { MenuProps } from 'antd'
import type React from 'react'

export interface IMenuItem {
  title: string
  to: string
  permission?: any
  key: any
  icon?: React.ReactNode
  submenu?: boolean
  submenuItems?: IMenuItem[]
  role?: any[]
}

export type MenuItem = Required<MenuProps>['items'][number]
