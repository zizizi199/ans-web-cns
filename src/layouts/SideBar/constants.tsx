import { AccountIcon, EvaluateIcon, HostelIcon, OwnerIcon, TenantIcon, TransactionIcon } from '../../components/Icon';
import {
  AccountRoutes,
  EvaluateRoutes,
  HostelRoutes,
  OwnerRoutes,
  TenantRoutes,
  TransactionRoutes,
} from '../../helpers/app.routes';
import React from 'react';

export {};

export const SideBarRoute = {
  SUPER_ADMIN: 'Super admin',
  ADMIN: 'Admin',
  CONSUMER: 'Tiêu thụ',
  ACCOUNTANT: 'Kế toán',
  AGENCY: 'Đại lý',
  MARKET: 'Thị trường',
};

export const menus = [
  {
    key: ['Quản lí chủ nhà'],
    title: 'Quản lí chủ nhà',
    to: OwnerRoutes.list,
    icon: <OwnerIcon />,
  },
  {
    key: ['Quản lí người đi thuê'],
    title: 'Quản lí người đi thuê',
    to: TenantRoutes.list,
    icon: <TenantIcon />,
  },
  {
    key: ['Quản lí nhà cho thuê'],
    title: 'Quản lí nhà cho thuê',
    to: HostelRoutes.list,
    icon: <HostelIcon />,
    submenu: true,
    submenuItems: [
      {
        title: 'Danh sách nhà trọ',
        to: HostelRoutes.list,
        key: ['Danh sách nhà trọ'],
      },
      {
        key: ['Quản lí tiện ích'],
        title: 'Quản lí tiện ích',
        to: HostelRoutes.list,
      },
    ],
  },
  {
    key: ['Lịch sử giao dịch'],
    title: 'Lịch sử giao dịch',
    to: TransactionRoutes.list,
    icon: <TransactionIcon />,
  },
  {
    key: ['Quản lí đánh giá'],
    title: 'Quản lí đánh giá',
    to: EvaluateRoutes.list,
    icon: <EvaluateIcon />,
  },
  {
    key: ['Quản lí tài khoản'],
    title: 'Quản lí tài khoản',
    to: AccountRoutes.list,
    icon: <AccountIcon />,
    submenu: true,
    submenuItems: [
      {
        title: 'Danh sách tài khoản',
        to: AccountRoutes.list,
        key: ['Danh sách tài khoản'],
      },
      {
        key: ['Phân quyền'],
        title: 'Phân quyền',
        to: AccountRoutes.list,
      },
    ],
  },
];
