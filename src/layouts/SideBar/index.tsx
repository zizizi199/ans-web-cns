import { Layout, Menu } from 'antd';
import React from 'react';
import { NavLink } from 'react-router-dom';
import { AppRoutes } from '../../helpers/app.routes';
import type { IMenuItem, MenuItem } from '../types';
import { menus } from './constants';
import styles from './styles.module.scss';
import { AnsLogoIcon } from '../../components/Icon';

interface Props {
  allowModules?: any;
  currentUser?: any;
  collapsed: boolean;
  setCollapsed: React.Dispatch<React.SetStateAction<boolean>>;
}

function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  children?: MenuItem[],
  type?: 'group'
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}

const { Sider } = Layout;

export const SideBar: React.FC<Props> = ({ collapsed, setCollapsed }) => {
  function getMenuItems(menus: IMenuItem[]) {
    return menus.map((menu: IMenuItem) => {
      if (menu.submenu && menu.submenuItems != null) {
        return getItem(
          menu.title,
          menu.key,
          menu.icon,
          menu.submenuItems.map((subMenuItem) =>
            getItem(
              <NavLink className={styles.bold} to={subMenuItem.to} key={subMenuItem.key}>
                {subMenuItem.title}
              </NavLink>,
              subMenuItem.key,
              subMenuItem.icon
            )
          )
        );
      }

      return getItem(
        <NavLink className={styles.bold} to={menu.to} key={menu.key}>
          {menu.title}
        </NavLink>,
        menu.key,
        menu.icon
      );
    });
  }

  const items: MenuItem[] = getMenuItems(menus);

  return (
    <Sider
      collapsible
      onCollapse={(value) => {
        setCollapsed(value);
      }}
      collapsed={collapsed}
      width={255}
      className={styles.slider}
    >
      <NavLink to={AppRoutes.home} className={styles.logo}>
        <div className={styles.logoWrapper}>{collapsed ? <AnsLogoIcon /> : <AnsLogoIcon />}</div>
      </NavLink>
      <div className={styles.sidebar}>
        <Menu mode="inline" style={{ width: collapsed ? 80 : 255 }} items={items} />
      </div>
    </Sider>
  );
};
