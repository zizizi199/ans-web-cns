import { ReactComponent as AnsLogoIcon } from './logo.svg';
import { ReactComponent as OwnerIcon } from './owner.svg';
import { ReactComponent as ArrowDownIcon } from './arrow-down.svg';
import { ReactComponent as AvatarIcon } from './avatar.svg';
import { ReactComponent as LogOutIcon } from './logout.svg';
import { ReactComponent as BellIcon } from './bell.svg';
import { ReactComponent as ExportIcon } from './export.svg';
import { ReactComponent as PlusIcon } from './plus.svg';
import { ReactComponent as SearchingIcon } from './search.svg';
import { ReactComponent as TenantIcon } from './tenant.svg';
import { ReactComponent as EditIcon } from './edit.svg';
import { ReactComponent as DeleteIcon } from './delete.svg';
import { ReactComponent as DeleteImage } from './deleteImg.svg';
import { ReactComponent as EvaluateIcon } from './evaluate.svg';
import { ReactComponent as TransactionIcon } from './transaction.svg';
import { ReactComponent as HostelIcon } from './hostel.svg';
import { ReactComponent as AccountIcon } from './acount.svg';
import { ReactComponent as DeclineIcon } from './decline.svg';
import { ReactComponent as AcceptWhiteIcon } from './accept-white.svg';
import { ReactComponent as HouseIcon } from './house.svg';
import { ReactComponent as MoneyIcon } from './money-icon.svg';
import { ReactComponent as PreviousIcon } from './previousBtn.svg';
import { ReactComponent as NextIcon } from './nextBtn.svg';
import { ReactComponent as EditRoomIcon } from './bx_edit.svg';
import { ReactComponent as StatusRoomIcon } from './pajamas_status.svg';
import { ReactComponent as MoneyRoomIcon } from './room-money.svg';
import { ReactComponent as PreviewIcon } from './preview.svg';
import { ReactComponent as Decline_X_Icon } from './decline-x.svg';
import { ReactComponent as ApproveIcon } from './approve.svg';
import { ReactComponent as VerifyIcon } from './isVerify.svg';
import { ReactComponent as UnVerifyIcon } from './unVerify.svg';

export {
  VerifyIcon,
  UnVerifyIcon,
  AnsLogoIcon,
  OwnerIcon,
  ArrowDownIcon,
  AvatarIcon,
  LogOutIcon,
  BellIcon,
  ExportIcon,
  PlusIcon,
  SearchingIcon,
  TenantIcon,
  EditIcon,
  DeleteIcon,
  DeleteImage,
  EvaluateIcon,
  TransactionIcon,
  HostelIcon,
  AccountIcon,
  DeclineIcon,
  AcceptWhiteIcon,
  HouseIcon,
  MoneyIcon,
  PreviousIcon,
  NextIcon,
  EditRoomIcon,
  StatusRoomIcon,
  MoneyRoomIcon,
  PreviewIcon,
  Decline_X_Icon,
  ApproveIcon,
};
