import { Button, Dropdown, Form, Input, Modal, Space, Spin, Tooltip, message } from 'antd';
import React, { useEffect, useState } from 'react';
import classNames from 'classnames';
import styles from './styles.module.scss';
import axiosIns from '../../helpers/request';
import { AUTH } from '../../helpers/api';
import { ArrowDownIcon, AvatarIcon, LogOutIcon, SearchingIcon } from '../Icon';
import { InfoCircleOutlined, LockOutlined } from '@ant-design/icons';
import { formValidateMessages } from 'src/pages/constants';
import { useLocation, useNavigate } from 'react-router-dom';

export const ProfileUser: React.FC = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const [hovered, setHovered] = useState(false);
  const [showInput, setshowInput] = useState<boolean>(false);
  const [inputValue, setInputValue] = useState<string>('');
  const [modalOpen, setModalOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const userDataString: any = localStorage.getItem('userInfor');
  const userData: any = JSON.parse(userDataString);
  const handleCancel = () => {
    form.resetFields();
    setModalOpen(false);
  };
  const [form] = Form.useForm();

  const handleChangePass = () => {
    setModalOpen(true);
  };
  useEffect(() => {
    setInputValue('');
    if (
      location.pathname === '/quan-li-chu-nha/danh-sach' ||
      location.pathname === '/quan-li-nguoi-di-thue/danh-sach' ||
      location.pathname === '/danh-sach-nha-tro/danh-sach' ||
      location.pathname === '/lich-su-giao-dich/danh-sach' ||
      location.pathname === '/quan-li-danh-gia/danh-sach' ||
      location.pathname === '/danh-sach-tai-khoan/danh-sach'
    ) {
      setshowInput(true);
    } else {
      setshowInput(false);
    }
  }, [location.pathname]);

  const onFinish = (values: any) => {
    setIsLoading(true);
    return new Promise((resolve, reject) => {
      axiosIns
        .put(`${AUTH.CHANGE_PASSWORD}/${userData.id}`, { ...values, phoneNumber: userData?.phoneNumber })
        .then((s) => {
          setIsLoading(false);
          if (s?.data?.statusCode === 200) {
            message.success(s?.data?.message);
            handleCancel();
          } else if (s?.data?.statusCode >= 202) {
            message.error(s?.data?.message);
          }
          resolve(s);
        })
        .catch((e) => {
          setIsLoading(false);
          message.error(e?.response?.data?.message);
          reject(e);
        });
    });
  };

  function handleLogout() {
    return new Promise((resolve, reject) => {
      axiosIns
        .get(AUTH.LOGOUT)
        .then((s) => {
          message.success('Đăng xuất thành công');
          resolve(s);
          localStorage.clear();
          window.location.href = '/login';
        })
        .catch((e) => {
          reject(e);
          localStorage.clear();
        });
    });
  }

  const handleInputChange = (event) => {
    console.log('event', event);

    setInputValue(event.target.value);
    const currentPath = location.pathname;
    const newSearch = new URLSearchParams(location.search);
    newSearch.set('paramName', event.target.value);
    navigate(`${currentPath}?${newSearch.toString()}`);
  };

  return (
    <>
      <div className={styles.inputText}>
        {showInput ? (
          <Input
            value={inputValue}
            onChange={handleInputChange}
            prefix={<SearchingIcon />}
            placeholder="Nhập tìm kiếm ..."
          />
        ) : (
          <></>
        )}
      </div>
      <Dropdown
        visible={hovered}
        onVisibleChange={setHovered}
        placement="bottomRight"
        trigger={['click']}
        className={styles.wrapInfo}
        overlay={
          <div className={styles.dropDownContainer}>
            <div className={styles.dropDown}>
              <LockOutlined style={{ fontSize: '20px', color: '#1F81BB' }} />
              <span className={styles.textModal}>
                <Button className={styles.dropDownBtn} onClick={() => handleChangePass()}>
                  Đổi mật khẩu
                </Button>
              </span>
            </div>
            <div className={styles.dropDown}>
              <LogOutIcon />
              <span className={styles.textModal}>
                <Button className={styles.dropDownBtn} onClick={() => handleLogout()}>
                  Đăng xuất
                </Button>
              </span>
            </div>
          </div>
        }
      >
        <div>
          <AvatarIcon />
          <div className={styles.content}>
            <span className={styles.textProfileName}>{localStorage.getItem('fullName')}</span>
            <p className={styles.greyName}>Admin</p>
          </div>
          <div
            className={classNames(styles.arrowIcon, {
              [styles.active]: hovered,
            })}
          >
            <ArrowDownIcon />
          </div>
        </div>
      </Dropdown>
      <Modal
        width={600}
        visible={modalOpen}
        title={<strong>Đổi mật khẩu</strong>}
        footer={null}
        onCancel={handleCancel}
      >
        <Spin spinning={isLoading}>
          <Form
            labelAlign="left"
            validateMessages={formValidateMessages}
            form={form}
            autoComplete="off"
            onFinish={onFinish}
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
          >
            <Form.Item label="Mật khẩu cũ" name="currentPassword" rules={[{ required: true }]}>
              <Input.Password placeholder="Nhập mật khẩu" />
            </Form.Item>
            <Form.Item label="Mật khẩu mới" name="newPassword" rules={[{ required: true }]}>
              <Input.Password
                placeholder="Nhập mật khẩu"
                prefix={
                  <Tooltip
                    title={
                      'Mật khẩu chứa tối thiểu 8 ký tự, tối đa 255 ký tự và có ít nhất một chữ hoa, một chữ thường, một số và một ký tự đặc biệt.'
                    }
                  >
                    <InfoCircleOutlined style={{ color: 'rgba(0,0,0,.45)' }} />
                  </Tooltip>
                }
              />
            </Form.Item>
            <Form.Item
              label="Xác nhận mật khẩu mới"
              name="reNewPassword"
              rules={[
                { required: true },
                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (!value || getFieldValue('newPassword') === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject(new Error('Mật khẩu không trùng khớp'));
                  },
                }),
              ]}
            >
              <Input.Password
                placeholder="Nhập mật khẩu"
                prefix={
                  <Tooltip
                    title={
                      'Mật khẩu chứa tối thiểu 8 ký tự, tối đa 255 ký tự và có ít nhất một chữ hoa, một chữ thường, một số và một ký tự đặc biệt.'
                    }
                  >
                    <InfoCircleOutlined style={{ color: 'rgba(0,0,0,.45)' }} />
                  </Tooltip>
                }
              />
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Space>
                <Button type="primary" htmlType="submit">
                  Xác nhận
                </Button>
                <Button type="primary" danger onClick={() => handleCancel()}>
                  Hủy
                </Button>
              </Space>
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    </>
  );
};
