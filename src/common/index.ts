export { default as Table } from './Table';

export const formatPrice = (number: number, sperator?: string) => {
  return number !== null && typeof number === 'number'
    ? number?.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, `$1${sperator ?? ','}`)
    : 0;
};

export const combineUrlParams = (url = '', params = {}) => {
  const keys = Object.keys(params);
  const paramUrl = keys
    .filter((key) => ![undefined, null, ''].includes(params[key]))
    .map((key) => `${key}=${params[key]}`)
    .join('&');
  return `${url}?${paramUrl}`;
};

export function forceDownload(blob, filename) {
  const a = document.createElement('a');
  a.download = filename;
  a.href = blob;
  document.body.appendChild(a);
  a.click();
  a.remove();
}

export const EXCEL_FORMAT_FILE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

export enum accountType {
  ADMIN = 1,
  SYSTEM_ADMIN,
  SALE,
}

export const accountTypeArr = [
  {
    id: accountType.ADMIN,
    name: 'Admin',
  },
  {
    id: accountType.SYSTEM_ADMIN,
    name: 'Admin hệ thống',
  },
  {
    id: accountType.SALE,
    name: 'Sale',
  },
];

export const statusAccountArr = [
  {
    id: 0,
    name: 'Chưa hoạt động',
  },
  {
    id: 1,
    name: 'Hoạt động',
  },
];

export enum statusTransactionType {
  PENDING = 1,
  CANCEL = 2,
  COMPLETE = 3,
}

export const statusTransactionArr = [
  {
    id: statusTransactionType.PENDING,
    name: 'Chờ thanh toán',
  },
  {
    id: statusTransactionType.CANCEL,
    name: 'Hủy',
  },
  {
    id: statusTransactionType.COMPLETE,
    name: 'Đã chuyển',
  },
];

export enum AttributeType {
  GENERAL_UTILITIES = 1, //tiện ích chung
  SERVICE = 2, //dịch vụ
  INFRASTRUCTURE = 3, //tiện ích riêng
}

export const ProductTypeArray = [
  {
    label: 'Căn hộ',
    key: 1,
  },
  {
    label: 'Căn hộ mini',
    key: 2,
  },
  {
    label: 'Phòng cho thuê',
    key: 3,
  },
];

export const filterOption = (input: string, option: any) => {
  return option.props.children.toString().toLowerCase().indexOf(input.toLowerCase().trim()) >= 0;
};

export function getVideoId(link: string) {
  const regExp =
    /^(?:https?:\/\/)?(?:www\.)?(?:youtube\.com\/(?:watch\?v=|embed\/|v\/|channels\/(?:\w+\/)?|user\/\w+|user\/(?:\w+\/)?\/user\/\w+)|youtu\.be\/)([a-zA-Z0-9_-]{11})/;
  const match = link?.match(regExp);
  return match?.[1] ?? null;
}

export enum roomStatusType {
  RENTED = 1, //Đã cho thuê
  SELLING = 2, //Đã mở bán
  NOT_SOLD_YET = 3, //Chưa mở bán
  NOT_SOLD = 4, //Không được mở bán
}

export const roomStatus: any = [
  {
    label: 'Đã cho thuê',
    key: 1,
  },
  {
    label: 'Đã mở bán',
    key: 2,
  },
  {
    label: 'Chưa mở bán',
    key: 3,
  },
  {
    label: 'Không được mở bán',
    key: 4,
  },
];

export enum roomStatusApproveType {
  NON_APPORVE = 0, 
  APPORVE = 1, 
  DECLINE = 2, 
}

export const roomStatusApprove: any = [
  {
    label: 'Chưa duyệt',
    key: 0,
  },
  {
    label: 'Đã duyệt',
    key: 1,
  },
  {
    label: 'Từ chối',
    key: 2,
  }
];

export const today = new Date();


export enum RoomDirection {
  WEST = 1,
  SOUTH,
  NORTH,
  EAST,
  NORTH_WEST,
  NORTH_EAST,
  SOUTH_WEST,
  SOUTH_EAST,
}

export const RoomDirectionArray = [
  {
    label: 'Tây',
    value: 1,
  },
  {
    label: 'Nam',
    value: 2,
  },
  {
    label: 'Bắc',
    value: 3,
  },
  {
    label: 'Đông',
    value: 4,
  },
  {
    label: 'Tây-Bắc',
    value: 5,
  },
  {
    label: 'Đông-Bắc',
    value: 6,
  },
  {
    label: 'Tây-Nam',
    value: 7,
  },
  {
    label: 'Đông-Nam',
    value: 8,
  },
];

export const statusCommentArr = [
  {
    id: 0,
    name: 'Chờ xử lý',
  },
  {
    id: 1,
    name: 'Đã duyệt',
  },
  {
    id: 2,
    name: 'Đã hủy',
  },
];

export const statusTenantsArr = [
  {
    id: 1,
    name: 'Không khóa',
  },
  {
    id: 2,
    name: 'Đã khóa',
  },
];
