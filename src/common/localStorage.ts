export const localStore = {
  ACCESS_TOKEN: 'access-token',
  ALLOW_MODULES: 'allowModules',
  EMAIL_RESET:'emailReset',
  OPT_RESET:'otpReset',
  PASSWORD_RESET:'passwordReset'
}
