const PATTERN_EMAIL =
  /^[\s\S]*(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))(?= *$)/;
const PATTERN_PHONE_NUMBER = /^\d{10}$/;
const PATTERN_PASSWORD = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/;
export const NEW_PATTERN_PASSWORD =/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,12}$/;
const PATTERN_NUMBER = /^[0-9]/;
const PATTERN_NUMBER_HOSPITAL = /^[0][0-9]/;
export { PATTERN_EMAIL, PATTERN_PHONE_NUMBER, PATTERN_PASSWORD, PATTERN_NUMBER, PATTERN_NUMBER_HOSPITAL };

export const POSITIVE_NUMBER = /^\d{1,3}$/;
export const POSITIVE_NUMBER_1 = /^[+]?\d+([.]\d+)?$/;
export const PERCENTAGE_NUMBER = /^\b(?<!\.)(?!0+(?:\.0+)?%)(?:\d|[1-9]\d|100)(?:(?<!100)\.\d+)?%/;
